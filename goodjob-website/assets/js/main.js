$(document).ready(function() {

	//toggling responsive menu
	$('.menu-toggle').on('click', function(e) {
		// e.preventDefault();
		if(!$(this).hasClass('open')) {
			openMenu();
		} else {
			closeMenu();
		}
		$(this).closest('header').toggleClass('mobile-nav-opened');
		$(this).closest('body').toggleClass('overlayed');
	});

	//initializing popup
	// $('.popup-init').on('click', function(e) {
	// 	e.preventDefault();
	// 	$('.popup').fadeIn(200);
	// 	$('body').addClass('popup-overlayed');
	// });

	//closing popup message
	$('.popup-close').on('click', function(e) {
		e.preventDefault();
		$('.popup').fadeOut(200);
		$('body').removeClass('popup-overlayed');
	});

	//initialinzg owl carousel
	if($('.owl-carousel-jobs').length) {
		$('.owl-carousel-jobs').owlCarousel({
			items: 1,
			loop: true,
			margin: 10,
			responsive: {
				768 : {
					items: 3
				},
				
				1200 : {
					items: 4
				}
			}
		});
	};

	//initializing owl carousel for job details page
	if ($('.owl-carousel-details').length) {
		var owlDetails = $('.owl-carousel-details');

		owlDetails.owlCarousel({
			items: 1,
			loop: true,
			margin: 0
		});

		$('.slide-next').on('click', function() {
			console.log('asd');
			owlDetails.trigger('next.owl.carousel');
		});

		$('.slide-prev').on('click', function() {
			owlDetails.trigger('prev.owl.carousel');
		});
	};

	//showing mobile number form
	$('.btn-add-form').on('click', function() {
		$(this).addClass('toHide').removeClass('toShow');
		$(this).siblings('.phone-form').addClass('shown').removeClass('toHide');
	})

	//hiding mobile number form on submit
	$('.phone-form button[type=submit]').on('click', function(e) {
		e.preventDefault();
		var self = $(this),
			phoneForm = self.closest('.phone-form'),
			btn = phoneForm.siblings('.btn-add-form');
		$(this).addClass('submitted');
		setTimeout(function() {
			phoneForm.addClass('toHide').removeClass('shown');
			btn.addClass('toShow').removeClass('toHide');
		}, 1000);
		setTimeout(function() {
			phoneForm.removeClass('toHide');
			btn.removeClass('toShow');
		}, 2200);
	});

	//showing washington state fair popup
	$('.js-btn-wsf-signup').on('click', function(e) {
		e.preventDefault();

		$('.popup-wsf-wrap').fadeIn(300).addClass('shown');
		$('body').addClass('popup-overlayed');
	});

	//masking phone input in washington state fair popup
	$('.popup-wsf-wrap #q3').mask('(000) 000-0000', {
		selectOnFocus: true
	});

	//controlling timeshift labels and inputs for washington state fair popup
	$('.fs-checkbox-group label.day-wrap').on('click', function() {
		var self = $(this),
			selfFor = self.attr('for').split('-'),
			forLastChar = selfFor.pop();

		switch (forLastChar) {
			case 'a':
				selfFor = selfFor.join('-');
				self.attr('for', selfFor + '-' + 'b');
				self.removeClass('unchecked').addClass('shift-1');
				break;
			case 'b':
				selfFor = selfFor.join('-');
				self.attr('for', selfFor + '-' + 'c');
				self.removeClass('shift-1').addClass('shift-2');
				break;
			case 'c':
				selfFor = selfFor.join('-');
				self.attr('for', selfFor + '-' + 'd');
				self.removeClass('shift-2').addClass('shift-3');
				break;
			case 'd':
				selfFor = selfFor.join('-');
				self.attr('for', selfFor + '-' + 'a');
				self.removeClass('shift-3').addClass('unchecked');
				break;
		};
	});

	//masking phone input as country code and activating form submit when 10 digits are typed
	$('header .phone-form input[type=tel]').mask('(000) 000-0000', {
		onComplete: function() {
			$('header .phone-form button[type=submit]').removeAttr('disabled');
		},
		selectOnFocus: true
	});

	$('.js-explore .phone-form input[type=tel]').mask('(000) 000-0000', {
		onComplete: function() {
			$('.js-explore .phone-form button[type=submit]').removeAttr('disabled');
		},
		selectOnFocus: true
	});

	//disabling submit button on mobile phone number form when less than 10 digits are inputted
	var unmaskedNum;
	$('.phone-form input[type=tel]').on('keyup', function(e) {
		var self = $(this);
		unmaskedNum = $(this).cleanVal();

		if (unmaskedNum.length < 10) {
			self.siblings('button[type=submit]').prop('disabled', true);
		}
	});

	//initially disabling the submit button on email forms
	$(".email-form-wrap input[type=submit]").prop('disabled', true);

	//validating entered email on input's value change
	$(".email-form-wrap input[type=text]").on("input", validate);

	//disabling popup form submit button initially
	$('.popup-form-wrap input[type=submit]').prop('disabled', true);

	//enabling popup form submit button on initial keyup (not for phone number)
	$('.popup-form-wrap input:not([placeholder=\'Phone Number\'])').on('keyup', function() {
		$('.popup-form-wrap input[type=submit]').prop('disabled', false);
	});

	//masking phone input as country code and activating form submit when 10 digits are typed
	$('.popup-form-wrap input[placeholder=\'Phone Number\']').mask('(000) 000-0000', {
		onComplete: function() {
			$('.popup-form-wrap input[type=submit]').removeAttr('disabled');
		}
	});

	// Add phone number: change flag
    $('.js-flag-show').click(function () {
        $(this).find('.js-flag-select').fadeToggle(200);
    });
    $('.js-flag-select button').click(function () {
        var btnName = $(this).attr('name');
        var flagContainer = $(this).parents('.js-flag-show');
        var flagClass = flagContainer.attr('class').match(/flagName[\w-]*\b/);
        flagContainer.removeClass(flagClass[0]).addClass(btnName);
    });

    //submitting phone number to goodjob app
    function sendTryGoodJobRequest($btnSubmit){
		var phoneNumber = $btnSubmit.siblings('input[type=tel]').val();
		var userType = $btnSubmit.data('usertype');
		
		if(phoneNumber != '' && userType != ''){			
			$.ajax({
				headers:{'Content-Type': 'application/json'},
				method: "POST",
				url: 'https://goodjob-api-dev.azurewebsites.net/api/Public/GoodJobIO/SendMessage',
				data: JSON.stringify({
					number: phoneNumber,
					userType: userType
				})
			})
		}
	};

	//blog listing aside behaviour
	if($('.blog-listing').length) {

		blogSelfAdPos();

		$(document).on('scroll', function() {
			blogSelfAdPos();
		});

	};
	
});

function openMenu(){
	$('div.circle').addClass('expand');
	$('div.burger').addClass('open');
	$('div.x, div.y, div.z').addClass('collapse');
	$('.menu li').addClass('animate');

	setTimeout(function(){
		$('div.y').hide();
		$('div.x').addClass('rotate30');
		$('div.z').addClass('rotate150');
	}, 70);
	
	setTimeout(function(){
		$('div.x').addClass('rotate45');
		$('div.z').addClass('rotate135');
	}, 120);
};

function closeMenu(){
	$('div.burger').removeClass('open');
	$('div.x').removeClass('rotate45').addClass('rotate30');
	$('div.z').removeClass('rotate135').addClass('rotate150');
	$('div.circle').removeClass('expand');
	$('.menu li').removeClass('animate');
	
	setTimeout(function(){
		$('div.x').removeClass('rotate30');
		$('div.z').removeClass('rotate150');
	}, 50);

	setTimeout(function(){
		$('div.y').show();
		$('div.x, div.y, div.z').removeClass('collapse');
	}, 70);
};

function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
};

function validate() {
	var self = $(this);
	var email = self.val();
	if (validateEmail(email)) {
		self.closest('form').find('input[type=submit]').removeAttr('disabled');
	} else {
		self.closest('form').find('input[type=submit]').prop('disabled', true);
	}
	return false;
};

function blogSelfAdPos() {
	var scrollTop = $(window).scrollTop(),
		elem = $('.self-ad'),
		elemHeight = elem.outerHeight(),
		parent = $('.blog-full-list'),
		parentHeight = parent.outerHeight(),
		parentOffsetTop = parent.offset().top,
		distance = (parentOffsetTop - scrollTop),
		botDistance = (parentHeight + parentOffsetTop - scrollTop - elemHeight - 100);

	if (distance < 100) {
		elem.addClass('abs-pos').css('top', 100);

		if (botDistance < 40) {
			elem.addClass('abs-bot').css('top', 'auto');
		} else {
			elem.removeClass('abs-bot');
		};

	} else {
		elem.removeClass('abs-pos');
	};

	console.log(botDistance);
};