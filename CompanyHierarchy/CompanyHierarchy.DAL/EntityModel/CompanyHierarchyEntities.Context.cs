﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CompanyHierarchy.DAL.EntityModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class CompanyHierarchyEntities : DbContext
    {
        public CompanyHierarchyEntities()
            : base("name=CompanyHierarchyEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeType> EmployeeTypes { get; set; }
        public virtual DbSet<HierarchyConnection> HierarchyConnections { get; set; }
        public virtual DbSet<RelationType> RelationTypes { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<TeamEmployee> TeamEmployees { get; set; }
        public virtual DbSet<TeamLead> TeamLeads { get; set; }
        public virtual DbSet<Hierarchy> Hierarchies { get; set; }
    }
}
