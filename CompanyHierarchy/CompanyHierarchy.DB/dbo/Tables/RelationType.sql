﻿CREATE TABLE [dbo].[RelationType] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_RelationType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

