﻿CREATE TABLE [dbo].[Hierarchy] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (200) NOT NULL,
    [Description]     NVARCHAR (MAX) NULL,
    [CreatedBy]       NVARCHAR (100) NULL,
    [ModifiedBy]      NVARCHAR (100) NULL,
    [ActiveFlag]      BIT            NOT NULL,
    [PendingApproval] BIT            NOT NULL,
    CONSTRAINT [PK_Hierarchy_1] PRIMARY KEY CLUSTERED ([Id] ASC)
);



