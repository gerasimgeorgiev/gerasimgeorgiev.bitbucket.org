﻿CREATE TABLE [dbo].[TeamLead] (
    [Id]                   BIGINT   IDENTITY (1, 1) NOT NULL,
    [TeamFKId]             INT      NOT NULL,
    [TeamLeadEmployeeFKId] BIGINT   NOT NULL,
    [StartDate]            DATETIME NOT NULL,
    [EndDate]              DATETIME NULL,
    [ActiveFlag]           BIT      NOT NULL,
    CONSTRAINT [PK_TeamLead] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TeamLead_Employee] FOREIGN KEY ([TeamLeadEmployeeFKId]) REFERENCES [dbo].[Employee] ([Id]),
    CONSTRAINT [FK_TeamLead_Team] FOREIGN KEY ([TeamFKId]) REFERENCES [dbo].[Team] ([Id])
);

