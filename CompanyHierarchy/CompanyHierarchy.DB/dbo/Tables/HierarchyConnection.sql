﻿CREATE TABLE [dbo].[HierarchyConnection] (
    [Id]                     BIGINT   IDENTITY (1, 1) NOT NULL,
    [HierarchyFKId]          INT      NOT NULL,
    [EmployeeLowerLevelFKId] BIGINT   NOT NULL,
    [EmployeeUpperLevelFKId] BIGINT   NOT NULL,
    [RelationFKId]           INT      NOT NULL,
    [StartDate]              DATETIME NOT NULL,
    [EndDate]                DATETIME NULL,
    CONSTRAINT [PK_Hierarchy] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Hierarchy_Employee] FOREIGN KEY ([EmployeeLowerLevelFKId]) REFERENCES [dbo].[Employee] ([Id]),
    CONSTRAINT [FK_Hierarchy_Employee1] FOREIGN KEY ([EmployeeUpperLevelFKId]) REFERENCES [dbo].[Employee] ([Id]),
    CONSTRAINT [FK_Hierarchy_RelationType] FOREIGN KEY ([RelationFKId]) REFERENCES [dbo].[RelationType] ([Id]),
    CONSTRAINT [FK_HierarchyConnection_Hierarchy] FOREIGN KEY ([HierarchyFKId]) REFERENCES [dbo].[Hierarchy] ([Id])
);

