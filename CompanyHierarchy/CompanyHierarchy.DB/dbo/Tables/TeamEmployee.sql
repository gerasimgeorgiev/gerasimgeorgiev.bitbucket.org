﻿CREATE TABLE [dbo].[TeamEmployee] (
    [Id]           BIGINT   IDENTITY (1, 1) NOT NULL,
    [TeamFKId]     INT      NOT NULL,
    [EmployeeFKId] BIGINT   NOT NULL,
    [StartDate]    DATETIME NOT NULL,
    [EndDate]      DATETIME NULL,
    [ActiveFlag]   BIT      NOT NULL,
    CONSTRAINT [PK_TeamEmployee] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TeamEmployee_Employee] FOREIGN KEY ([EmployeeFKId]) REFERENCES [dbo].[Employee] ([Id]),
    CONSTRAINT [FK_TeamEmployee_Team] FOREIGN KEY ([TeamFKId]) REFERENCES [dbo].[Team] ([Id])
);

