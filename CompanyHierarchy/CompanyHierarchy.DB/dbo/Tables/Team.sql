﻿CREATE TABLE [dbo].[Team] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [HierarchyFKId] INT            NOT NULL,
    [Name]          NVARCHAR (200) NOT NULL,
    [Description]   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Team_Hierarchy] FOREIGN KEY ([HierarchyFKId]) REFERENCES [dbo].[Hierarchy] ([Id])
);



