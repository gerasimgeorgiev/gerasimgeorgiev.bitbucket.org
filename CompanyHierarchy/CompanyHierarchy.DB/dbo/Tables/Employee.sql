﻿CREATE TABLE [dbo].[Employee] (
    [Id]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [EmployeeTypeFKId]  INT            NOT NULL,
    [EmployeeId]        NVARCHAR (50)  NOT NULL,
    [ActiveDirectoryId] NVARCHAR (100) NOT NULL,
    [FirstName]         NVARCHAR (100) NOT NULL,
    [LastName]          NVARCHAR (100) NOT NULL,
    [WorkCity]          NVARCHAR (100) NULL,
    [WorkState]         NVARCHAR (50)  NULL,
    [WorkCountry]       NVARCHAR (50)  NULL,
    [OfficePhone]       NVARCHAR (50)  NULL,
    [CellPhone]         NVARCHAR (50)  NULL,
    [Email]             NVARCHAR (100) NULL,
    [ActiveFlag]        BIT            NOT NULL,
    [AdminFlag]         BIT            NOT NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Employee_EmployeeType] FOREIGN KEY ([EmployeeTypeFKId]) REFERENCES [dbo].[EmployeeType] ([Id])
);

