﻿using System.Collections.Generic;

namespace CompanyHierarchy.Core
{
    public class EmployeeDto
    {
        public long Id { get; set; }
        public int HierarchyId { get; set; }
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeType { get; set; }
        public int EmployeeTypeId { get; set; }
        public long TopLevelEmployeeId { get; set; }
        public long LowerLevelEmployeeId { get; set; }
        public bool IsHierarchyRoot { get; set; }
        public bool IsTeam { get; set; }
        public bool IsTeamMember { get; set; }
        public List<EmployeeDto> TeamMembers { get; set; }
        public string TeamName { get; set; }
        public List<EmployeeDto> ChildElements { get; set; }
    }
}
