﻿using System.Collections.Generic;

namespace CompanyHierarchy.Core
{
    public class HierarchyDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public List<EmployeeDto> HierarchyConnections { get; set; }
    }
}
