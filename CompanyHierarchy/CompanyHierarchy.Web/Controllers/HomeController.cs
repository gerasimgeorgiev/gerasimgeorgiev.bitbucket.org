﻿using CompanyHierarchy.BLL;
using CompanyHierarchy.Core;
using System;
using System.Web.Mvc;

namespace CompanyHierarchy.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetHierarchies()
        {
            try
            {
                var hierarchies = HierarchyActions.GetHierarchies();

                return Json(new { Success = true, Data = hierarchies }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Status = "Error", Data = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddNewHierarchy(EmployeeDto newEmployee, string hierarchyName)
        {
            try
            {
                int hierarchyId = HierarchyActions.AddNewHierarchy(hierarchyName);

                HierarchyActions.AddEditEmployee(newEmployee, hierarchyId);

                return Json(new { Success = true, Data = hierarchyId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Status = "Error", Data = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddEditEmployee(EmployeeDto newEmployee, int hierarchyId)
        {
            try
            {
                HierarchyActions.AddEditEmployee(newEmployee, hierarchyId);

                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Status = "Error", Data = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddEditTeam(EmployeeDto team, int hierarchyId)
        {
            try
            {
                HierarchyActions.AddEditTeam(team, hierarchyId);

                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Status = "Error", Data = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddTeamMember(EmployeeDto newEmployee, int hierarchyId)
        {
            try
            {
                long newEmployeeId = HierarchyActions.AddEditEmployee(newEmployee, hierarchyId, true);

                newEmployee.Id = newEmployeeId;

                HierarchyActions.AddNewTeamMember(newEmployee, hierarchyId);

                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Status = "Error", Data = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteTreeNode(long nodeId, int hierarchyId, bool isTeam)
        {
            try
            {
                HierarchyActions.DeleteTreeNode(nodeId, hierarchyId, isTeam);

                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Status = "Error", Data = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetHierarchy(int hierarchyId)
        {
            try
            {
                var hierarchy = HierarchyActions.GetHierarchy(hierarchyId);
                return Json(new { Success = true, Data = hierarchy }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Status = "Error", Data = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetEmployeeTypes()
        {
            try
            {
                var employeeTypes = HierarchyActions.GetEmployeeTypes();
                return Json(new { Success = true, Data = employeeTypes }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Status = "Error", Data = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}