﻿var operation = "";
var selectedTreeElementId = "";
var isTeamSelected = false;
var selectedTreeNodeData = {};

$("#BtnCreateNewHierarchy").unbind().click(function () {
    operation = "newHierarchy";
    AddNewEmployee();
    $("#TrHierarchyName", "#AddEditEmployeeDialog").show();
});

$("#BtnHierarchiesList").unbind().click(function () {
    GetHierarchies(function (hierarchies) {
        PopulateHierarchies(hierarchies);
    });
});

$("#BtnAddRootElement").unbind().click(function () {
    AddNewEmployee();
});

$("#BtnAddChildEmployee").unbind().click(function () {
    operation = "addChildElement";
    AddNewEmployee();
});

$("#BtnAddParentEmployee").unbind().click(function () {
    operation = "addParentElement";
    AddNewEmployee();
});

$("#BtnAddTeamMember").unbind().click(function () {
    operation = "addTeamMember";
    AddNewEmployee();
});

$("#BtnAddTeam").unbind().click(function () {
    operation = "addTeam";
    AddNewTeam();
});

$("#BtnDeleteTreeNode").unbind().click(function () {
    if (confirm("Are you sure?")) {
        DeleteTreeNode();
    }
});

$("#BtnEditTreeNode").unbind().click(function () {
    EditTreeNode();
});

function GetHierarchies(callback) {
    $.ajax({
        url: directory_root + "Home/GetHierarchies",
        type: "POST",
        contentType: "application/json",
        success: function (response) {
            if (response.Success == true) {
                var hierarchies = response.Data;

                if (callback)
                    callback(hierarchies);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            alert("Error");
        }
    });
}

function PopulateHierarchies(hierarchies) {
    $("#TblHierarchies tr:gt(0)").remove();
    $(hierarchies).each(function (index, hierarchy) {

        $("<tr><td align='left' class='tblCellBorder tblCellWhiteBgColor'><input type='hidden' value='" +
            hierarchy.Id + "'/>" + hierarchy.Name + "</td><td align='left' class='tblCellBorder tblCellWhiteBgColor'>" +
            hierarchy.IsActive + "</td></tr>").appendTo("#TblHierarchies");
    });

    var tblId = "#TblHierarchies";

    $("#TblHierarchies tr:gt(0) td").each(function (index, tblCell) {
        $(tblCell).css("cursor", "pointer");

        $(tblCell).unbind().click(function () {
            var bgColor = $(this).css("backgroundColor");

            if (bgColor == "rgb(255, 255, 255)") {
                // Select row, deselect all others
                $(tblId + " tr:gt(0)").each(function (i, row) {
                    $(this).find("td").css({ backgroundColor: "white" });
                });
                $(tblCell).parent().find("td").css("background-color", "#9FF791");

                hierarchyId = $(this).parent().find("td:first input[type=hidden]").val();
            }
            else if (bgColor == "rgb(159,247,145)") {
                $(tblCell).parent().find("td").css({ backgroundColor: "white" });
            }
        });
    });

    $("#HierarchiesListDialog").dialog({
        width: 400,
        height: 400,
        modal: true,
        buttons: {
            "Load hierarchy": function () {
                assembleHierarchyTree();

                hideButtons();

                $(this).dialog("close");
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });
}

function EditTreeNode() {
    if (isTeamSelected == true) {
        operation = "editTeam";
        AddNewTeam();

        $("#TxtBoxTeamName", "#AddTeamDialog").val(selectedTreeNodeData.TeamName);
        $("#hdnFieldTeamId", "#AddTeamDialog").val(selectedTreeNodeData.Id);
    }
    else {
        operation = "editEmployee";
        AddNewEmployee();

        $("#TxtBoxFirstName", "#AddEditEmployeeDialog").val(selectedTreeNodeData.FirstName);
        $("#TxtBoxLastName", "#AddEditEmployeeDialog").val(selectedTreeNodeData.LastName);
        $("#DropDownEmployeeType", "#AddEditEmployeeDialog").val(selectedTreeNodeData.EmployeeTypeId);
        $("#hdnFieldEmployeeId", "#AddEditEmployeeDialog").val(selectedTreeNodeData.Id);
    }
}

function DeleteTreeNode() {
    $.ajax({
        url: directory_root + "Home/DeleteTreeNode",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
            nodeId: selectedTreeElementId,
            hierarchyId: hierarchyId,
            isTeam: isTeamSelected
        }),
        success: function (response) {
            if (response.Success == true) {
                hideButtons();
                deselectAllRectangles();
                assembleHierarchyTree();
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {

        }
    });
}

function SaveNewEmployee(callback) {
    var employeeData = new Object();
    var firstName = $("#TxtBoxFirstName", "#AddEditEmployeeDialog").val();
    var lastName = $("#TxtBoxLastName", "#AddEditEmployeeDialog").val();
    var employeeTypeId = $("#DropDownEmployeeType", "#AddEditEmployeeDialog").val();
    firstName = $.trim(firstName);
    lastName = $.trim(lastName);

    if (firstName.length > 0 && lastName.length > 0) {
        employeeData.FirstName = firstName;
        employeeData.LastName = lastName;
        employeeData.FirstName = firstName;
        employeeData.EmployeeTypeId = employeeTypeId;

        var url = directory_root + "Home/AddEditEmployee";

        if (operation == "addChildElement" || operation == "addTeamMember") {
            employeeData.TopLevelEmployeeId = selectedTreeElementId;
        }

        if (operation == "addParentElement") {
            employeeData.LowerLevelEmployeeId = selectedTreeElementId;
            employeeData.TopLevelEmployeeId = 0;
        }

        if (operation == "addTeamMember") {
            url = directory_root + "Home/AddTeamMember";
            employeeData.TopLevelEmployeeId = selectedTreeElementId;
        }

        if (operation == "editEmployee") {
            employeeData.Id = $("#hdnFieldEmployeeId", "#AddEditEmployeeDialog").val();
        }

        var requestData = JSON.stringify({
            newEmployee: employeeData,
            hierarchyId: hierarchyId
        });

        if (operation == "newHierarchy") {
            var hierarchyName = $("#TxtBoxHierarchyName", "#AddEditEmployeeDialog").val();
            hierarchyName = $.trim(hierarchyName);

            if (hierarchyName.length == 0) {
                alert("Hierarchy Name is mandatory");
                return;
            }

            requestData = JSON.stringify({
                newEmployee: employeeData,
                hierarchyName: hierarchyName
            });

            url = directory_root + "Home/AddNewHierarchy";
        }

        $.ajax({
            url: url,
            type: "POST",
            contentType: "application/json",
            data: requestData,
            success: function (response) {
                if (response.Success == true) {

                    if (operation == "newHierarchy") {
                        hierarchyId = response.Data;
                    }

                    $("#AddEditEmployeeDialog").dialog("close");
                    hideButtons();
                    deselectAllRectangles();

                    assembleHierarchyTree();
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {

            }
        });
    }
}

function AddNewEmployee() {
    $("#TrHierarchyName", "#AddEditEmployeeDialog").hide();

    $("#TxtBoxFirstName", "#AddEditEmployeeDialog").val("");
    $("#TxtBoxLastName", "#AddEditEmployeeDialog").val("");
    $("#TxtBoxEmployeeId", "#AddEditEmployeeDialog").val("");

    $("#AddEditEmployeeDialog").dialog({
        width: 400,
        height: 250,
        modal: true,
        buttons: {
            "Save": function () {
                SaveNewEmployee(function () {
                    $(this).dialog("close");
                });
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });
}

function AddNewTeam() {
    $("#TxtBoxTeamName", "#AddTeamDialog").val("");

    $("#AddTeamDialog").dialog({
        width: 400,
        height: 170,
        modal: true,
        buttons: {
            "Save": function () {
                SaveNewTeam(function () {
                    $(this).dialog("close");
                });
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });
}

function SaveNewTeam(callback) {
    var teamData = new Object();
    var teamName = $("#TxtBoxTeamName", "#AddTeamDialog").val();
    teamName = $.trim(teamName);

    if (teamName.length > 0) {
        teamData.TeamName = teamName;
        teamData.TopLevelEmployeeId = selectedTreeElementId;

        if (operation == "editTeam") {
            teamData.Id = $("#hdnFieldTeamId", "#AddTeamDialog").val();
        }

        $.ajax({
            url: directory_root + "Home/AddEditTeam",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                team: teamData,
                hierarchyId: hierarchyId
            }),
            success: function (response) {
                if (response.Success == true) {
                    $("#AddTeamDialog").dialog("close");
                    hideButtons();
                    deselectAllRectangles();

                    assembleHierarchyTree();
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {

            }
        });
    }
}