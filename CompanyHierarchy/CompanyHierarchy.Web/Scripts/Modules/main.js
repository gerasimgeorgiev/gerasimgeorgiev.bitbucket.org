﻿var hierarchyId = 0;

var boxWidth = 180,
    boxHeight = 50;

var maxBoxHeight = 0;

// Setup zoom and pan
var zoom = d3.behavior.zoom()
  .scaleExtent([.1, 1])
  .on('zoom', function () {
      svg.attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");
  })
  // Offset so that first pan and zoom does not jump back to the origin
  .translate([150, 200]);

var svg = d3.select("#DivHierarchyContainer").append("svg")
  .attr('width', 1324)
  .attr('height', 700)
  .attr("overflow", "hidden")
  .call(zoom)
  .append('g')
  // Left padding of tree so that the whole root node is on the screen.
  // TODO: find a better way
  .attr("transform", "translate(150,200)");

var tree = d3.layout.tree()
  // Using nodeSize we are able to control
  // the separation between nodes. If we used
  // the size parameter instead then d3 would
  // calculate the separation dynamically to fill
  // the available space.
  .nodeSize([50, 151])
  // By default, cousins are drawn further apart than siblings.
  // By returning the same value in all cases, we draw cousins
  // the same distance apart as siblings.
  .separation(function () {
      return 4;
  })
  // Tell d3 what the child nodes are. Remember, we're drawing
  // a tree so the ancestors are child nodes.
  .children(function (person) {
      return person.ChildElements;
  });

var diagonal = d3.svg.diagonal()
	.projection(function (d) {
	    return [d.x, d.y];
	});

function assembleHierarchyTree() {
    getHierarchy(function (hierarchy) {
        svg.selectAll('g.person').remove();
        svg.selectAll('path.link').remove();

        if (!hierarchy || hierarchy == null || hierarchy ==undefined)
            return;

        $("#spanHierarchyName").text(hierarchy.Name);
        hierarchyId = hierarchy.Id;

        if (hierarchy.HierarchyConnections.length == 0) {
            $("#BtnAddRootElement, #TblTreeNodeOperations").show();
            return;
        }
        else {
            $("#BtnAddRootElement").hide();

            hierarchyId = hierarchy.HierarchyConnections[0].HierarchyId;
        }

        var nodes = tree.nodes(hierarchy.HierarchyConnections[0]);
        var links = tree.links(nodes);

        // Style nodes    
        var node = svg.selectAll("g.person")
            .data(nodes)
          .enter().append("g")
            .attr("class", "person")
            .attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; });

        // Draw the rectangle person boxes
        node.append("rect")
            .attr({
                x: -(boxWidth / 2),
                y: -(boxHeight / 2),
                height: boxHeight,
                cursor:"pointer"
            })
            .attr("width", function (d) {
                return boxWidth;
            })
            .attr("height", function (d) {
                return calculateBoxHeight(d);
            })
            .on({
                "click": function (d) {
                    selectEmployeeNode(d);
                }
            });

        // Draw the person's name and position it inside the box
        node.append("text")
            .attr("dx", -(boxWidth / 2) + 10)
            .attr("dy", -5)
            .attr("text-anchor", "start")
            .attr('class', 'name')
            .attr('cursor', 'pointer')
            .text(function (d) {
                if (d.IsTeam)
                    return d.TeamName;
                else
                    return d.FirstName + " " + d.LastName;
            })
            .on({
                "click": function (d) {
                    selectEmployeeNode(d);
                }
            });

        node.append("text")
                   .attr("dx", -(boxWidth / 2) + 10)
                   .attr("dy", 15)
                   .attr("text-anchor", "start")
                   .attr('cursor', 'pointer')
                   .text(function (d) {
                       if (d.IsTeam == false)
                           return d.EmployeeType;
                   })
                   .on({
                       "click": function (d) {
                           selectEmployeeNode(d);
                       }
                   });

        createTeamMembers(node);

        // Declare the links
        var link = svg.selectAll("path.link")
            .data(links, function (d) { return d.target.Id; });

        // Enter the links.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", diagonal);
    });
}

function selectEmployeeNode(data) {
    hideButtons();
    isTeamSelected = false;
    operation = "";

    d3.selectAll('rect')
        .each(function (d, i) {
            if (d.Id == data.Id) {
                selectedTreeNodeData = data;

                var filledColor = d3.select(this).style("fill");

                if (filledColor == "#fff") {
                    deselectAllRectangles();
                    $("#BtnAddChildEmployee, #BtnDeleteTreeNode, #BtnEditTreeNode, #TblTreeNodeOperations").show();
                    selectedTreeElementId = data.Id;

                    d3.select(this).style("fill", "lightblue");

                    if (data.IsTeam == true) {
                        $("#BtnAddChildEmployee").hide();
                        $("#BtnAddTeamMember").show();
                        isTeamSelected = true;
                    }
                    else {
                        $("#BtnAddTeam").show();

                        if (data.IsHierarchyRoot == true) {
                            $("#BtnAddParentEmployee").show();
                        }
                    }

                    if (data.IsTeamMember == true) {
                        hideButtons();
                        $("#BtnDeleteTreeNode, #BtnEditTreeNode, #TblTreeNodeOperations").show();
                    }
                }
                else {
                    deselectAllRectangles();
                    hideButtons();
                    selectedTreeElementId = "";
                }
            }
        });
}

function hideButtons() {
    $("#TblButtonsLeftNavigation button:gt(1), #TblTreeNodeOperations").hide();
}

function deselectAllRectangles() {
    selectedTreeElementId = "";

    d3.selectAll('rect')
        .each(function (d, i) {
            d3.select(this).style("fill", "#fff");
        });
}

/**
 * Custom path function that creates straight connecting lines.
 */
function elbow(d) {
    return "M" + (d.source.x) + "," + (d.source.y)
        + "V" + ((d.target.y - d.source.y) / 2)
        + "H" + d.target.x
        + "V" + d.target.y;
}

function createTeamMembers(node) {
    var teamMemberNode = node.selectAll("nestedRect")
         .data(function (d) {
             return d.TeamMembers
         })
         .enter();

    teamMemberNode.append("rect")
        .attr({
            x: -(boxWidth / 2) + 20
        })
        .attr('cursor', 'pointer')
        .attr("y", function (d, i) { return (-(boxHeight / 2) + 40 + (i * boxHeight)); })
        .attr("width", function (d) {
            return boxWidth - 40;
        })
        .attr("height", function (d) {
            return 40;
        })
        .on({
            "click": function (d) {
                selectEmployeeNode(d);
            }
        });

    // Employee Name
    teamMemberNode.append("text")
        .attr("dx", -(boxWidth / 2) + 25)
        .attr("dy", function (d, i) { return (-(boxHeight / 2) + 60 + (i * boxHeight)); })
        .attr("text-anchor", "start")
        .attr('cursor', 'pointer')
        .attr('class', 'name')
        .text(function (d) {
            return d.FirstName + " " + d.LastName;
        })
        .on({
            "click": function (d) {
                selectEmployeeNode(d);
            }
        });

    // Employee Type
    teamMemberNode.append("text")
            .attr("dx", -(boxWidth / 2) + 25)
            .attr("dy", function (d, i) { return (-(boxHeight / 2) + 75 + (i * boxHeight)); })
            .attr("text-anchor", "start")
            .attr('cursor', 'pointer')
            .text(function (d) {
                return d.EmployeeType
            })
            .on({
                "click": function (d) {
                    selectEmployeeNode(d);
                }
            });
}

function calculateBoxHeight(data) {
    if (data.IsTeam) {
        var customBoxHeight = (data.TeamMembers.length * boxHeight) + 40;

        if (customBoxHeight > maxBoxHeight)
            maxBoxHeight = customBoxHeight;

        return customBoxHeight;
    }
    else
        return boxHeight;
}

function getHierarchy(callback) {
    $.ajax({
        url: directory_root + "Home/GetHierarchy",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
            hierarchyId: hierarchyId
        }),
        success: function (response) {
            if (response.Success == true) {
                if (callback)
                    callback(response.Data);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            alert("Error");
        }
    });
}

assembleHierarchyTree();

function GetEmployeeTypes() {
    $.ajax({
        url: directory_root + "Home/GetEmployeeTypes",
        type: "POST",
        contentType: "application/json",
        success: function (response) {
            $("#DropDownEmployeeType option", "#AddEditEmployeeDialog").remove();

            if (response.Success == true) {
                var employeeTypes = response.Data;

                $(employeeTypes).each(function (index, employeeType) {
                    $("<option value='" + employeeType.Id + "' selected='selected'>" + employeeType.Name + "</option>").appendTo("#DropDownEmployeeType", "#AddEditEmployeeDialog");
                });
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            alert("Error");
        }
    });
}

GetEmployeeTypes();