﻿using CompanyHierarchy.Core;
using CompanyHierarchy.DAL.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CompanyHierarchy.BLL
{
    public class HierarchyActions
    {
        public static int AddNewHierarchy(string hierarchyName)
        {
            using (CompanyHierarchyEntities dbContext = new CompanyHierarchyEntities())
            {
                var existingHierarchy = dbContext.Hierarchies.FirstOrDefault(m => m.Name == hierarchyName);

                if (existingHierarchy != null)
                    throw new Exception("There is another hierarchy with the same name");

                bool isActive = false;
                var activeHierarchy = dbContext.Hierarchies.FirstOrDefault(m => m.ActiveFlag == true);
                if (activeHierarchy == null)
                    isActive = true;

                var newHierarchy = new Hierarchy()
                {
                    ActiveFlag = isActive,
                    Name = hierarchyName,
                    PendingApproval = true
                };

                dbContext.Hierarchies.Add(newHierarchy);
                dbContext.SaveChanges();

                return newHierarchy.Id;
            }
        }

        public static List<HierarchyDto> GetHierarchies()
        {
            using (CompanyHierarchyEntities dbContext = new CompanyHierarchyEntities())
            {
                var hierarchies = (from hierarchy in dbContext.Hierarchies
                                   select new HierarchyDto()
                                   {
                                       Id = hierarchy.Id,
                                       Name = hierarchy.Name,
                                       IsActive = hierarchy.ActiveFlag
                                   }).ToList();

                return hierarchies;
            }
        }

        public static HierarchyDto GetHierarchy(int hierarchyId = 0)
        {
            using (CompanyHierarchyEntities dbContext = new CompanyHierarchyEntities())
            {
                var hierarchy = hierarchyId == 0 ? dbContext.Hierarchies.FirstOrDefault(m => m.ActiveFlag == true) :
                    dbContext.Hierarchies.FirstOrDefault(n => n.Id == hierarchyId);

                if (hierarchy == null && hierarchyId == 0)
                    return null;

                if (hierarchy == null && hierarchyId > 0)
                    throw new Exception(string.Format("Missing hierarchy with Id {0}", hierarchyId));

                var hierarchyRoot = dbContext.HierarchyConnections.FirstOrDefault(m =>
                    m.EmployeeLowerLevelFKId == m.EmployeeUpperLevelFKId
                    && m.HierarchyFKId == hierarchy.Id);

                if (hierarchyRoot == null)
                {
                    return new HierarchyDto()
                    {
                        Id = hierarchy != null ? hierarchy.Id : 0,
                        Name = hierarchy.Name,
                        HierarchyConnections = new List<EmployeeDto>()
                    };
                }
                else
                {
                    var hierarchyConnections = new List<EmployeeDto>();

                    var rootElement = new EmployeeDto()
                    {
                        Id = hierarchyRoot.EmployeeLowerLevelFKId,
                        FirstName = hierarchyRoot.Employee.FirstName,
                        LastName = hierarchyRoot.Employee.LastName,
                        IsHierarchyRoot = true,
                        HierarchyId = hierarchy.Id,
                        EmployeeType = hierarchyRoot.Employee.EmployeeType.Name,
                        IsTeam = false,
                        TeamMembers = new List<EmployeeDto>(),
                        TeamName = string.Empty
                    };


                    var childElements = GetChildElements(rootElement.Id, hierarchy.Id, dbContext);

                    rootElement.ChildElements = childElements;

                    hierarchyConnections.Add(rootElement);

                    return new HierarchyDto()
                    {
                        Id = hierarchy.Id,
                        Name = hierarchy.Name,
                        HierarchyConnections = hierarchyConnections
                    };
                }
            }
        }

        private static List<EmployeeDto> GetChildElements(long parentElementId, int hierarchyId, CompanyHierarchyEntities dbContext)
        {
            var childElements = (from hierarchyEntry in dbContext.HierarchyConnections
                                 where hierarchyEntry.EmployeeUpperLevelFKId == parentElementId
                                 && hierarchyEntry.EmployeeUpperLevelFKId != hierarchyEntry.EmployeeLowerLevelFKId
                                 && hierarchyEntry.HierarchyFKId == hierarchyId
                                 select new EmployeeDto()
                                 {
                                     Id = hierarchyEntry.EmployeeLowerLevelFKId,
                                     FirstName = hierarchyEntry.Employee.FirstName,
                                     LastName = hierarchyEntry.Employee.LastName,
                                     IsHierarchyRoot = false,
                                     EmployeeType = hierarchyEntry.Employee.EmployeeType.Name,
                                     EmployeeTypeId = hierarchyEntry.Employee.EmployeeTypeFKId,
                                     IsTeam = false,
                                     TeamName = string.Empty,
                                     TopLevelEmployeeId = parentElementId
                                 }).ToList();

            foreach (EmployeeDto entry in childElements)
            {
                entry.ChildElements = new List<EmployeeDto>();
                entry.TeamMembers = new List<EmployeeDto>();

                entry.ChildElements = GetChildElements(entry.Id, hierarchyId, dbContext);
            }

            var teams = (from teamLead in dbContext.TeamLeads
                         where teamLead.TeamLeadEmployeeFKId == parentElementId
                         && teamLead.ActiveFlag == true
                         && teamLead.Team.HierarchyFKId == hierarchyId
                         select new EmployeeDto()
                         {
                             IsTeam = true,
                             TeamName = teamLead.Team.Name,
                             Id = teamLead.Team.Id,
                             IsHierarchyRoot = false,
                             TopLevelEmployeeId = teamLead.TeamLeadEmployeeFKId
                         }).ToList();

            foreach (EmployeeDto team in teams)
            {
                team.ChildElements = new List<EmployeeDto>();
                team.TeamMembers = (from teamMember in dbContext.TeamEmployees
                                    where teamMember.TeamFKId == team.Id 
                                    && teamMember.ActiveFlag == true
                                    && teamMember.Team.HierarchyFKId == hierarchyId
                                    select new EmployeeDto()
                                    {
                                        Id = teamMember.EmployeeFKId,
                                        FirstName = teamMember.Employee.FirstName,
                                        LastName = teamMember.Employee.LastName,
                                        EmployeeType = teamMember.Employee.EmployeeType.Name,
                                        EmployeeTypeId = teamMember.Employee.EmployeeTypeFKId,
                                        IsHierarchyRoot = false,
                                        IsTeam = false,
                                        IsTeamMember = true,
                                        TeamName = string.Empty,
                                        TopLevelEmployeeId = team.Id
                                    }).ToList();
            }

            childElements = childElements.Union(teams).ToList();

            return childElements;
        }

        public static long AddEditEmployee(EmployeeDto employeeData, int hierarchyId, bool addToTeam = false)
        {
            using (CompanyHierarchyEntities dbContext = new CompanyHierarchyEntities())
            {
                var employeeType = dbContext.EmployeeTypes.FirstOrDefault(m => m.Id == employeeData.EmployeeTypeId);

                if (employeeType == null)
                    throw new Exception("Missing Employee Type");

                if (employeeData.Id > 0)
                {
                    // Edit employee
                    var employeeToEdit = dbContext.Employees.FirstOrDefault(m => m.Id == employeeData.Id);

                    if (employeeToEdit != null)
                    {
                        employeeToEdit.FirstName = employeeData.FirstName;
                        employeeToEdit.LastName = employeeData.LastName;
                        employeeToEdit.EmployeeTypeFKId = employeeType.Id;

                        dbContext.SaveChanges();

                        return employeeData.Id;
                    }
                    else
                        throw new Exception("Missing employee");
                }
                else
                {
                    // Add new employee
                    int hierarchyConnections = dbContext.HierarchyConnections.Where(m => m.HierarchyFKId == hierarchyId).Count();

                    var newEmployee = new Employee()
                    {
                        ActiveFlag = true,
                        AdminFlag = false,
                        FirstName = employeeData.FirstName,
                        LastName = employeeData.LastName,
                        EmployeeId = Guid.NewGuid().ToString(),
                        EmployeeTypeFKId = employeeType.Id,
                        ActiveDirectoryId = Guid.NewGuid().ToString()
                    };

                    dbContext.Employees.Add(newEmployee);
                    dbContext.SaveChanges();

                    if (hierarchyConnections == 0)
                    {
                        // This is the hierarchy root employee
                        employeeData.TopLevelEmployeeId = newEmployee.Id;
                    }

                    if (employeeData.TopLevelEmployeeId > 0 && !addToTeam)
                    {
                        var topLevelEmployee = dbContext.Employees.FirstOrDefault(m => m.Id == employeeData.TopLevelEmployeeId);

                        if (topLevelEmployee == null)
                            throw new Exception("Missing top level employee");

                        var hierarchyRelation = dbContext.RelationTypes.FirstOrDefault();

                        if (hierarchyRelation == null)
                            throw new Exception("Missing hierarchy relation entry");

                        var newHierarchyEntry = new HierarchyConnection()
                        {
                            EmployeeLowerLevelFKId = newEmployee.Id,
                            EmployeeUpperLevelFKId = employeeData.TopLevelEmployeeId,
                            RelationFKId = hierarchyRelation.Id,
                            HierarchyFKId = hierarchyId,
                            StartDate = DateTime.Now
                        };

                        dbContext.HierarchyConnections.Add(newHierarchyEntry);
                        dbContext.SaveChanges();
                    }

                    if (employeeData.LowerLevelEmployeeId > 0)
                    {
                        // Add parent element logic
                        var lowerLevelEmployee = dbContext.Employees.FirstOrDefault(m => m.Id == employeeData.LowerLevelEmployeeId);

                        if (lowerLevelEmployee == null)
                            throw new Exception("Missing lower level employee");

                        // Get old root element
                        var hierarchyEntry = dbContext.HierarchyConnections.FirstOrDefault(m =>
                            m.EmployeeLowerLevelFKId == lowerLevelEmployee.Id
                            && m.EmployeeUpperLevelFKId == lowerLevelEmployee.Id
                            && m.HierarchyFKId == hierarchyId);

                        if (hierarchyEntry == null)
                            throw new Exception("Missing hierarchy relation entry for old root element");

                        // Update Upper element Id of the old root
                        hierarchyEntry.EmployeeUpperLevelFKId = newEmployee.Id;

                        var hierarchyRelation = dbContext.RelationTypes.FirstOrDefault();

                        if (hierarchyRelation == null)
                            throw new Exception("Missing hierarchy relation entry");

                        var newHierarchyEntry = new HierarchyConnection()
                        {
                            EmployeeLowerLevelFKId = newEmployee.Id,
                            EmployeeUpperLevelFKId = newEmployee.Id,
                            RelationFKId = hierarchyRelation.Id,
                            HierarchyFKId = hierarchyId,
                            StartDate = DateTime.Now
                        };

                        dbContext.HierarchyConnections.Add(newHierarchyEntry);
                        dbContext.SaveChanges();
                    }

                    return newEmployee.Id;
                }
            }
        }

        public static void AddEditTeam(EmployeeDto newTeam, int hierarchyId)
        {
            using (CompanyHierarchyEntities dbContext = new CompanyHierarchyEntities())
            {
                var existingTeam = dbContext.Teams.FirstOrDefault(m => m.Name == newTeam.TeamName
                && m.HierarchyFKId == hierarchyId);

                if (existingTeam != null)
                    throw new Exception("There is already a team with the same name");

                if (newTeam.Id > 0)
                {
                    // Edit a team
                    var teamToEdit = dbContext.Teams.FirstOrDefault(m => m.Id == newTeam.Id && m.HierarchyFKId == hierarchyId);
                    if (teamToEdit != null)
                    {
                        teamToEdit.Name = newTeam.TeamName;
                        dbContext.SaveChanges();
                    }
                    else
                        throw new Exception("Missing team");
                }
                else
                {
                    // Add new team
                    var newTeamEntry = new Team()
                    {
                        Name = newTeam.TeamName,
                        HierarchyFKId = hierarchyId
                    };

                    dbContext.Teams.Add(newTeamEntry);
                    dbContext.SaveChanges();

                    var newTeamLeadEntry = new TeamLead()
                    {
                        TeamFKId = newTeamEntry.Id,
                        TeamLeadEmployeeFKId = newTeam.TopLevelEmployeeId,
                        ActiveFlag = true,
                        StartDate = DateTime.Now
                    };

                    dbContext.TeamLeads.Add(newTeamLeadEntry);
                    dbContext.SaveChanges();
                }
            }
        }

        public static void AddNewTeamMember(EmployeeDto newTeamMember, int hierarchyId)
        {
            using (CompanyHierarchyEntities dbContext = new CompanyHierarchyEntities())
            {
                var team = dbContext.Teams.FirstOrDefault(m => m.Id == newTeamMember.TopLevelEmployeeId
                && m.HierarchyFKId == hierarchyId);

                if (team == null)
                    throw new Exception("Missing Team");

                var newTeamMemberEntry = new TeamEmployee()
                {
                    TeamFKId = (int)newTeamMember.TopLevelEmployeeId,
                    EmployeeFKId = newTeamMember.Id,
                    StartDate = DateTime.Now,
                    ActiveFlag = true
                };

                dbContext.TeamEmployees.Add(newTeamMemberEntry);
                dbContext.SaveChanges();
            }
        }

        public static void DeleteTreeNode(long nodeId, int hierarchyId, bool isTeam)
        {
            using (CompanyHierarchyEntities dbContext = new CompanyHierarchyEntities())
            {
                if (isTeam)
                {
                    DeleteTeam(nodeId, hierarchyId, dbContext);
                }
                else
                {
                    DeleteChildElement(nodeId, hierarchyId, dbContext);
                }
            }
        }

        private static void DeleteChildElement(long nodeId, int hierarchyId, CompanyHierarchyEntities dbContext)
        {
            var teamIds = dbContext.TeamLeads.Where(m => m.TeamLeadEmployeeFKId == nodeId 
            && m.Team.HierarchyFKId == hierarchyId).Select(n => n.TeamFKId).ToList();

            foreach (int teamId in teamIds)
            {
                DeleteTeam(teamId, hierarchyId, dbContext);
            }

            var hierarchyEntries = dbContext.HierarchyConnections.Where(m => m.EmployeeLowerLevelFKId == nodeId).ToList();

            foreach (HierarchyConnection hierarchyEntry in hierarchyEntries)
            {
                dbContext.HierarchyConnections.Remove(hierarchyEntry);
            }

            var hierarchyEntries2 = dbContext.HierarchyConnections.Where(m => m.EmployeeUpperLevelFKId == nodeId
            && m.EmployeeLowerLevelFKId != m.EmployeeUpperLevelFKId
            && m.HierarchyFKId == hierarchyId).ToList();

            foreach (HierarchyConnection hierarchyEntry in hierarchyEntries2)
            {
                DeleteChildElement(hierarchyEntry.EmployeeLowerLevelFKId, hierarchyId, dbContext);
            }

            var teamMembers = dbContext.TeamEmployees.Where(m => m.EmployeeFKId == nodeId
            && m.Team.HierarchyFKId == hierarchyId).ToList();

            foreach (TeamEmployee teamMember in teamMembers)
            {
                dbContext.TeamEmployees.Remove(teamMember);
            }

            dbContext.SaveChanges();

            var employee = dbContext.Employees.FirstOrDefault(m => m.Id == nodeId);

            if (employee != null)
            {
                dbContext.Employees.Remove(employee);
                dbContext.SaveChanges();
            }
        }

        private static void DeleteTeam(long nodeId, int hierarchyId, CompanyHierarchyEntities dbContext)
        {
            var teamLeadEntry = dbContext.TeamLeads.FirstOrDefault(m => m.TeamFKId == nodeId && m.Team.HierarchyFKId == hierarchyId);

            if (teamLeadEntry != null)
            {
                dbContext.TeamLeads.Remove(teamLeadEntry);
                dbContext.SaveChanges();
            }

            var teamMembers = dbContext.TeamEmployees.Where(m => m.TeamFKId == nodeId && m.Team.HierarchyFKId == hierarchyId).ToList();

            foreach (TeamEmployee teamMember in teamMembers)
            {
                dbContext.TeamEmployees.Remove(teamMember);
            }
            dbContext.SaveChanges();

            var teamMembersIds = teamMembers.Select(n => n.EmployeeFKId).ToList();

            foreach (long teamMemberId in teamMembersIds)
            {
                DeleteChildElement(teamMemberId, hierarchyId, dbContext);
            }

            var teamToDelete = dbContext.Teams.FirstOrDefault(m => m.Id == nodeId && m.HierarchyFKId == hierarchyId);

            if (teamToDelete != null)
            {
                dbContext.Teams.Remove(teamToDelete);
                dbContext.SaveChanges();
            }
        }

        public static List<EmployeeTypeDto> GetEmployeeTypes()
        {
            using (CompanyHierarchyEntities dbContext = new CompanyHierarchyEntities())
            {
                var employeeTypes = (from employeeType in dbContext.EmployeeTypes
                                     select new EmployeeTypeDto()
                                     {
                                         Id = employeeType.Id,
                                         Name = employeeType.Name
                                     }).ToList();

                return employeeTypes;
            }
        }
    }
}