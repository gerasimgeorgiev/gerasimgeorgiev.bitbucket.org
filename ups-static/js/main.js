(function($){ 
	"use strict";

	//toggling sidebar on button click
	$('.js-sidebar-control').on('click', function(e) {
		e.preventDefault();
		$('.sidebar').toggleClass('opened');
		$('body').toggleClass('sidebar-opened');
	});

	$('.js-modal-handle').on('click', function(e) {
		e.preventDefault();

		var self = $(this),
			dataModal = self.attr('data-modal-handle'),
			modalToOpen = $('.modal-wrapper[data-modal-action="' + dataModal + '"]');

		if (modalToOpen.hasClass('modal-in-content')) {
			var anchorTarget = $('.modal-in-content-anchor[data-modal-handle="' + dataModal + '"]'),
				anchorWidth = anchorTarget.outerWidth(),
				anchorHeight = anchorTarget.outerHeight(),
				anchorOffset = anchorTarget.offset(),
				anchorOffsetTop = anchorOffset.top,
				anchorOffsetLeft = anchorOffset.left,
				windowScrollTop = $(window).scrollTop(),
				windowWidth = $(window).width();

			var finalPositionTop = ((anchorHeight + anchorOffsetTop) - windowScrollTop + 2),
				finalPositionRight = (windowWidth - (anchorWidth + anchorOffsetLeft));

			var modalToOpenInner = modalToOpen.find('.modal-wrapper-inner');

			modalToOpenInner.css({
				'top': finalPositionTop,
				'right': finalPositionRight,
				'left': 'auto',
			});
		};

		if (modalToOpen.length) {
			modalToOpen.addClass('opened');
		};
		$('html').addClass('modal-active');
	});

	$('.js-modal-close').on('click', function(e) {
		e.preventDefault();

		var self = $(this);

		self.closest('.modal-wrapper').removeClass('opened');
		$('html').removeClass('modal-active');
	});
	
	// file upload from input directly
	$('input[type="file"].direct-upload').on('change', function() {

		var self = $(this),
			fileInfo = self.val().split('\\').pop(),
			fileName = fileInfo.split('.')[0],
			fileType = fileInfo.split('.')[1];

		if (self.hasClass('upload-from-modal')) {
			self.closest('.modal-wrapper').find('.js-modal-close').trigger('click');
		};

		var tableRowNew = $('.table-standart .new-row').clone();

		tableRowNew.prependTo('.table-standart tbody');
		tableRowNew.find('.asset-name').text(fileName);
		tableRowNew.find('.asset-kind').text(fileType);

		var d = new Date();
		tableRowNew.find('.asset-upload-time').text(d);
		tableRowNew.find('.asset-upload-time').attr('datetime', d);

		tableRowNew.removeClass('new-row');
	});

	// rename folder
	$('.asset-link-action[data-asset-link-action="rename"]').on('click', function(e) {
		e.preventDefault();

		var self = $(this),
			parent = self.closest('tr'),
			assetName = parent.find('.asset-name'),
			assetNameText = assetName.text();

		if (!assetName.hasClass('renaming')) {
			assetName.html('<input type="text" class="input-rename-asset" value="' + assetNameText + '">');
			assetName.addClass('renaming');
		} else {
			return false;
		}
	});

	// submitting new name on enter press
	$('.asset-name').on('keyup', '.input-rename-asset', function(e) {

		var self = $(this),
			selfValue = self.val();

		if (e.keyCode == 13) {
			self.parent('.asset-name').text(selfValue);
		};
	});

	// grid menu handle
	$('.js-top-bar-inner-menu-handle').on('click', function(e) {
		e.preventDefault();

		var self = $(this),
			gridMenuWrapper = self.closest('.top-bar-inner-menu-wrapper');


		if (gridMenuWrapper.hasClass('inner-menu-opened')) {
			gridMenuWrapper.removeClass('inner-menu-opened');
		} else {
			$('.top-bar').find('.inner-menu-opened').removeClass('inner-menu-opened');
			gridMenuWrapper.addClass('inner-menu-opened');
		};
	});

	// pin button
	$('.btn-add-to-favorites').on('click', function(e) {
		e.preventDefault();

		var self = $(this),
			pinTarget = self.closest('.pin-target');

		if (self.hasClass('pinned')) {
			// unpin functionality here
			pinTarget.remove();
		} else {
			// pin functionality here
		};
	});

	// custom tooltip showing general code
	$('.js-custom-tooltip-show').on('mouseenter', function() {
		var self = $(this),
			tooltip = $('.custom-tooltip'),
			selfWidth = self.outerWidth(),
			selfHeight = self.outerHeight(),
			selfOffset = self.offset(),
			selfOffsetTop = selfOffset.top,
			selfOffsetLeft = selfOffset.left,
			windowScrollTop = $(window).scrollTop(),
			windowWidth = $(window).width();

		var finalPositionTop = (selfOffsetTop - 15),
		finalPositionLeft = ((selfWidth + selfOffsetLeft) - (selfWidth/2));

		tooltip.css({
			'top': finalPositionTop,
			'left': finalPositionLeft,
		});

		tooltip.addClass('shown');
	});

	$('.custom-tooltip-close').on('click', function() {
		var self = $(this),
			tooltip = self.closest('.custom-tooltip');
		
			tooltip.removeClass('shown');
	});

	// custom dropdown - selectir plugin used

	$('.js-selectric-dd').selectric({
		arrowButtonMarkup: '<span class="button icon"><img src="img/icons/icon-arrow-down-brown.png" alt=""></span>',
	});

	// datepicker functionality
	$('[data-toggle="datepicker"]').datepicker({
		date: new Date(2018, 3, 8),
	});

	// bar chart in DC page
	if ($('.graph-container-inner-canvas-outbound.bar-chart').length) {
		var ctx = $('.graph-container-inner-canvas-outbound.bar-chart'),
			pWidth = ctx.closest('.graph-container-inner-outbound').width(),
			pHeight = ctx.closest('.graph-container-inner-outbound').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Berlingske Sans';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: [
					["MON", "04/02"],
					["TUE", "04/03"],
					["WED", "04/04"],
					["THU", "04/05"],
					["FRI", "04/06"],
					["SAT", "04/07"],
					["SUN", "04/08"],
				],
		        datasets: [{
		        	label: 'Previous week',
		            data: [1986, 600, 400, 200, 1800, 1200, 980],
		            backgroundColor: [
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)'
		            ],
		            borderWidth: 0
		        },{
					label: 'Current week',
		            data: [1086, 200, 630, 210, 800, 1000, 680],
		            backgroundColor: [
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)'
		            ],
		            borderWidth: 0
				}]
		    },
		    options: {
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
					display: true,
					position: 'bottom',
					labels: {
						boxWidth: 12,
						fontSize: 13,
						fontColor: '#000000',
						fontFamily: 'Berlingske Sans',
					}
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: true,
		    		mode: 'index',
					intersect: false,
					titleFontFamily: 'Berlingske Sans',
		    	},
		        scales: {
		        	xAxes: [{
						categoryPercentage: 0.4,
            			barPercentage: 0.8,
		        		gridLines: {
		            		display: false,
							drawTicks: false,
							tickMarkLength: 20,
		            	},
		            	ticks: {
		                	fontStyle: 'normal',
							fontSize: 14,
							fontFamily: 'Berlingske Sans',
							fontColor: '#656d78',
		                },
						barThickness: 16
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.28)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 12,
							fontStyle: 'normal',
							fontFamily: 'Berlingske Sans',
		                	min: 0,
		                	max: 2000,
		                	maxTicksLimit: 4,
		                	stepSize: 500,
		                    beginAtZero: true
		                }
		            }]
		        }
			}
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

	if ($('.graph-container-inner-canvas-inbound.bar-chart').length) {
		var ctx = $('.graph-container-inner-canvas-inbound.bar-chart'),
			pWidth = ctx.closest('.graph-container-inner-inbound').width(),
			pHeight = ctx.closest('.graph-container-inner-inbound').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Berlingske Sans';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: [
					["MON", "04/02"],
					["TUE", "04/03"],
					["WED", "04/04"],
					["THU", "04/05"],
					["FRI", "04/06"],
					["SAT", "04/07"],
					["SUN", "04/08"],
				],
		        datasets: [{
		        	label: 'Previous week',
		            data: [1986, 600, 400, 200, 1800, 1200, 980],
		            backgroundColor: [
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)'
		            ],
		            borderWidth: 0
		        },{
					label: 'Current week',
		            data: [1086, 200, 630, 210, 800, 1000, 680],
		            backgroundColor: [
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)'
		            ],
		            borderWidth: 0
				}]
		    },
		    options: {
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
					display: true,
					position: 'bottom',
					labels: {
						boxWidth: 12,
						fontSize: 13,
						fontColor: '#000000',
						fontFamily: 'Berlingske Sans',
					}
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: true,
		    		mode: 'index',
					intersect: false,
					titleFontFamily: 'Berlingske Sans',
		    	},
		        scales: {
		        	xAxes: [{
						categoryPercentage: 0.4,
            			barPercentage: 0.8,
		        		gridLines: {
		            		display: false,
							drawTicks: false,
							tickMarkLength: 20,
		            	},
		            	ticks: {
		                	fontStyle: 'normal',
							fontSize: 14,
							fontFamily: 'Berlingske Sans',
							fontColor: '#656d78',
		                },
						barThickness: 16
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.28)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 12,
							fontStyle: 'normal',
							fontFamily: 'Berlingske Sans',
		                	min: 0,
		                	max: 2000,
		                	maxTicksLimit: 4,
		                	stepSize: 500,
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

	if ($('.graph-container-inner-canvas-inventory.bar-chart').length) {
		var ctx = $('.graph-container-inner-canvas-inventory.bar-chart'),
			pWidth = ctx.closest('.graph-container-inner-inventory').width(),
			pHeight = ctx.closest('.graph-container-inner-inventory').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Berlingske Sans';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: [
					["MON", "04/02"],
					["TUE", "04/03"],
					["WED", "04/04"],
					["THU", "04/05"],
					["FRI", "04/06"],
					["SAT", "04/07"],
					["SUN", "04/08"],
				],
		        datasets: [{
		        	label: 'Pieces Counted',
		            data: [2000, 800, 600, 400, 1900, 1400, 1280],
		            backgroundColor: [
		                'rgba(140, 140, 140, 0.9)',
		                'rgba(140, 140, 140, 0.9)',
		                'rgba(140, 140, 140, 0.9)',
		                'rgba(140, 140, 140, 0.9)',
		                'rgba(140, 140, 140, 0.9)',
		                'rgba(140, 140, 140, 0.9)',
		                'rgba(140, 140, 140, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(140, 140, 140, 1)',
		            	'rgba(140, 140, 140, 1)',
		            	'rgba(140, 140, 140, 1)',
		            	'rgba(140, 140, 140, 1)',
		            	'rgba(140, 140, 140, 1)',
		            	'rgba(140, 140, 140, 1)',
		            	'rgba(140, 140, 140, 1)'
		            ],
		            borderWidth: 0
		        },{
		        	label: 'Minus',
		            data: [1986, 600, 400, 200, 1800, 1200, 980],
		            backgroundColor: [
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)',
		                'rgba(150, 188, 233, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)',
		            	'rgba(150, 188, 233, 1)'
		            ],
		            borderWidth: 0
		        },{
					label: 'Surplus',
		            data: [1086, 200, 630, 210, 800, 1000, 680],
		            backgroundColor: [
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)',
		                'rgba(184, 233, 134, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)',
		            	'rgba(184, 233, 134, 1)'
		            ],
		            borderWidth: 0
				}]
		    },
		    options: {
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
					display: true,
					position: 'bottom',
					labels: {
						boxWidth: 12,
						fontSize: 13,
						fontColor: '#000000',
						fontFamily: 'Berlingske Sans',
					}
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: true,
		    		mode: 'index',
					intersect: false,
					titleFontFamily: 'Berlingske Sans',
		    	},
		        scales: {
		        	xAxes: [{
						categoryPercentage: 0.68,
            			barPercentage: 0.8,
		        		gridLines: {
		            		display: false,
							drawTicks: false,
							tickMarkLength: 20,
		            	},
		            	ticks: {
		                	fontStyle: 'normal',
							fontSize: 14,
							fontFamily: 'Berlingske Sans',
							fontColor: '#656d78',
		                },
						barThickness: 16
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.28)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 12,
							fontStyle: 'normal',
							fontFamily: 'Berlingske Sans',
		                	min: 0,
		                	max: 2000,
		                	maxTicksLimit: 4,
		                	stepSize: 500,
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

	// bar chart in homepage
	if ($('.homepage-graph-canvas.bar-chart1').length) {
		var ctx = $('.homepage-graph-canvas.bar-chart1'),
			pWidth = ctx.closest('.homepage-graph-container1').width(),
			pHeight = ctx.closest('.homepage-graph-container1').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Berlingske Sans';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["04/01", "04/02", "04/03", "04/04", "04/05", "04/06"],
		        datasets: [{
					label: 'Ontime Order',
		            data: [400, 200, 180, 420, 270, 370],
		            backgroundColor: [
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)'
		            ],
		            borderWidth: 0
				}]
		    },
		    options: {
				layout: {
					padding: {
						bottom: 10,
					}
				},
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
					display: false,
					position: 'bottom',
					labels: {
						boxWidth: 12,
						fontSize: 13,
						fontColor: '#000000',
						fontFamily: 'Berlingske Sans',
					}
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: true,
		    		mode: 'index',
					intersect: false,
					titleFontFamily: 'Berlingske Sans',
		    	},
		        scales: {
		        	xAxes: [{
		        		gridLines: {
		            		display: false,
							drawTicks: false,
		            	},
		            	ticks: {
		                	fontStyle: 'bold',
							fontSize: 10,
							fontFamily: 'Berlingske Sans',
		                },
						barThickness: 20
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.58)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 10,
							fontStyle: 'bold',
							fontFamily: 'Berlingske Sans',
		                	min: 0,
		                	max: 500,
		                	maxTicksLimit: 4,
		                	stepSize: 250,
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

	if ($('.homepage-graph-canvas.bar-chart2').length) {
		var ctx = $('.homepage-graph-canvas.bar-chart2'),
			pWidth = ctx.closest('.homepage-graph-container2').width(),
			pHeight = ctx.closest('.homepage-graph-container2').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Berlingske Sans';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["04/01", "04/02", "04/03", "04/04", "04/05", "04/06"],
		        datasets: [{
					label: 'Dock-to-stock Net Performance',
		            data: [400, 200, 180, 420, 270, 370],
		            backgroundColor: [
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)'
		            ],
		            borderWidth: 0
				}]
		    },
		    options: {
				layout: {
					padding: {
						bottom: 10,
					}
				},
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
					display: false,
					position: 'bottom',
					labels: {
						boxWidth: 12,
						fontSize: 13,
						fontColor: '#000000',
						fontFamily: 'Berlingske Sans',
					}
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: true,
		    		mode: 'index',
					intersect: false,
					titleFontFamily: 'Berlingske Sans',
		    	},
		        scales: {
		        	xAxes: [{
		        		gridLines: {
		            		display: false,
							drawTicks: false,
		            	},
		            	ticks: {
		                	fontStyle: 'bold',
							fontSize: 10,
							fontFamily: 'Berlingske Sans',
		                },
						barThickness: 20
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.58)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 10,
							fontStyle: 'bold',
							fontFamily: 'Berlingske Sans',
		                	min: 0,
		                	max: 500,
		                	maxTicksLimit: 4,
		                	stepSize: 250,
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

	if ($('.homepage-graph-canvas.bar-chart3').length) {
		var ctx = $('.homepage-graph-canvas.bar-chart3'),
			pWidth = ctx.closest('.homepage-graph-container3').width(),
			pHeight = ctx.closest('.homepage-graph-container3').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Berlingske Sans';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["04/01", "04/02", "04/03", "04/04", "04/05", "04/06"],
		        datasets: [{
					label: 'Orders Cycle Count',
		            data: [400, 200, 180, 420, 270, 370],
		            backgroundColor: [
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)',
						'rgba(180, 100, 32, 0.9)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)',
		            	'rgba(180, 100, 32, 1)'
		            ],
		            borderWidth: 0
				}]
		    },
		    options: {
				layout: {
					padding: {
						bottom: 10,
					}
				},
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
					display: false,
					position: 'bottom',
					labels: {
						boxWidth: 12,
						fontSize: 13,
						fontColor: '#000000',
						fontFamily: 'Berlingske Sans',
					}
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: true,
		    		mode: 'index',
					intersect: false,
					titleFontFamily: 'Berlingske Sans',
		    	},
		        scales: {
		        	xAxes: [{
		        		gridLines: {
		            		display: false,
							drawTicks: false,
		            	},
		            	ticks: {
		                	fontStyle: 'bold',
							fontSize: 10,
							fontFamily: 'Berlingske Sans',
		                },
						barThickness: 20
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.58)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 10,
							fontStyle: 'bold',
							fontFamily: 'Berlingske Sans',
		                	min: 0,
		                	max: 500,
		                	maxTicksLimit: 4,
		                	stepSize: 250,
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

	// line chart
	if ($('.graph-container-inner-canvas-outbound.line-chart').length) {
		var ctx = $('.graph-container-inner-canvas-outbound.line-chart'),
			pWidth = ctx.closest('.graph-container-inner-outbound').width(),
			pHeight = ctx.closest('.graph-container-inner-outbound').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Berlingske Sans';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'line',
		    data: {
		        labels: [
					["MON", "04/02"],
					["TUE", "04/03"],
					["WED", "04/04"],
					["THU", "04/05"],
					["FRI", "04/06"],
					["SAT", "04/07"],
					["SUN", "04/08"],
				],
		        datasets: [{
		        	label: 'Received < order Cut-off time',
					data: [750, 1000, 500, 790, 600, 490, 990],
					borderColor: '#e4e4e4',
					backgroundColor: 'rgba(255, 255, 255, 0)',
					borderDash: [10],
					pointBackgroundColor: '#e4e4e4',
					pointStyle: 'rect',
					lineTension: 0,
					pointRadius: 4,
					pointHoverRadius: 8,
		            borderWidth: 2
		        },{
		        	label: 'Lines shipped on time',
					data: [500, 740, 670, 590, 530, 656, 980],
					borderColor: '#7ed321',
					backgroundColor: 'rgba(255, 255, 255, 0)',
					pointBackgroundColor: '#7ed321',
					pointStyle: 'triangle',
					lineTension: 0,
					pointRadius: 4,
					pointHoverRadius: 8,
		            borderWidth: 2
		        },{
					label: 'Controllable',
					data: [500, 970, 660, 500, 750, 750, 1000],
					borderColor: '#f94e4e',
					backgroundColor: 'rgba(255, 255, 255, 0)',
					pointBackgroundColor: '#f94e4e',
					pointStyle: 'circle',
					lineTension: 0,
					pointRadius: 3,
					pointHoverRadius: 7,
		            borderWidth: 2
				},{
					label: 'Non Controllable',
					data: [800, 590, 390, 670, 890, 760, 680],
					borderColor: '#fff797',
					backgroundColor: 'rgba(255, 255, 255, 0)',
					pointBackgroundColor: '#fff797',
					pointStyle: 'rectRot',
					lineTension: 0,
					pointRadius: 5,
					pointHoverRadius: 10,
		            borderWidth: 2
				}]
		    },
		    options: {
				bezierCurve: false,
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
					display: true,
					position: 'bottom',
					labels: {
						usePointStyle: true,
						boxWidth: 12,
						fontSize: 13,
						fontColor: '#000000',
						fontFamily: 'Berlingske Sans',
					}
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: true,
		    		mode: 'index',
					intersect: false,
					titleFontFamily: 'Berlingske Sans',
		    	},
		        scales: {
		        	xAxes: [{
		        		gridLines: {
		            		display: false,
							drawTicks: false,
							tickMarkLength: 20,
		            	},
		            	ticks: {
		                	fontStyle: 'normal',
							fontSize: 14,
							fontFamily: 'Berlingske Sans',
							fontColor: '#656d78',
		                },
						barThickness: 12
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.28)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 12,
							fontStyle: 'normal',
							fontFamily: 'Berlingske Sans',
		                	min: 0,
		                	max: 2000,
		                	maxTicksLimit: 4,
		                	stepSize: 500,
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

	if ($('.graph-container-inner-canvas-inbound.line-chart').length) {
		var ctx = $('.graph-container-inner-canvas-inbound.line-chart'),
			pWidth = ctx.closest('.graph-container-inner-inbound').width(),
			pHeight = ctx.closest('.graph-container-inner-inbound').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Berlingske Sans';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'line',
		    data: {
		        labels: [
					["MON", "04/02"],
					["TUE", "04/03"],
					["WED", "04/04"],
					["THU", "04/05"],
					["FRI", "04/06"],
					["SAT", "04/07"],
					["SUN", "04/08"],
				],
		        datasets: [{
		        	label: 'Lines shipped on time',
					data: [500, 740, 670, 590, 530, 656, 980],
					borderColor: '#7ed321',
					backgroundColor: 'rgba(255, 255, 255, 0)',
					pointBackgroundColor: '#7ed321',
					pointStyle: 'triangle',
					lineTension: 0,
					pointRadius: 4,
					pointHoverRadius: 8,
		            borderWidth: 2
		        },{
					label: 'Controllable',
					data: [500, 970, 660, 500, 750, 750, 1000],
					borderColor: '#f94e4e',
					backgroundColor: 'rgba(255, 255, 255, 0)',
					pointBackgroundColor: '#f94e4e',
					pointStyle: 'circle',
					lineTension: 0,
					pointRadius: 3,
					pointHoverRadius: 7,
		            borderWidth: 2
				},{
					label: 'Non Controllable',
					data: [800, 590, 390, 670, 890, 760, 680],
					borderColor: '#fff797',
					backgroundColor: 'rgba(255, 255, 255, 0)',
					pointBackgroundColor: '#fff797',
					pointStyle: 'rectRot',
					lineTension: 0,
					pointRadius: 5,
					pointHoverRadius: 10,
		            borderWidth: 2
				}]
		    },
		    options: {
				bezierCurve: false,
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
					display: true,
					position: 'bottom',
					labels: {
						usePointStyle: true,
						boxWidth: 12,
						fontSize: 13,
						fontColor: '#000000',
						fontFamily: 'Berlingske Sans',
					}
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: true,
		    		mode: 'index',
					intersect: false,
					titleFontFamily: 'Berlingske Sans',
		    	},
		        scales: {
		        	xAxes: [{
		        		gridLines: {
		            		display: false,
							drawTicks: false,
							tickMarkLength: 20,
		            	},
		            	ticks: {
		                	fontStyle: 'normal',
							fontSize: 14,
							fontFamily: 'Berlingske Sans',
							fontColor: '#656d78',
		                },
						barThickness: 12
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.28)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 12,
							fontStyle: 'normal',
							fontFamily: 'Berlingske Sans',
		                	min: 0,
		                	max: 2000,
		                	maxTicksLimit: 4,
		                	stepSize: 500,
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

	if ($('.graph-container-inner-canvas-inventory.line-chart').length) {
		var ctx = $('.graph-container-inner-canvas-inventory.line-chart'),
			pWidth = ctx.closest('.graph-container-inner-inventory').width(),
			pHeight = ctx.closest('.graph-container-inner-inventory').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Berlingske Sans';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'line',
		    data: {
		        labels: [
					["MON", "04/02"],
					["TUE", "04/03"],
					["WED", "04/04"],
					["THU", "04/05"],
					["FRI", "04/06"],
					["SAT", "04/07"],
					["SUN", "04/08"],
				],
		        datasets: [{
					label: 'SKU\'s Counted',
					data: [500, 970, 660, 500, 750, 750, 1000],
					borderColor: '#f94e4e',
					backgroundColor: 'rgba(255, 255, 255, 0)',
					pointBackgroundColor: '#f94e4e',
					pointStyle: 'circle',
					lineTension: 0,
					pointRadius: 3,
					pointHoverRadius: 7,
		            borderWidth: 2
				},{
					label: 'SKU\'s Differences',
					data: [800, 590, 390, 670, 890, 760, 680],
					borderColor: '#fff797',
					backgroundColor: 'rgba(255, 255, 255, 0)',
					pointBackgroundColor: '#fff797',
					pointStyle: 'rectRot',
					lineTension: 0,
					pointRadius: 5,
					pointHoverRadius: 10,
		            borderWidth: 2
				}]
		    },
		    options: {
				bezierCurve: false,
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
					display: true,
					position: 'bottom',
					labels: {
						usePointStyle: true,
						boxWidth: 12,
						fontSize: 13,
						fontColor: '#000000',
						fontFamily: 'Berlingske Sans',
					}
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: true,
		    		mode: 'index',
					intersect: false,
					titleFontFamily: 'Berlingske Sans',
		    	},
		        scales: {
		        	xAxes: [{
		        		gridLines: {
		            		display: false,
							drawTicks: false,
							tickMarkLength: 20,
		            	},
		            	ticks: {
		                	fontStyle: 'normal',
							fontSize: 14,
							fontFamily: 'Berlingske Sans',
							fontColor: '#656d78',
		                },
						barThickness: 12,
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.28)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 12,
							fontStyle: 'normal',
							fontFamily: 'Berlingske Sans',
		                	min: 0,
		                	max: 2000,
		                	maxTicksLimit: 4,
		                	stepSize: 500,
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

})(jQuery);
