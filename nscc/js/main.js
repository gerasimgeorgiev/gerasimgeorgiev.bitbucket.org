(function($){ 
	"use strict";

	//toggling sidebar on button click
	$('.js-sidebar-control').on('click', function(e) {
		e.preventDefault();
		$('.sidebar').toggleClass('opened');
	});

	$(window).resize(function() {
		//fixing tooltip box stretching page issue
		titleTooltipBox.css({
			'top': '',
			'left': '',
		});
	});

	//custom tooltip (chrome-like)
	var titleTooltipBox = $('.title-tooltip-box');

	var mouseTooltipTimer;

	$('body').on('mousemove', '*[data-title]', function(e) {
		var self = $(this),
			title = self.attr('data-title');

		clearInterval(mouseTooltipTimer);

		mouseTooltipTimer = setInterval(function() {
			if (self.is(':hover')) {

				var titleTopPosition = e.pageY,
					titleLeftPosition = e.pageX;

				titleTooltipBox.text(title);
				titleTooltipBox.css({
					'top': titleTopPosition + 22,
					'left': titleLeftPosition,
				});
				titleTooltipBox.addClass('shown');
			};
		}, 500);
	});

	$('body').on('mouseleave', '*[data-title]', function() {

		clearInterval(mouseTooltipTimer);
		titleTooltipBox.removeClass('shown');
		setTimeout(function() {
			titleTooltipBox.text('');
		}, 220);
	});

	//modal control
	// open modal
    $('body').on("click", ".js-modal-open", function () {
        var self = $(this);
        var btnName = self.attr('name');

        if (!self.hasClass('inactive')) {
        	$('.' + btnName).fadeIn(200);
        };
    });

	// close modal
    $('.js-modal-close').click(function () {
        var self = $(this);
        self.parents('.modal-bg-wrapper').fadeOut(200);
    });

    //closing modal on outside click
    $('.modal-bg-wrapper').on('click', function(e){
		var target = e.target;
	    if (!$(target).is('.modal-inner-wrapper *') && !$(target).is('.modal-inner-wrapper')) {
	    	$(this).fadeOut(200);
	    };
	});

    //modal approve check on btn click
	$('.modal-approve .btn-approve').on('click', function(e) {
		var self = $(this),
			parent = self.closest('.modal-approve'),
			inputVal = parent.find('.input-split-percent').val(),
			errorText = parent.find('.error-txt'),
			personPercents = parent.find('.person-wrap .num-percent'),
			percentResult = 0;

		personPercents.each(function() {
			var self = $(this),
				selfText = self.text();

			percentResult = percentResult + parseInt(selfText);
		});

		if (inputVal.length === 0) {
			errorText.text('Please enter Split %').addClass('shown');
			e.preventDefault();
		} else {

			if (parseInt(inputVal) + parseInt(percentResult) > 100) {
				errorText.text('The total split % cannot exceed 100%').addClass('shown');
				e.preventDefault();
			};
		};
	});

	//modal reject check on btn click
	$('.modal-reject .btn-reject').on('click', function(e) {
		var self = $(this),
		parent = self.closest('.modal-reject'),
		inputVal = parent.find('.input-reject-reason').val(),
		errorText = parent.find('.error-txt');

		if (inputVal.length === 0) {
			errorText.text('Please enter a rejection reason.').addClass('shown');
			e.preventDefault();
		}
	})

	//custom dropdown implementation
	$('.custom-dropdown select').select2({
		minimumResultsForSearch: Infinity,
	});

	//custom scrollbars
	$('.customer-tile .info').mCustomScrollbar({
		theme: 'blue',
	});

	//custom scrollbar for table in approver approve edit - with sticky header
	var tableScrollableHead = $('.table-scrollable.table-approver-edit-wrap thead'),
		tableScrollableDragger,
		tableScrollableTopPosition,
		tableScrollableHeadFinalPosition;
	$('.table-scrollable.table-approver-edit-wrap').mCustomScrollbar({
		theme: 'blue',
		callbacks: {
			onCreate: function() {
				//scrollable element
				tableScrollableDragger = $('.table-scrollable.table-approver-edit-wrap').find('.mCSB_container');
			},
			whileScrolling: function() {
				//getting scrollable element position from top
				tableScrollableTopPosition = tableScrollableDragger.position().top;
				//multiplying by -1 because the value is negative
				tableScrollableHeadFinalPosition = 'translate(0px, ' + tableScrollableTopPosition * (-1) + 'px)';
				//applying the final top distance value to the head ot the table
				tableScrollableHead.css('transform', tableScrollableHeadFinalPosition);
			},
		},
	});

	//custom scrollbar for table in admin finalize data page - with sticky header
	var tableScrollableHead = $('.table-scrollable.table-admin-finalize-wrap thead'),
		tableScrollableDragger,
		tableScrollableTopPosition,
		tableScrollableHeadFinalPosition;
	$('.table-scrollable.table-admin-finalize-wrap').mCustomScrollbar({
		theme: 'blue',
		callbacks: {
			onCreate: function() {
				//scrollable element
				tableScrollableDragger = $('.table-scrollable.table-admin-finalize-wrap').find('.mCSB_container');
			},
			whileScrolling: function() {
				//getting scrollable element position from top
				tableScrollableTopPosition = tableScrollableDragger.position().top;
				//multiplying by -1 because the value is negative
				tableScrollableHeadFinalPosition = 'translate(0px, ' + tableScrollableTopPosition * (-1) + 'px)';
				//applying the final top distance value to the head ot the table
				tableScrollableHead.css('transform', tableScrollableHeadFinalPosition);
			},
		},
	});

	//talbe with collapsable rows
	$('.table-collapsed-rows .collapsed-children .name').on('click', function(e) {
		var self = $(this),
			target = e.target,
			parent = self.closest('.collapsed-children'),
			siblingRows = parent.nextUntil('tr:not(.collapsable)');

		if (!$(target).is('.percent-small')) {
			parent.toggleClass('expanded-children');
			siblingRows.toggleClass('collapsed');
		};
	});

	//input clear button display control
	$('.search-bar-wrap input').on('keyup', function() {
		var self = $(this),
			val = self.val(),
			btnClear = self.siblings('.btn-clear');

		if (val == '') {
			btnClear.removeClass('active');
		} else {
			btnClear.addClass('active');
		};
	});

	//input clear button control for deleting input field
	$('.search-bar-wrap .btn-clear').on('click', function() {
		var self = $(this),
			input = self.siblings('input');

		input.val('');
		self.removeClass('active');
	});

	//sortable tables / table sorting
	$('.table-sortable.table-activity-log').tablesorter({
		//setting initial table order to 3rd column (date)
		sortList: [[2,1]],
		//disabling sorting for 2nd and 4th column
		headers: {
			1: {
				sorter: false,
			},
			3: {
				sorter: false,
			},
		},
	});

	$('.table-sortable.table-admin-nsm-activity-log').tablesorter({
		//setting initial table order to 4th column (date)
		sortList: [[2,1]],
		//disabling sorting for 2nd column
		headers: {
			1: {
				sorter: false,
			},
		},
	});

	$('.table-sortable.table-admin-nsm-adjustments').tablesorter({
		//setting initial table order to 5th column (date), 4th column is empty but present
		sortList: [[4,0]],
	});

	$('.table-sortable.table-approver-activity-log').tablesorter({
		//setting initial table order to 3rd column (date)
		sortList: [[3,1]],
		//disabling sorting for 2nd and 4th column
		headers: {
			1: {
				sorter: false,
			},
			2: {
				sorter: false,
			},
			4: {
				sorter: false,
			},
		},
	});

	$('.table-sortable.table-approver-edit').tablesorter({
		//setting initial table order to 1st column (customer name)
		sortList: [[0,0]],
		//disabling sorting for all remaining columns but the first
		headers: {
			1: {
				sorter: false,
			},
			2: {
				sorter: false,
			},
			3: {
				sorter: false,
			},
			4: {
				sorter: false,
			},
			5: {
				sorter: false,
			},
		}
	});

	$('.table-sortable.table-admin-finalize').tablesorter({
		//setting initial table order to 1st column (nsm name)
		sortList: [[0,0]],
	});

	//fetch data button control
	$('.fetch-data-status-wrap a').on('click', function(e) {
		var self = $(this),
			status = self.siblings('.status'),
			statusProgress = self.siblings('.status.in-progress'),
			statusSuccess = self.siblings('.status.success'),
			statusFailed = self.siblings('.status.failed');

		status.removeClass('active');

		e.preventDefault();
		statusProgress.addClass('active');

		setTimeout(function() {
			statusProgress.removeClass('active')
			statusSuccess.addClass('active');
		}, 1500);

		setTimeout(function() {
			statusSuccess.removeClass('active')
			statusFailed.addClass('active');
		}, 3000);
	});

	//admin btn finalize data control
	$('.js-toggle-fdata').on('change', function() {
		var btn = $('.js-toggle-fdata-btn'),
			btnSwitchMonth = $('.js-switch-month'),
			inputs = $('.js-toggle-fdata').length,
			toggledInputs = $('.js-toggle-fdata:checked').length;

		if (inputs == toggledInputs) {
			btn.removeClass('inactive').attr('onclick', 'return true;');
			btnSwitchMonth.removeClass('inactive').attr('onclick', 'return true;');
		} else {
			btn.addClass('inactive').attr('onclick', 'return false;');
			btnSwitchMonth.addClass('inactive').attr('onclick', 'return false;');
		}
	});

	//datepicker initializing from different element
	$('.js-datepicker-init').on('click', function() {
		var self = $(this);

		self.prev().datepicker('show');
	});

	// jQuery UI: datepicker
    $(function () {
    	$('.js-datepicker').datepicker();

        // $('.js-datepicker-btn').datepicker({
        //     showOn: 'both',
        //     buttonText: ''
        // });
    });

    $('.dashboard .summary-tiles-wrap .month-handle').on('click', function() {
    	var self = $(this),
    		parent = self.closest('.tile'),
    		monthWrap = parent.find('.month-wrap');

    	if (!self.hasClass('active')) {

    		parent.find('.ttm-wrap.active').fadeOut(300, function() {
    			$(this).removeClass('active');
    			parent.find('.ttm-handle.active').removeClass('active');
	    		self.addClass('active');
	    		monthWrap.fadeIn(300).addClass('active');
    		})
    	};
    });

    $('.dashboard .summary-tiles-wrap .ttm-handle').on('click', function() {
    	var self = $(this),
    		parent = self.closest('.tile'),
    		ttmWrap = parent.find('.ttm-wrap');

    	if (!self.hasClass('active')) {

    		parent.find('.month-wrap.active').fadeOut(300, function() {
    			$(this).removeClass('active');
    			parent.find('.month-handle.active').removeClass('active');
	    		self.addClass('active');
	    		ttmWrap.fadeIn(300, function() {
	    			ttmWrap.addClass('active');

	    			// ttmChartInit();
	    		});
    		})
    	};
    });

	// setTimeout(function() {
	// 	ttmChartInit();
	// }, 500);

	if ($('.ttm-chart').length) {

		$('.ttm-chart').each(function() {
			var self = $(this),
				chartCanvas = self.find('.ttm-chart-inner'),
				chartCanvasId = chartCanvas.attr('id');

			console.log(chartCanvasId);

			//timeout neccessary for the tooltip
			setTimeout(function() {
				ttmChartInit(chartCanvas);
			}, 500);
		});
	};

})(jQuery);

var customTooltips = function (tooltip) {

	var tooltipEl = $('.bar-chart-tooltip')

	tooltipEl.css({
		opacity: 0,
	});

	var position = $(this._chart.canvas).offset(),
		canvasId = $(this._chart.canvas).attr('id'),
		unit;

	switch (canvasId) {
		case 'ttm-sales-inner':
			unit = '$';
			break;
		case 'ttm-gm-dollars-inner':
			unit = '$';
			break;
		case 'ttm-my-commission-inner':
			unit = '$';
			break;
		case 'ttm-gm-percent-inner':
			unit = '%';
			break;
		case 'ttm-per-customer-inner':
			unit = '$';
			break;
		case 'ttm-branches-inner':
			unit = '#';
			break;
		default:
			unit = '$';
			break;
	};

	if (!tooltip || !tooltip.opacity) {
		return;
	};

	if (tooltip.dataPoints.length > 0) {
		tooltip.dataPoints.forEach(function (dataPoint) {
			var content = '<span class="label">' + unit + '</span>' + dataPoint.yLabel;
			var $tooltip = tooltipEl;

			$tooltip.html(content);
			$tooltip.css({
				opacity: 1,
				top: position.top + dataPoint.y + "px",
				left: position.left + dataPoint.x +"px",
			});
		});
	}
};


function ttmChartInit(selector) {
	var ctx = $(selector),
		pWidth = ctx.closest('.ttm-chart').width(),
		pHeight = ctx.closest('.ttm-chart').height();

	ctx.width(pWidth);
	ctx.height(pHeight);

	Chart.defaults.global.defaultFontFamily = 'Lato';
	Chart.defaults.global.maintainAspectRatio = false;

	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: ["Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan"],
	        datasets: [{
	        	label: '$',
	            data: [12300, 26800, 19000, 44320, 18200, 11300, 32893, 15987, 26783, 18903, 35018, 19200],
	            backgroundColor: [
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)',
	                'rgb(170, 246, 255)'
	            ],
	            hoverBackgroundColor: [
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)',
	            	'rgb(0, 217, 242)'
	            ],
	            borderWidth: 0
	        }]
	    },
	    options: {
	    	responsive: true,
	    	title: {
	    		display: false
	    	},
	    	legend: {
	    		display: false
	    	},
	    	hover: {
	    		mode: 'index',
	    		intersect: false
	    	},
	    	tooltips: {
	    		enabled: false,
	    		mode: 'index',
	    		intersect: false,
	    		custom: customTooltips
	    	},
	        scales: {
	        	xAxes: [{
	        		gridLines: {
	            		display: false,
	            		drawBorder: false,
	            	},
	            	ticks: {
	                	fontSize: 12,
	                	fontColor: '#545968',
	                	maxRotation: 90,
	                	minRotation: 90,
	                },
	                barThickness: 12
	        	}],
	            yAxes: [{
	            	gridLines: {
	            		display: false,
	            		drawBorder: false
	            	},
	                ticks: {
	                	display: false,
	                	min: 0,
	                    beginAtZero:true
	                }
	            }]
	        }
	    }
	});


	setTimeout(function() {
		myChart.resize();
	}, 1200);
};