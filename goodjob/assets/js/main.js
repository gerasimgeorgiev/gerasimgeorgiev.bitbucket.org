function fadeSection(section) {
    section.siblings('section').velocity('fadeOut', { duration: 300 });
    section.siblings('section').promise().done(function () {
        section.velocity('fadeIn', { duration: 300 });
    });
    section.removeData();
};

function swipeSectionLeft(section) {
    section.velocity(
        { translateX: '-30%', opacity: 0 },
        { display: 'none', duration: 300 }
    );
    section.promise().done(function () {
        section.css('transform', 'translateX(0)');
        section.next('section').velocity(
            { translateX: [0, 30], opacity: 1 },
            { display: 'block', duration: 300 }
        );
    });
    section.next('section').removeData();
};

function swipeSectionRight(section) {
    section.velocity(
        { translateX: '30%', opacity: 0 },
        { display: 'none', duration: 300 }
    );
    section.promise().done(function () {
        section.css('transform', 'translateX(0)');
        section.prev('section').velocity(
            { translateX: [0, -30], opacity: 1 },
            { display: 'block', duration: 300 }
        );
    });
    section.prev('section').removeData();
};

function jobTitleMultiline() {
    // test if title goes in a new line
    $('.js-jobTitle').each(function () {
        var lineHeight = $(this).css('line-height').replace('px', '');
        var titleHeight = $(this).height();
        if (titleHeight > lineHeight) {
            $(this).parent().addClass('multiline');
        } else {
            $(this).parent().removeClass('multiline');
        };
    });  
};

function findWorkersDeadline() {
    var dateTime = new Date();
    var currentHour = dateTime.getHours();
    var currentDay = dateTime.getDate();
    var currentMonth = dateTime.getMonth();
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var inputValue = $('.js-deadlineRange-input').val();
    var expireHour = parseInt(inputValue) + 2;
    var deadlineHour = currentHour + 2 + parseInt(inputValue);
    var ampm = deadlineHour >= 12 ? 'pm' : 'am';
    var hourFormated = deadlineHour % 12;
    hourFormated = hourFormated ? hourFormated : 12;

    if (deadlineHour > 23) {
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        currentDay = tomorrow.getDate();
        currentMonth = tomorrow.getMonth();
        ampm = 'am';
    }

    $('.js-deadlineRange-output').text(months[currentMonth] + ' ' + currentDay + ', ' + hourFormated + ampm);
    $('.js-deadlineRange-hours').text(expireHour);
};

function _arrayBufferToBase64( buffer ) {
    var binary = '';
    var bytes = new Uint8Array( buffer );
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
    }
    return window.btoa( binary );
}

function getOrientation(file, callback) {
  var reader = new FileReader();
  reader.onload = function(e) {

    var view = new DataView(e.target.result);
    if (view.getUint16(0, false) != 0xFFD8) return callback(-2);
    var length = view.byteLength, offset = 2;
    while (offset < length) {
      var marker = view.getUint16(offset, false);
      offset += 2;
      if (marker == 0xFFE1) {
        if (view.getUint32(offset += 2, false) != 0x45786966) return callback(-1);
        var little = view.getUint16(offset += 6, false) == 0x4949;
        offset += view.getUint32(offset + 4, little);
        var tags = view.getUint16(offset, little);
        offset += 2;
        for (var i = 0; i < tags; i++)
          if (view.getUint16(offset + (i * 12), little) == 0x0112)
            // view.setUint16(offset + (i * 12) +8,0x1, little);
            return callback(view.getUint16(offset + (i * 12) + 8, little));
      }
      else if ((marker & 0xFF00) != 0xFF00) break;
      else offset += view.getUint16(offset, false);
    }
    return callback(-1);
  };
  reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
}

$(document).ready(function () {

    // POC for number swipe - to delete after testing
    if ($('.poc-main-wrapper').length) {
        $('.poc-main-wrapper').on('swipeleft', function () {
            $(this).children('.poc-btn-wrapper').animate({ 'left': '-=9rem' }, 400);
        });
        $('.poc-main-wrapper').on('swiperight', function () {
            $(this).children('.poc-btn-wrapper').animate({ 'left': '+=9rem' }, 400);
        });

        $('.js-pocSwipe-btn').click(function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
        });
    };

    //fixing footer with fixed position covering the input elements on typing
    $('input:not([type=checkbox]), input:not([type=radio])').on('focus', function() {
        $('.btn-full').css('position', 'absolute');
        $('.btn-half').css('position', 'absolute');
    });

    $('input:not([type=checkbox]), input:not([type=radio])').on('blur', function() {
        $('.btn-full').css('position', 'fixed');
        $('.btn-half').css('position', 'fixed');
    });

    var _originalSize = $(window).width() + $(window).height()
    $(window).resize(function(){
        if($(window).width() + $(window).height() != _originalSize){
            $('.btn-full').css('position', 'absolute');
            $('.btn-half').css('position', 'absolute');
        } else {
            $('.btn-full').css('position', 'fixed');
            $('.btn-half').css('position', 'fixed');
        }
    });

    // GLOBAL
    // show clear button on search input
    $('.js-searchInput').on('input', function () {
        if ($(this).val().length !== 0) {
            $('.js-searchInput-clear').css('display', 'block');
        } else {
            $('.js-searchInput-clear').css('display', 'none');
        };
    });

    // clear search input
    $('.js-searchInput-clear').click(function () {
        $('.js-searchInput').val('');
        $(this).css('display', 'none');
    });

    // hide error icon if image not loaded
    $('.js-img-error').error(function () {
        $(this).hide();
    });

    // open modal
    $('.js-modal-open').click(function () {
        var self = $(this);
        var btnName = self.attr('name');

        $('.' + btnName).fadeIn(200);

        //fixing job examples popup from bad visualisation on mobile scroll (disappearing address bar)
        if( btnName === 'js-examples') {
            $('body').addClass('overlayed-examples');
        };

        // controlling worker popup in my-jobs-schedule-details
        if(self.hasClass('workers-wrap-img')) {
            $('.modal-worker-wrapper .pointer').removeClass('animated');

            var workerId = self.attr('data-worker-id'),
                elHeight = self.outerHeight(),
                popUpHeight = $('.modal-worker-wrapper .modal-message-wrapper').height(),
                winHeight = $(window).height(),
                scrollTop = $(window).scrollTop(),
                offset = self.offset(),
                topPosition = offset.top + elHeight + 15,
                leftPosition = offset.left;

            $('.modal-worker-wrapper').css('top', topPosition);
            $('.modal-worker-wrapper .pointer').addClass('animated').css('left', leftPosition);

            if (((winHeight + scrollTop - offset.top) < (popUpHeight + elHeight))) {

                var modalOffset = $('.modal-worker-wrapper').offset().top,
                    modalHeight = $('.modal-worker-wrapper').height(),
                    posToScroll = modalOffset - winHeight + modalHeight;

                $('html, body').animate({
                    scrollTop: posToScroll
                }, 300);
            };
        }
    });

    // close modal
    $('.js-modal-close').click(function () {
        var self = $(this);
        self.parents('.modal-bg-wrapper').fadeOut(200);
        self.parents('.modal-fullpage-wrapper').fadeOut(200);
        self.closest('.modal-worker-wrapper').fadeOut({duration: 200, queue: false}).slideUp(200);

        //making body scrollable again - see js-examples & disappearing address bar bug
        if (self.closest('.modal-bg-wrapper').hasClass('js-examples')) {
            $('body').removeClass('overlayed-examples');
        };
    });

    // rsvp hide buttons and show text
    $('.js-rsvp-accept').on('click', function() {
        var self = $(this),
            parent = self.closest('.btns-wrap');

        parent.slideUp(300);
        parent.siblings('.hidden').slideDown(300);
    });

    // select day in a week
    $('.js-daySelector').click(function () {

        // canceling selection on unselected weeks in project staffing
        if($('.choose-week-section').length) {
            if(!$(this).closest('.radio-wrap').hasClass('chosen')) {
                return false;
            };
        };

        $(this).toggleClass('marked');

        // showing appropriate workers in my hires page
        if($('.my-hires').length) {
            myHiresWorkersSelect();
        };
    });

    // open-close side menu
    $('.js-sideMenu-open').click(function () {
        $('.signin-sidemenu').animate({'left':'0'}, 500);
        $('body').addClass('overlayed');
    });
    $('.js-sideMenu-close').click(function () {
        $('.signin-sidemenu').animate({'left':'-200vw'}, 500);
        $('body').removeClass('overlayed');
    });

    // show-hide modal menu
    $('.js-modalMenu-toggle').click(function () {
        $('.js-modalMenu').fadeToggle(200);
    });

    // toggle shortlisted job state
    $('.js-btn-shortlist').on('click', function (e) {
        e.preventDefault();
        var self = $(this);
        self.toggleClass('shortlisted');
    });

    // main screen page
    if($('.main-screen .owl-carousel').length) {
        $('.main-screen .owl-carousel').owlCarousel({
            items: 1,
            margin: 0,
            loop: false,
            center: true,
            dots: true
        });
    };

    if($('.find-workers-wrapper .owl-carousel').length) {
        var owl = $('.find-workers-wrapper .owl-carousel');

        owl.on('initialized.owl.carousel', function(event) {
            var firstItem = $('.active');
            firstItem = firstItem.eq(2);
            firstItem.addClass('current');
        }).owlCarousel({
            dots:false,
            loop: false,
            items: 5,
            margin: 0
        });

        owl.on('changed.owl.carousel', function(event) {
            setTimeout(function() {
                var currentItem = $('.active');
                currentItem = $(currentItem[2]);
                $('.percent-slider .current').removeClass('current');
                currentItem.addClass('current');
            }, 100);
        });

        $('.owl-item').on('click', function() {
            n = $(this).index();
            n = n - 2;
            owl.trigger('to.owl.carousel', [n, 200, true]);
        });
    };

    // How it works page
    if ($('.how-works-wrapper .owl-carousel').length) {
        $('.how-works-wrapper .owl-carousel').owlCarousel({
            items: 1,
            margin: 0,
            loop: false,
            center: true,
            dots: true
        });
    };

    //prelaunch page 
    if($('.prelaunch-screen').length) {
        $('.prelaunch-screen form').on('submit', function(e) {
            e.preventDefault();
            $('.before-submit').fadeOut(300, function() {
                $('.after-submit').fadeIn(300);
            });
        });
    };

    // unlock job position slider
    if($('.unlock-slider .owl-carousel').length) {
        $('.unlock-slider .owl-carousel').owlCarousel({
            items: 1,
            margin: 0,
            loop: false,
            center: true,
            dots: true
        });
    };

    // choosing week and preventing user to select day if inactive
    if($('.project-staffing-wrapper .choose-week-section').length) {
        var radioInput = $('.radio-week');

        radioInput.on('change', function() {
            if($(this).closest('.radio-wrap').hasClass('chosen')) {
                return false;
            } else {
                $('.chosen').find('.marked').removeClass('marked');
                $('.chosen').removeClass('chosen');
                $(this).closest('.radio-wrap').addClass('chosen');
            };
        });

        $('.radio-wrap:not(.chosen) .js-daySelector').on('click', function() {
            return false;
        });
    };

    // Project Stuffing pages
    if ($('.project-staffing-wrapper').length) {
        
        // New Quote: needed certifications
        $('.js-needed-certification').click(function () {
            if ($(this).hasClass('line')) {
                $(this).removeClass('line').addClass('green');
            } else if ($(this).hasClass('green')) {
                $(this).removeClass('green').addClass('blue');
            } else if ($(this).hasClass('blue')) {
                $(this).removeClass('blue').addClass('line');
            };
        });
    };

    if ($('.find-jobs-wrapper').length) {
        // List of Jobs: show-hide Sort popup
        $('.js-sortJobs-toggle').click(function () {
            $('.js-sortJobs').fadeToggle(200);
        });

        // 
        $('.js-saveWorker-matchNum').each(function () {
            var matchValue = $(this).text();
            var matchValueDeg = matchValue * 3.6;
            var parentWrapper = $(this).parents('.match-cont');

            // console.log(matchValueDeg);

            if (matchValueDeg > 180) {
                parentWrapper.find('.js-saveWorker-matchChart').addClass('above-half');
            }

            parentWrapper.find('.js-saveWorker-matchPie').css({
                '-moz-transform': 'rotate(' + matchValueDeg + 'deg)',
                '-webkit-transform': 'rotate(' + matchValueDeg + 'deg)',
                'transform': 'rotate(' + matchValueDeg + 'deg)'
            });
        });
    };

    // MY PROFILE pages
    if ($('.workers-profile-subpages-wrapper .introduce-yourself').length) {

        $('.img-upload-label').on('click', function() {
            if($(this).hasClass('imgPreviewed')) {
                $('#img-upload-input').val('');
                $(this).find('#img-preview').addClass('deleted').attr('src', '');
                $(this).removeClass('imgPreviewed').find('.txt-small').text('Upload photo');
                return false;
            };
        });

        $('#img-upload-input').on('change', function() {
            readURL(this);
            var self = $(this);
        });
    };

    if ($('.js-fitnessSurvey-slideInput').length) {
        $('.js-fitnessSurvey-slideInput').slider();
    };

    if ($('.workers-profile-subpages-wrapper').length) {
        // Personal Transportation: choose tile
        $('.js-myTransport').on('click', function () {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                $(this).siblings('.js-myTransport').removeClass('active');
            };
        });
    };

    if ($('.workers-side-wrapper .profile').length) {
        //controlling expandable element in workers profile
        $('.js-expandable-control').on('click', function() {
            var expandable = $('.js-expandable'),
                expandPart = $('.js-expand-part');

            if (expandable.hasClass('closed')) {
                expandPart.slideDown(300);
                expandable.removeClass('closed').addClass('opened');
            } else {
                expandPart.slideUp(300);
                expandable.removeClass('opened').addClass('closed');
            };
        });
    };

    // FIND WORKERS pages
    if ($('.find-workers-wrapper').length) {
        jobTitleMultiline();

        // List of Jobs: change job tile view
        $('.js-change-tiles').click(function () {
            $(this).toggleClass('btn-view-rows btn-view-tiles');
            $('.tiles-wrapper').toggleClass('view-tiles view-rows');
        });

        // List of Workers: select workers
        $('.js-selectAll-workers').click(function () {
            if ($(this).is(":checked")) {
                $('.js-selectWorker').each(function () {
                    $(this).addClass('selected');
                });
            } else {
                $('.js-selectWorker').each(function () {
                    $(this).removeClass('selected');
                });
            };
        });
        $('.js-selectWorker').click(function () {
            $(this).toggleClass('selected');
            if (!$(this).hasClass('selected')) {
                $('.js-selectAll-workers').prop('checked', false);
            };
        });

        // List of Workers: save worker
        $('.js-saveWorker').click(function () {
            $(this).addClass('saved');
            $(this).html('Saved');
        });

        // List of Workers: show-hide Sort popup
        $('.js-sortWorkers-toggle').click(function () {
            $('.js-sortWorkers').fadeToggle(200);
        });

        // List of Workers: draw Match value pie chart
        $('.js-saveWorker-matchNum').each(function () {
            var matchValue = $(this).text();
            var matchValueDeg = matchValue * 3.6;
            var parentWrapper = $(this).parents('.match-cont');

            if (matchValueDeg > 180) {
                parentWrapper.find('.js-saveWorker-matchChart').addClass('above-half');
            }

            parentWrapper.find('.js-saveWorker-matchPie').css({
                '-moz-transform': 'rotate(' + matchValueDeg + 'deg)',
                '-webkit-transform': 'rotate(' + matchValueDeg + 'deg)',
                'transform': 'rotate(' + matchValueDeg + 'deg)'
            });
        });

        // Filters Schedule: activate range slider
        if($('.js-filter-matchScore-range').length) {
            $('.js-filter-matchScore-range').rangeslider();            
        };

        // Workers side Filters: activate sliders
        if ($('.js-filter-workersSide-range').length) {
            $('.js-filter-workersSide-range').rangeslider();
        };
        if ($('.js-filter-workersSide-slideInput').length) {
            $('.js-filter-workersSide-slideInput').slider();
        };

        // Workers side Filters: change button color
        $('.js-filter-switchButtons').click(function () {
            $(this).addClass('link-blue');
            $(this).siblings().removeClass('link-blue');
        });

        // Workers side Filters: show-hide day selector
        $('.js-filter-toggleDaySelector').click(function () {
            $(this).siblings('.day-selector-wrapper').slideToggle(200);
        });

        // Workers side Notifications: show-hide email notifications
        $('.js-myProfile-mailNotifications').click(function () {
            $(this).parent('li').siblings('.email-notifications-note').slideToggle(200);
        });

        // Workers side Notifications: sms notifications modal
        $('.js-myProfile-smsNotifications').click(function (e) {
            if ($(this).is(':checked')) {
                e.preventDefault();
                $('.js-myProfile-smsNotifications-modal').fadeIn(200);
            };
        });
        $('.js-myProfile-smsNotifications-turnOn').click(function () {
            $('.js-myProfile-smsNotifications').prop('checked', true);
            $('.js-myProfile-smsNotifications-modal').fadeOut(200);
        });

        // Workers side Profile: change period on Work Preferences
        $('.js-profile-timePeriod').click(function () {
            $(this).siblings('.js-profile-timePeriod').removeClass('active');
            $(this).addClass('active');
        });

        // Workers side Profile: change distance on Work Preferences
        $('.js-homeDistance').click(function () {
            $(this).siblings('.js-homeDistance').removeClass('active');
            $(this).addClass('active');
        });

        // Filters Schedule: show-hide Match Score range slider
        $('.js-filter-matchScore-btn').click(function () {
            $('.js-filter-matchScore').slideToggle();
        });

        // Make it Urgent: set deadline value
        if ($('.js-deadlineRange-input').length) {
            $('.js-deadlineRange-input').slider();
        };
        findWorkersDeadline();
        $('.js-deadlineRange-input').on('change mousemove', function () {
            findWorkersDeadline();
        });

        // Skills: change button color
        $('.js-filterSkills-select').click(function () {
            $(this).toggleClass('must des');
        });

        // Fill my schedule: switch schedule lists
        $('.js-select-week').on('click', function (e) {
            var btn = $(this);
            e.preventDefault();
            if (!btn.hasClass('active')) {
                if (btn.hasClass('js-current-week-btn')) {
                    $('.js-next-week').stop().fadeOut(300, function () {
                        $('.js-current-week').stop().fadeIn(300);
                    });
                    $('.js-select-week.active').removeClass('active');
                    btn.addClass('active');
                };
                if (btn.hasClass('js-next-week-btn')) {
                    $('.js-current-week').stop().fadeOut(300, function () {
                        $('.js-next-week').stop().fadeIn(300);
                    });
                    $('.js-select-week.active').removeClass('active');
                    btn.addClass('active');
                };
            };
        });
    };

    //preventing worker from entering job details when not signed in
    if ($('.find-jobs-wrapper.not-signed-wrap').length) {
        
        $('.not-signed-wrap .btn-details').on('click', function(e) {
            e.preventDefault();
        })
    }

    // SELECT JOB SCHEDULE pages
    if ($('.select-jobSchedule-wrapper').length) {

        // Make it Urgent: show deadline hours dropdown
        $('.urgent-form input:radio').click(function () {
            if ($('.js-setDeadline').is(":checked")) {
                $('.js-setDeadline-hrs').css('display', 'block');
            } else {
                $('.js-setDeadline-hrs').css('display', 'none');
            };
        });
    };

    // SEARCH AREA page
    if ($('.search-area-wrapper').length) {

        // change radius
        $('.js-searchArea-radius').click(function () {
            $(this).addClass('active');
            $(this).siblings('.js-searchArea-radius').removeClass('active');
        });
    };

    // SIGN IN pages
    if ($('.signin-pages').length) {

        // Pick Account: change selected account
        $('.js-account-type').click(function () {
            $(this).parent('.acc-btn-wrapper').siblings().removeClass('active');
            $(this).parent('.acc-btn-wrapper').toggleClass('active');
            $(this).parents('.acc-choice-wrapper').siblings('.js-account-txt').toggleClass('active');

            // change Next link url
            if ($(this).parent().hasClass('btn-personal')) {
                $(this).parents('.acc-choice-wrapper').siblings('.js-account-link').attr('href', 'signup-individual.html');
            };
            if ($(this).parent().hasClass('btn-business')) {
                $(this).parents('.acc-choice-wrapper').siblings('.js-account-link').attr('href', 'signup-business.html');
            }
        });

        // Sign In: show password text
        $('.js-showPass').click(function () {
            $(this).toggleClass('shown');
            if ($(this).hasClass('shown')) {
                $(this).siblings('.js-signinPass').prop('type', 'text');
            } else {
                $(this).siblings('.js-signinPass').prop('type', 'password');
            };
        });

        // Add phone number: change flag
        $('.js-flag-show').click(function () {
            $(this).find('.js-flag-select').fadeToggle(200);
        });
        $('.js-flag-select button').click(function () {
            var btnName = $(this).attr('name');
            var flagContainer = $(this).parents('.js-flag-show');
            var flagClass = flagContainer.attr('class').match(/flagName[\w-]*\b/);
            flagContainer.removeClass(flagClass[0]).addClass(btnName);
        });
    };

    // TIME & PAY pages
    if ($('.workers-timePay-wrapper').length) {
        
        // My Work History page change view
        $('.js-toggle-WorkHistory').click(function () {
            $('section').each(function () {
                if ($(this).hasClass('active')) {
                    $(this).fadeOut(200).removeClass('active');
                    $(this).promise().done(function () {
                        $(this).siblings('section').fadeIn(200).addClass('active');
                    });
                };
            });
            $(this).text(function (i, text) {
                return text === 'Monthly' ? "Weekly" : "Monthly";
            });
        });

        $('.js-open-details').on('click', function() {
            $(this).toggleClass('opened').find('.details-part').slideToggle(300);
        });
    };

    if ($('.time-pay-wrapper').length) {

        //selecting all workers in timesheet
        $('.js-select-all-workers').on('change', function() {
            var self = $(this);

            if (self.is(':checked')) {
                $('.worker-individual').each(function() {
                    $(this).prop('checked', true);
                })
            } else {
                $('.worker-individual').each(function() {
                    $(this).prop('checked', false);
                })
            }
        })

        //select date in history week
        $('.js-select-history-date').on('click', function() {
            var self = $(this),
                attrName = self.attr('data-name');

            $('.initial').fadeOut(300);
            $('.info-wrap').fadeOut(300);
            $('.marked').removeClass('marked');
            setTimeout(function() {
                self.addClass('marked');
                $('[data-day="' + attrName + '"]').fadeIn(200);
            }, 400);
        })
    }

    // MY JOBS page
    if ($('.my-jobs-wrapper').length) {

        // switch week lists
        $('.js-select-week').on('click', function (e) {
            var btn = $(this);
            e.preventDefault();
            if (!btn.hasClass('active')) {
                if (btn.hasClass('js-current-week-btn')) {
                    $('.js-next-week').stop().fadeOut(300, function () {
                        $('.js-current-week').stop().fadeIn(300);
                    });
                    $('.js-select-week.active').removeClass('active');
                    btn.addClass('active');
                };
                if (btn.hasClass('js-next-week-btn')) {
                    $('.js-current-week').stop().fadeOut(300, function () {
                        $('.js-next-week').stop().fadeIn(300);
                    });
                    $('.js-select-week.active').removeClass('active');
                    btn.addClass('active');
                };
            };
        });
        
        // change selected top tab
        $('.js-myJobs-tabs button').click(function () {
            if (!$(this).hasClass('active')) {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
                var buttonName = $(this).attr('name');

                // change active section on click
                $('section').each(function () {                
                    if ($(this).hasClass(buttonName)) {
                        var thisSection = $(this);
                        fadeSection(thisSection);
                    };
                });
            };
        });

        // change active section on left swipe
        $('section').on('swipeleft', function () {
            if ($(this).next('section').length) {
                var thisSection = $(this);
                swipeSectionLeft(thisSection);

                // change active tab button
                setTimeout(function () {
                    $('.js-myJobs-tabs').children('button.active').removeClass('active').next('button').addClass('active');
                }, 300);                
            };
        });

        // change active section on right swipe
        $('section').on('swiperight', function () {
            if ($(this).prev('section').length) {
                var thisSection = $(this);
                swipeSectionRight(thisSection)

                // change active tab button
                setTimeout(function () {
                    $('.js-myJobs-tabs').children('button.active').removeClass('active').prev('button').addClass('active');
                }, 300);                
            };
        });

        // show-hide inactive jobs
        $('.js-inactive-jobs-btn').on('click', function () {
            if ($(this).is(":checked")) {
                $('.js-inactive-job').each(function () {
                    $(this).fadeOut(200);
                });
            } else {
                $('.js-inactive-job').each(function () {
                    $(this).fadeIn(200);
                });
            };
        });

        // approve and reject hover activation
        $('.js-billrate-approved-btn').click(function () {
            var parent = $(this).parents('.js-blur-wrapper');
            parent.addClass('blured-elements');
            parent.find('.js-billrate-approved').css('display', 'block');
        });
        $('.js-billrate-rejected-btn').click(function () {
            var parent = $(this).parents('.js-blur-wrapper');
            parent.addClass('blured-elements');
            parent.find('.js-billrate-rejected').css('display', 'block');
        });

        // open-close Negotiation form
        $('.js-negotiateMsg-open').click(function () {
            if (!$(this).hasClass('active') && !$(this).hasClass('disabled')) {
                $(this).addClass('active');
                $('.js-negotiateMsg-form').css('display', 'block');                
            };
        });
        $('.js-negotiateMsg-close').click(function () {
            $('.js-negotiateMsg-open').removeClass('active');
            $('.js-negotiateMsg-form').css('display', 'none');
        });

        // open-close Negotiation modal note
        $('.js-negotiateMsg-send').click(function () {
            $('.js-negotiateMsg-form').css('display', 'none');
            $('.js-negotiateMsg-open').toggleClass('active disabled');
            $('.js-negotiateMsg-note').css('display', 'block');
        });
        $('.js-negotiateMsg-noteClose').click(function () {
            $('.js-negotiateMsg-note').css('display', 'none');
        });
    };

    // CREATE NEW JOB pages
    if ($('.create-job-wrapper').length) {
        jobTitleMultiline();

        // Step 1: select job category
        $('.js-select-newJob').click(function () {
            $(this).toggleClass('selected');
            $(this).siblings('.js-select-newJob').removeClass('selected');
        });

        // Step 3: change question number
        $('.js-questionNum-up').click(function () {
            var value = parseInt($(this).siblings('.js-questionNum').text(), 10) + 1;
            $(this).siblings('.js-questionNum').text(value);
        });
        $('.js-questionNum-down').click(function () {
            var value = parseInt($(this).siblings('.js-questionNum').text(), 10);
            if (value > 0) {
                value--;
            };
            $(this).siblings('.js-questionNum').text(value);
        });

        // upload file button
        $('button.js-attachImage').click(function () {
            $(this).siblings('input.js-attachImage').click();
        });

        // next button
        $('.js-newJob-nextStep').click(function () {
            $(this).parent().siblings('section').removeClass('active');
            $(this).parent().next('section').addClass('active');
        });

        // back button
        $('.js-newJob-prevStep').click(function () {
            $(this).parent().siblings('section').removeClass('active');
            $(this).parent().prevAll('section').eq(1).addClass('active');
        });
    };

    // disabling select on past days

    $('.create-schedule-wrapper').find('.day-past').removeClass('js-wrap js-not-selected js-selected');

    // selecting and deselecting days in create schedule

    $('.create-schedule-wrapper .js-wrap').on('click', function() {
        $(this).toggleClass('js-not-selected js-selected');
    });

    // controling toggle buttons

    $('.js-control-toggle-btn').on('click', function() {
        var btn = $(this).find('.toggle-btn');
        if ($('input:checked').length) {
            btn.prop('checked', false);
        } else {
            btn.prop('checked', true);
        };
    });

    $('.js-control-toggle-btn').on('click', function() {
        if($('.toggle-btn:checked').length) {
            $(this).siblings('.js-overlay-parent').removeClass('overlay-active');
            $('.js-auto-hire-value-here').fadeIn(200);
        } else {
            $(this).siblings('.js-overlay-parent').addClass('overlay-active');
            $('.js-auto-hire-value-here').fadeOut(200);
        };
    });

    if($('.js-group-days').length) {
        $('.js-control-toggle-btn').on('click', function(e) {
            e.preventDefault();
            var dayWrap = $(this).closest('.week-wrapper').siblings('.day-wrapper');
            var day = dayWrap.find('.single-day');
            var daysNumber = day.length;
            var twoPartStandart = dayWrap.find('.two-part-standart:first-of-type');
            var selectWorkers = twoPartStandart.find('.js-select-workers').val();
            var selectStartTime = twoPartStandart.find('.js-select-start-time').val();
            var selectDuration = twoPartStandart.find('.js-select-duration').val();

            if($('.toggle-btn:checked').length) {
                day.each(function() {
                    var curDay = $(this);
                    twoPartStandart.fadeOut(200, function() {
                        var cl = $(twoPartStandart.clone());
                        dayWrap.append(cl.fadeIn(200));
                        cl.find('h2').html(curDay.html());

                        if(selectWorkers) {
                            $('.js-select-workers').val(selectWorkers);
                        };

                        if(selectStartTime) {
                            $('.js-select-start-time').val(selectStartTime);
                        };

                        if(selectDuration) {
                            $('.js-select-duration').val(selectDuration);
                        };

                        $('.two-part-standart:not(:first-of-type)').bind('remove-day', function(event) {
                            $(this).remove();
                        });
                    });
                });
            } else {
                dayWrap.fadeOut(200, function() {
                    $('.two-part-standart').trigger('remove-day');
                    twoPartStandart.fadeIn(200);
                }).fadeIn(0);
            };
        });
    };

    // circle/round picker (slider) on create new job schedule page 3

    if($('.js-round-slider').length) {
        $('.js-round-slider').roundSlider({
            width: 5,
            startAngle: 90,
            editableTooltip: false,
            handleSize: "50, 50",
            radius: 115,
            sliderType: "min-range",
            create: function (e) {
                $('.rs-tooltip').attr('id', 'findMe');
                document.getElementById('findMe').insertAdjacentHTML('beforeend', '%');
            },
            change: function (e) {
                document.getElementById('findMe').insertAdjacentHTML('beforeend', '%');
                getAndSetValue();
            },
            drag: function (e) {
                document.getElementById('findMe').insertAdjacentHTML('beforeend', '%');
                getAndSetValue();
            },
            stop: function (e) {
                $('.rs-handle').blur();
            }
        });
    };

    if($('.js-select-radio').length) {
        $('.js-select-radio').on('click', function() {
            $(this).siblings('input[type=radio], input[type=checkbox]').prop('checked', true);
        });
    };

    // workers-side_my-jobs job border colors

    if($('.workers-side-wrapper.my-jobs-wrapper .calendar').length) {
        var elem = $(this);
        colorMyJobs(elem);
    };

    if($('.js-deselect').length) {
        $('.js-deselect').on('click', function() {
            $(this).parent().siblings('.check-wrap').children('input[type=checkbox]').prop('checked', false);
        });
    };

    // if($('.js-deselect-specific').length) {
    //     $('.js-deselect-specific').on('click', function() {
    //         $('.deselect-specific-target').prop('checked', false);
    //     });
    // };
});

function getAndSetValue() {
    var obj = $('.js-round-slider').data('roundSlider');
    var val = obj.getValue();

    $('.js-auto-hire-value-here').text('@ ' + val + '%');
};

$(window).resize(function () {
    // FIND WORKERS pages
    if ($('.find-workers-wrapper').length || $('.create-job-wrapper').length) {
        jobTitleMultiline();
    };
});

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};

function colorMyJobs(elem) {
    var jobWrapper = elem.find('.job-wrap');

    // classes for border colors
    var colorClasses = ['purple', 'dark-purple', 'light-blue', 'light-green', 'yellow', 'red', 'orange'];
    
    // shuffling color classes in the array
    shuffle(colorClasses);

    // set colors based on exact same job name (case sensitive)
    jobWrapper.each(function(i, val) {

        var jobId = $(this).attr('data-id'),
            color = colorClasses[i];

        var localJobColor = localStorage.getItem('jobN' + jobId);


        if(localJobColor === null) {

            elem.find('[data-id=' + jobId + ']').addClass('colored').addClass(colorClasses[i]);

            localJobColor = localStorage.setItem('jobN' + jobId, color);

        } else {

            elem.find('[data-id=' + jobId + ']').addClass('colored').addClass(localJobColor);

        };
    });

    for(var i=0, len=localStorage.length; i<len; i++) {
        var arr = [],
            key = localStorage.key(i);
            key = key.split('N');

        $('.job-wrap').each(function() {
            var attr = $(this).attr('data-id');

            if(key[1] === attr) {
                arr.push(1);

                return arr;
            };
                
            return arr;
        });

        if(arr.length === 0) {
            key = key.join('N');
            localStorage.removeItem(key);
        };
    };
};

function myHiresWorkersSelect() {
    var daySelected = $('.day-selector-wrapper .marked');
    var workerContainer = $('.workers-wrap-img');
    workerContainer.removeClass('seeMe').fadeOut(200);;

    daySelected.each(function() {
        var dataAttr = $(this).attr('data-day');
        var dates = [];
        dates.push(dataAttr);


        workerContainer.each(function() {
            var curCont = $(this);

            $.each($(this).data('day'), function(index, value) {

                if(dates[0] === value) {
                    curCont.addClass('seeMe');
                };
            });
        });
    });

    $('.seeMe').stop().fadeIn(200);
};

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            getOrientation(input.files[0], function(orientation) {
                // alert('orientation: ' + orientation);
                switch (orientation) {
                    case 1:
                        // $('#img-preview').css('transform', 'rotate(90deg)');
                        break;
                    case 2:
                        $('#img-preview').css('transform', 'rotateY(180deg)');
                        break;
                    case 3:
                        $('#img-preview').css('transform', 'rotate(180deg)');
                        break;
                    case 4:
                        $('#img-preview').css('transform', 'rotateY(180deg) rotate(180deg)');
                        break;
                    case 5:
                        $('#img-preview').css('transform', 'rotateY(180deg) rotate(90deg)');
                        break;
                    case 6:
                        $('#img-preview').css('transform', 'rotate(90deg)');
                        break;
                    case 7:
                        $('#img-preview').css('transform', 'rotateY(180deg) rotate(-90deg)');
                        break;
                    case 8:
                        $('#img-preview').css('transform', 'rotate(-90deg)');
                        break;
                };
            })
            $('#img-preview').removeClass('deleted').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    };

    $('.img-upload-label').addClass('imgPreviewed').find('.txt-small').text('Delete');
};