﻿using PoC.BLL;
using PoC.Core;
using System.Web.Http;

namespace PoC.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/Movie")]
    public class MovieController : ApiController
    {
        [HttpGet]
        [Route("GetMovies")]
        public IHttpActionResult GetMovies()
        {
            var movies = MovieActions.GetMovies();

            return Ok(new { Success = true, Data = movies });
        }

        [HttpGet]
        [Route("GetGenres")]
        public IHttpActionResult GetGenres(string filter)
        {
            var genres = MovieActions.GetGenres(filter);

            return Ok(new { Success = true, Data = genres });
        }

        [HttpPost]
        [Route("SaveMoviesChanges")]
        public IHttpActionResult SaveMoviesChanges(MovieVM movie)
        {
            MovieActions.SaveMoviesChanges(movie);

            return Ok(new { Success = true });
        }

        [HttpPost]
        [Route("DeleteMovie")]
        public IHttpActionResult DeleteMovie([FromBody]int movieId)
        {
            MovieActions.DeleteMovie(movieId);

            return Ok(new { Success = true });
        }

        [HttpPost]
        [Route("AddNewMovie")]
        public IHttpActionResult AddNewMovie(MovieVM movie)
        {
            MovieActions.AddNewMovie(movie);

            return Ok(new { Success = true });
        }
    }
}