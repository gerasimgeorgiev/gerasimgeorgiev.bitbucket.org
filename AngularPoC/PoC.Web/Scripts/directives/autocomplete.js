﻿app.directive('genres', function () {
    return {
        restrict: 'E',
        templateUrl: '/Home/GenreAutocomplete'
    };
});