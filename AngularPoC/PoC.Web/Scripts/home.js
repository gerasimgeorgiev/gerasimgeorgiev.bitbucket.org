﻿var app = angular.module("moviesApp", [ "ngResource", "ui.bootstrap", "ngRoute", "ngGrid"]);

app.config(function ($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl: "/Home/MoviesList",
        controller: "listMovies"
    })
    .when("/Edit", {
        templateUrl: "/Home/EditMovie",
        controller: "editMovie"
    })
    .when("/Add", {
        templateUrl: "/Home/AddMovie",
        controller: "addMovie"
    })
    .when("/Login", {
        templateUrl: "/Account/Login",
        controller: "login"
    })
    .when("/Register", {
        templateUrl: "/Account/Register",
        controller: "register"
    });
});

app.run(['$rootScope', '$location', 'authentication', function ($rootScope, $location, authentication) {
    $rootScope.apiAddress = "//localhost:10466/";

    $rootScope.$on('$routeChangeStart', function (event) {

        if (!authentication.isUserLoggedIn()) {
            $rootScope.isUserLoggedIn = false;

            var locationPath = $location.path();
            if (locationPath != "/Register") {
                $location.path('/Login');
            }
            else {
                $location.path($location.path());
            }
        }
        else {
            $rootScope.userName = authentication.getUserName();
            $rootScope.isUserLoggedIn = true;

            $location.path($location.path());
        }
    });
}]);