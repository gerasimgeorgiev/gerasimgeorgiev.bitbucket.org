﻿app.service('authentication', function () {
    var user = {};

    var setAuthenticatedUserData = function (userData) {
        user.accessToken = userData.access_token;
        user.userName = userData.userName;

        localStorage.userData = JSON.stringify(user);
    };

    var getUserName = function (userData) {
        if (localStorage.userData && localStorage.userData.length > 0) {
            user = JSON.parse(localStorage.userData);
            return user.userName;
        }

        return "";
    };

    var isUserLoggedIn = function () {
        if (localStorage.userData && localStorage.userData.length > 0) {
            user = JSON.parse(localStorage.userData);
        }
        else
            return false;

        if (!user.accessToken) {
            return false;
        }
        else {
            return user.accessToken.length > 0;
        }
    };

    var getAccessToken = function () {
        if (localStorage.userData && localStorage.userData.length > 0) {
            user = JSON.parse(localStorage.userData);
        }
        else
            return "";

        if (!user.accessToken) {
            return "";
        }
        else {
            return user.accessToken;
        }
    };

    var logout = function () {
        user.accessToken = '';
        user.userName = '';
        user = {};

        localStorage.userData = "";
    };

    return {
        setAuthenticatedUserData: setAuthenticatedUserData,
        getUserName: getUserName,
        isUserLoggedIn: isUserLoggedIn,
        logout: logout,
        getAccessToken: getAccessToken
    };
});