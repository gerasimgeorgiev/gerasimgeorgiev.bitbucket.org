﻿app.service('translation', function ($resource) {

    this.getTranslation = function($scope, language) {
        var languageFilePath = '../../translations/translation_' + language + '.json';
        $resource(languageFilePath).get(function (data) {
            $scope.translation = data;
        });
    };

    this.setLanguage = function (language) {
        localStorage.language = language;
    };

    this.getLanguage = function () {
        if (!localStorage.language)
            return "en";
        else
            return localStorage.language;
    };
});