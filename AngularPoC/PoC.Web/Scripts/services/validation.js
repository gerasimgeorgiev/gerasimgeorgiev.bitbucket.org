﻿app.service('movievalidation', function () {
    var validationSummary = {};

    var validateMovie = function (movieTemp) {
        movie = movieTemp;
        var showSummary = false;

        if (!movie || !movie.Name || movie.Name.length == 0) {
            validationSummary.NameRequired = true;
            showSummary = true;
        }
        else
            validationSummary.NameRequired = false;

        if (!movie || !movie.GenreId || movie.GenreId.length == 0) {
            validationSummary.GenreRequired = true;
            showSummary = true;
        }
        else
            validationSummary.GenreRequired = false;

        validationSummary.ShowSummary = showSummary;

        return validationSummary;
    };

    return {
        validateMovie: validateMovie
    };
});