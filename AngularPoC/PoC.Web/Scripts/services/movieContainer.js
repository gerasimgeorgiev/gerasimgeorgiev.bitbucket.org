﻿app.service('movieContainer', function () {
    var movie = {};
    var genre = "";

    var setCurrentMovie = function (movieTemp) {
        movie = movieTemp;
    };

    var getCurrentMovie = function () {
        return movie;
    };

    var setGenre = function (genreTemp) {
        genre = genreTemp;
    };

    var getGenre = function () {
        return genre;
    };

    return {
        setCurrentMovie: setCurrentMovie,
        getCurrentMovie: getCurrentMovie,
        setGenre: setGenre,
        getGenre: getGenre
    };
});