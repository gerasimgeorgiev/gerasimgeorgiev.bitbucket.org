﻿app.controller('common', function ($scope, $http, $location, authentication, translation) {
    $scope.language = translation.getLanguage();

    $scope.logout = function () {
        authentication.logout();
        $location.path("/Login");
    };

    $scope.swithLanguage = function () {
        translation.setLanguage($scope.language);
        translation.getTranslation($scope, translation.getLanguage());
    };

    translation.getTranslation($scope, translation.getLanguage());
});