﻿app.controller('genreautocomplete', function ($scope, $rootScope, $http, $location, movieContainer, authentication) {
    $scope.genre = movieContainer.getGenre();

    $scope.$watch('genre', function (newVal, oldVal) {
        movieContainer.setGenre(newVal);
    }, true);

    $scope.getGenres = function (filter) {
        if (!filter)
            filter = '';

        return $http.get($rootScope.apiAddress + "api/Movie/GetGenres?filter=" + filter,
            {
                headers: { 'Authorization': 'Bearer ' + authentication.getAccessToken() }
            })
        .then(function (response) {
            if (response.data.Success == true) {
                return response.data.Data;
            }
        }, function (error) {

        });
    };
});