﻿app.controller('login', function ($scope, $rootScope, $http, $location, authentication, translation) {
    $scope.login = function () {
        var postData = "grant_type=password&username=" + $scope.user.Email + "&password=" + $scope.user.Password;

        $http.post($rootScope.apiAddress + "Token", postData)
        .then(function (response) {
            authentication.setAuthenticatedUserData(response.data);
            $location.path("/");
        }, function (error) {

        });
    };

    $scope.register = function () {
        $location.path("/Register");
    };

    $scope.$watch(function () { return localStorage.language; }, function (newVal, oldVal) {
        if (oldVal != newVal) {
            translation.getTranslation($scope, translation.getLanguage());
        }
    });

    translation.getTranslation($scope, translation.getLanguage());
});