﻿app.controller('editMovie', function ($scope, $rootScope, $http, $location, $routeParams, movieContainer, authentication, movievalidation, translation) {
    $scope.movie = movieContainer.getCurrentMovie();
    $scope.genre = movieContainer.setGenre({ Id: $scope.movie.GenreId, Name: $scope.movie.GenreName });

    $scope.cancel = function () {
        $location.path("/");
    };

    $scope.saveChanges = function () {
        $scope.genre = movieContainer.getGenre();

        if (!$scope.movie)
            $scope.movie = {};

        if ($scope.genre)
            $scope.movie.GenreId = $scope.genre.Id;
        else
            $scope.movie.GenreId = "";

        $scope.validationSummary = movievalidation.validateMovie($scope.movie);

        if ($scope.validationSummary.ShowSummary == false) {
            $http.post($rootScope.apiAddress + "api/Movie/SaveMoviesChanges", $scope.movie,
                {
                    headers: { 'Authorization': 'Bearer ' + authentication.getAccessToken() }
                })
            .then(function (response) {
                if (response.data.Success == true) {
                    movieContainer.setGenre("");
                    $location.path("/");
                }
            });
        }
    };

    $scope.$watch(function () { return localStorage.language; }, function (newVal, oldVal) {
        if (oldVal != newVal) {
            translation.getTranslation($scope, translation.getLanguage());
        }
    });

    translation.getTranslation($scope, translation.getLanguage());
});