﻿app.controller('register', function ($scope, $rootScope, $http, $location, translation) {
    $scope.createAccount = function () {
        if ($scope.user.Password != $scope.user.ConfirmPassword) {
            alert("Password does not match");
            return;
        }

        $http.post($rootScope.apiAddress + "api/Account/Register", angular.toJson($scope.user))
        .then(function (response) {
            $location.path("/Login");
        }, function (error) {

        });
    };

    $scope.cancel = function () {
        $location.path("/Login");
    };

    $scope.$watch(function () { return localStorage.language; }, function (newVal, oldVal) {
        if (oldVal != newVal) {
            translation.getTranslation($scope, translation.getLanguage());
        }
    });

    translation.getTranslation($scope, translation.getLanguage());
});