﻿app.controller('listMovies', function ($scope, $rootScope, $http, $location, movieContainer, authentication, translation) {
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: ['5', '10', '15'],
        pageSize: '5',
        currentPage: 1
    };

    $scope.gridOptions = {
        data: 'movies',
        enablePaging: true,
        showFooter: true,
        columnDefs: [
            { field: 'Name', displayName: "Name", width: 500 },
            { field: 'GenreName', displayName: "Genre" },
            {
                displayName: '', cellTemplate:
                 '<div class="grid-action-cell">' +
                 '<button type="button" ng-click="editMovie(row.entity)">{{translation.EDIT}}</button></div>',
                 width:90 
            },
            {
                displayName: '', cellTemplate:
                 '<div class="grid-action-cell">' +
                 '<button type="button" ng-click="deleteMovie(row.entity.Id)">{{translation.DELETE}}</button></div>',
                width: 90
            }
        ],
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
    };

    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.movies = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getMovies($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }

        if (newVal !== oldVal && newVal.pageSize !== oldVal.pageSize) {
            $scope.pagingOptions.currentPage = 1;

            $scope.getMovies($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }
    }, true);

    $scope.getMovies = function (pageSize, page) {
        $http.get($rootScope.apiAddress + "api/Movie/GetMovies",
            {
                headers: { 'Authorization': 'Bearer ' + authentication.getAccessToken() }
            })
            .then(function (response) {
                $scope.movies = [];

                if (response.data.Success == true) {
                    $scope.setPagingData(response.data.Data, page, pageSize);
                }
            }, function (error) {
        });
    };

    $scope.editMovie = function (movie) {
        movieContainer.setCurrentMovie(movie);
        $location.path("/Edit");
    };

    $scope.deleteMovie = function (movieId) {
        if (confirm("Are you sure?")) {
            $http.post($rootScope.apiAddress + "api/Movie/DeleteMovie", movieId,
            {
                headers: { 'Authorization': 'Bearer ' + authentication.getAccessToken() }
            })
            .then(function (response) {
                if (response.data.Success == true) {
                    $scope.getMovies($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                }
            });
        }
    };

    $scope.addMovie = function () {
        $location.path("/Add");
    };

    $scope.getMovies($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch(function () { return localStorage.language; }, function (newVal, oldVal) {
        if (oldVal != newVal) {
            translation.getTranslation($scope, translation.getLanguage());
        }
    });

    translation.getTranslation($scope, translation.getLanguage());
}); 