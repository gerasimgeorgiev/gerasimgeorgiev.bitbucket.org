﻿using System.Web.Mvc;

namespace PoC.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MoviesList()
        {
            return View();
        }

        public ActionResult EditMovie()
        {
            return View();
        }

        public ActionResult AddMovie()
        {
            return View();
        }

        public ActionResult GenreAutocomplete()
        {
            return PartialView("~/Views/Home/_Autocomplete.cshtml");
        }

        public ActionResult Validation()
        {
            return PartialView("~/Views/Home/_Validation.cshtml");
        }
    }
}