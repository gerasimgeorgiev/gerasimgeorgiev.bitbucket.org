﻿using PoC.Core;
using PoC.DAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PoC.BLL
{
    public class MovieActions
    {
        public static List<MovieVM> GetMovies()
        {
            using (MoviesDBEntities dbContext = new MoviesDBEntities())
            {
                var movies = (from movie in dbContext.Movies
                              where movie.IsDeleted == false
                              select new MovieVM()
                              {
                                  Id = movie.MovieID,
                                  GenreId = movie.GenreId,
                                  GenreName = movie.Genre.Name,
                                  Name = movie.Name
                              }).ToList();

                return movies;
            }
        }

        public static List<GenreVM> GetGenres(string filter)
        {
            using (MoviesDBEntities dbContext = new MoviesDBEntities())
            {
                var genres = (from genre in dbContext.Genres
                              where string.IsNullOrEmpty(filter) ? true : genre.Name.Contains(filter)
                              select new GenreVM()
                              {
                                  Id = genre.Id,
                                  Name = genre.Name
                              }).ToList();

                return genres;
            }
        }

        public static void SaveMoviesChanges(MovieVM movie)
        {
            using (MoviesDBEntities dbContext = new MoviesDBEntities())
            {
                var movieToUpdate = dbContext.Movies.FirstOrDefault(m => m.MovieID == movie.Id);

                if (movieToUpdate == null)
                    throw new Exception("Movie not found");

                movieToUpdate.Name = movie.Name;
                movieToUpdate.GenreId = movie.GenreId;
                movieToUpdate.ModifiedDateTime = DateTime.Now;

                dbContext.SaveChanges();
            }
        }

        public static void DeleteMovie(int movieid)
        {
            using (MoviesDBEntities dbContext = new MoviesDBEntities())
            {
                var movieToUpdate = dbContext.Movies.FirstOrDefault(m => m.MovieID == movieid);

                if (movieToUpdate == null)
                    throw new Exception("Movie not found");

                movieToUpdate.IsDeleted = true;
                movieToUpdate.ModifiedDateTime = DateTime.Now;

                dbContext.SaveChanges();
            }
        }

        public static void AddNewMovie(MovieVM movie)
        {
            using (MoviesDBEntities dbContext = new MoviesDBEntities())
            {
                Movie newMoview = new Movie()
                {
                    Name = movie.Name,
                    GenreId = movie.GenreId,
                    IsDeleted = false,
                    CreatedDateTime = DateTime.Now,
                    ModifiedDateTime = DateTime.Now
                };

                dbContext.Movies.Add(newMoview);

                dbContext.SaveChanges();
            }
        }
    }
}
