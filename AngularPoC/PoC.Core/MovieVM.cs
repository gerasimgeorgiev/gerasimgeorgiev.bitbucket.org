﻿namespace PoC.Core
{
    public class MovieVM
    {
        public int Id { get; set; }
        public int GenreId { get; set; }
        public string GenreName { get; set; }
        public string Name { get; set; }
    }
}
