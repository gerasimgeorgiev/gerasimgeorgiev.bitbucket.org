﻿namespace PoC.Core
{
    public class GenreVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
