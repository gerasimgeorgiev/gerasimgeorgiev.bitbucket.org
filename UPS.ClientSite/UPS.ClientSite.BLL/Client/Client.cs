﻿using System;
using System.Collections.Generic;
using System.Linq;
using UPS.ClientSite.Core.Client;
using UPS.ClientSite.DAL.Models;

namespace UPS.ClientSite.BLL.Client
{
    public class ClientActions
    {
        public static List<ClientDTO> GetClients()
        {
            using (UPSClientSiteContext dbContext = new UPSClientSiteContext())
            {
                var clients = (from client in dbContext.Client
                               select new ClientDTO()
                               {
                                   Id = client.ClientId,
                                   Name = client.ClientName,
                                   Number = client.MasterClientNumber
                               }).ToList();

                foreach(ClientDTO client in clients)
                {
                    client.ClientAccounts = (from clientAccount in dbContext.ClientAccount
                                             where clientAccount.ClientId == client.Id
                                             select new ClientAccountDto()
                                             {
                                                 Id = clientAccount.ClientAccountId,
                                                 Name = clientAccount.ClientAccountName,
                                                 Number = clientAccount.ClientAccountNumber
                                             }).ToList();


                }

                return clients;
            }
        }
    }
}
