﻿using System;
using System.Linq;
using UPS.ClientSite.Core;
using UPS.ClientSite.DAL.Models;

namespace UPS.ClientSite.BLL
{
    public class MembershipActions
    {
        internal static int GetUserId(string userEmail, UPSClientSiteContext dbContext)
        {
            var userEntry = dbContext.ClientSiteUser.FirstOrDefault(m => m.Email == userEmail);

            if (userEntry == null)
                throw new Exception(string.Format("Could not find user with Email: {0}", userEmail));
            else
                return userEntry.ClientSiteUserId;
        }

        public static UserDTO GetBasicUserData(string userId)
        {
            using (UPSClientSiteContext dbContext = new UPSClientSiteContext())
            {
                var existingUser = dbContext.ClientSiteUser.FirstOrDefault(m => m.Email == userId);

                if (existingUser != null)
                {
                    return new UserDTO()
                    {
                        FirstName = existingUser.FirstName,
                        LastName = existingUser.LastName,
                        Email = userId
                    };
                }
                else
                    throw new Exception("Missing ClientSiteUser entry");
            }
        }

        public static void UpdateUserInDb(UserDTO newUser)
        {
            using (UPSClientSiteContext dbContext = new UPSClientSiteContext())
            {
                int userId;

                var existingUser = dbContext.ClientSiteUser.FirstOrDefault(m => m.Email == newUser.Email);

                if (existingUser != null)
                {
                    existingUser.FirstName = newUser.FirstName;
                    existingUser.LastName = newUser.LastName;
                    existingUser.ActiveFlag = true;

                    dbContext.SaveChanges();

                    userId = existingUser.ClientSiteUserId;
                }
                else
                {
                    ClientSiteUser newUserEntry = new ClientSiteUser()
                    {
                        Email = newUser.Email,
                        FirstName = newUser.FirstName,
                        LastName = newUser.LastName,
                        ActiveFlag = true
                    };

                    dbContext.ClientSiteUser.Add(newUserEntry);
                    dbContext.SaveChanges();

                    userId = newUserEntry.ClientSiteUserId;
                }

                // Create Client/Client Account association
                var clientAccountEntry = dbContext.ClientAccount.FirstOrDefault(m => m.ClientAccountId == newUser.ClientAccountId);

                if (clientAccountEntry == null)
                    throw new Exception(string.Format("Missing Client Account entry with id: {0}", newUser.ClientAccountId));

                var clientAccountUserAssociation = dbContext.ClientSiteUserAccountAssociation.FirstOrDefault(m => m.ClientAccountId == newUser.ClientAccountId && m.ClientSiteUserId == userId);

                if (clientAccountUserAssociation != null)
                    clientAccountUserAssociation.ActiveFlag = true;
                else
                {
                    var newClientAccountUserAssociation = new ClientSiteUserAccountAssociation()
                    {
                        ClientAccountId = newUser.ClientAccountId,
                        ClientSiteUserId = userId,
                        ActiveFlag = true
                    };

                    dbContext.ClientSiteUserAccountAssociation.Add(newClientAccountUserAssociation);
                }

                dbContext.SaveChanges();
            }
        }
    }
}
