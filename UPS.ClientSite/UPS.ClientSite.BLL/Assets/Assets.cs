﻿using System;
using System.Collections.Generic;
using System.Linq;
using UPS.ClientSite.Core.Assets;
using UPS.ClientSite.DAL.Models;

namespace UPS.ClientSite.BLL.Assets
{
    public class AssetActions
    {
        public static List<AssetDto> GetFolderContent(AssetsFilterDto filter)
        {
            using (UPSClientSiteContext dbContext = new UPSClientSiteContext())
            {
                if (string.IsNullOrEmpty(filter.CurrentUserEmail))
                    throw new Exception("Missing current user's email within filter object");

                int userId = MembershipActions.GetUserId(filter.CurrentUserEmail, dbContext);

                var assetsList = new List<AssetDto>();

                var clientAccountIdsForUser = dbContext.ClientSiteUserAccountAssociation
                    .Where(m => m.ClientSiteUserId == userId && m.ActiveFlag == true)
                    .Select(n => n.ClientAccountId).ToList();

                if (filter.IsRootLevel)
                {
                    if (clientAccountIdsForUser != null && clientAccountIdsForUser.Count > 0)
                    {
                        assetsList = (from clientAccountFolder in dbContext.ClientAccountFolder
                                      where clientAccountIdsForUser.Contains(clientAccountFolder.ClientAccountId)
                                      select new AssetDto()
                                      {
                                          DbAssetId = clientAccountFolder.ClientAccountFolderId,
                                          Name = clientAccountFolder.RootFolderName,
                                          IsRootFolder = true,
                                          IsFolder = true,
                                          RootFolderId = clientAccountFolder.ClientAccountFolderId,
                                          LastModifiedDate = clientAccountFolder.LastModifiedDate
                                      }).ToList();
                    }
                }
                else
                {
                    if (filter.DbFolderId > 0)
                    {
                        if(filter.IsRootFolder)
                        {
                            // Get sub-folders
                            var subFolders = (from subFolder in dbContext.SubFolder
                                              where !subFolder.ParentSubFolderId.HasValue &&
                                              subFolder.ClientAccountFolderId == filter.DbFolderId && subFolder.DeletedFlag == false
                                              select new AssetDto()
                                              {
                                                  IsRootFolder = false,
                                                  DbAssetId = subFolder.SubFolderId,
                                                  IsFolder = true,
                                                  Name = subFolder.SubFolderName,
                                                  RootFolderId = subFolder.ClientAccountFolderId,
                                                  LastModifiedDate = subFolder.LastModifiedDate
                                              }).ToList();

                            // Get Files
                            var subFiles = (from subFile in dbContext.File
                                              where !subFile.SubFolderId.HasValue &&
                                              subFile.ClientAccountFolderId == filter.DbFolderId && subFile.DeletedFlag == false
                                              select new AssetDto()
                                              {
                                                  IsRootFolder = false,
                                                  DbAssetId = subFile.FileId,
                                                  IsFolder = false,
                                                  Name = subFile.FileName,
                                                  AssetUri = subFile.FileReferenceNumber,
                                                  RootFolderId = subFile.ClientAccountFolderId,
                                                  LastModifiedDate = subFile.LastModifiedDate,
                                                  FileExtension = System.IO.Path.GetExtension(subFile.FileName),
                                                  FileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(subFile.FileName)
                                              }).ToList();

                            assetsList = subFolders.Union(subFiles).ToList();
                        }
                        else
                        {
                            // Get sub-folders
                            var subFolders = (from subFolder in dbContext.SubFolder
                                              where subFolder.ParentSubFolderId.HasValue &&
                                              subFolder.ParentSubFolderId.Value == filter.DbFolderId && subFolder.DeletedFlag == false
                                              select new AssetDto()
                                              {
                                                  IsRootFolder = false,
                                                  DbAssetId = subFolder.SubFolderId,
                                                  IsFolder = true,
                                                  Name = subFolder.SubFolderName,
                                                  RootFolderId = subFolder.ClientAccountFolderId,
                                                  ParentFolderId = subFolder.ParentSubFolderId,
                                                  LastModifiedDate = subFolder.LastModifiedDate
                                              }).ToList();

                            // Get Files
                            var subFiles = (from subFile in dbContext.File
                                            where subFile.SubFolderId.HasValue &&
                                            subFile.SubFolderId.Value == filter.DbFolderId && subFile.DeletedFlag == false
                                            select new AssetDto()
                                            {
                                                IsRootFolder = false,
                                                DbAssetId = subFile.FileId,
                                                IsFolder = false,
                                                Name = subFile.FileName,
                                                AssetUri = subFile.FileReferenceNumber,
                                                RootFolderId = subFile.ClientAccountFolderId,
                                                ParentFolderId = subFile.SubFolderId,
                                                LastModifiedDate = subFile.LastModifiedDate,
                                                FileExtension = System.IO.Path.GetExtension(subFile.FileName),
                                                FileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(subFile.FileName)
                                            }).ToList();

                            assetsList = subFolders.Union(subFiles).ToList();
                        }
                    }
                }

                return assetsList;
            }
        }

        public static List<string> GetAllFileUrisWithinFolder(AssetsFilterDto filter, List<string> fileNames)
        {
            var assetsList = GetFolderContent(filter);
            fileNames.AddRange(assetsList.Where(m => m.IsFolder == false).Select(n => n.AssetUri));

            filter.IsRootFolder = false;
            filter.IsRootLevel = false;

            foreach (AssetDto subFolder in assetsList.Where(m => m.IsFolder == true))
            {
                filter.DbFolderId = subFolder.DbAssetId;

                var moreFileNames = GetAllFileUrisWithinFolder(filter, fileNames);
                fileNames.AddRange(moreFileNames);
            }

            return fileNames;
        }

        public static void AddNewAsset(AssetDto assetDto)
        {
            using (UPSClientSiteContext dbContext = new UPSClientSiteContext())
            {
                var rootFolderEntry = dbContext.ClientAccountFolder.FirstOrDefault(m => m.ClientAccountFolderId == assetDto.RootFolderId);

                if (rootFolderEntry != null)
                {
                    if (assetDto.ParentFolderId > 0)
                    {
                        var parentFolderEntry = dbContext.SubFolder.FirstOrDefault(m => m.SubFolderId == assetDto.ParentFolderId);

                        if (parentFolderEntry != null)
                        {
                            CreateNewAsset(dbContext, assetDto);
                        }
                        else
                            throw new Exception(string.Format("Missing folder with id: {0}", assetDto.ParentFolderId.ToString()));
                    }
                    else
                    {
                        // Create asset on root folder level
                        CreateNewAsset(dbContext, assetDto);
                    }
                }
                else
                    throw new Exception(string.Format("Missing root folder with id: {0}", assetDto.RootFolderId.ToString()));
            }
        }

        private static void CreateNewAsset(UPSClientSiteContext dbContext, AssetDto assetDto)
        {
            if (!assetDto.IsFolder)
            {
                var newAsset = new File()
                {
                    ClientAccountFolderId = assetDto.RootFolderId,
                    DeletedFlag = false,
                    FileName = assetDto.Name,
                    FileReferenceNumber = assetDto.AssetUri,
                    LastModifiedDate = DateTime.Now
                };

                if (assetDto.ParentFolderId > 0)
                    newAsset.SubFolderId = assetDto.ParentFolderId;

                dbContext.File.Add(newAsset);
            }
            else
            {
                var newFolder = new SubFolder()
                {
                    ClientAccountFolderId = assetDto.RootFolderId,
                    DeletedFlag = false,
                    SubFolderName = assetDto.Name,
                    LastModifiedDate = DateTime.Now
                };

                if (assetDto.ParentFolderId > 0)
                    newFolder.ParentSubFolderId = assetDto.ParentFolderId;

                dbContext.SubFolder.Add(newFolder);
            }
            dbContext.SaveChanges();
        }

        public static void EditFolder(AssetDto assetDto)
        {
            using (UPSClientSiteContext dbContext = new UPSClientSiteContext())
            {
                var folderEntry = dbContext.SubFolder.FirstOrDefault(m => m.SubFolderId == assetDto.DbAssetId);

                if (folderEntry != null)
                {
                    folderEntry.SubFolderName = assetDto.Name;
                    folderEntry.LastModifiedDate = DateTime.Now;

                    dbContext.SaveChanges();
                }
                else
                    throw new Exception(string.Format("Missing folder with id: {0}", assetDto.DbAssetId.ToString()));
            }
        }

        public static void DeleteAsset(AssetDto assetDto)
        {
            using (UPSClientSiteContext dbContext = new UPSClientSiteContext())
            {
                if (assetDto.IsFolder)
                {
                    var assetToDelete = dbContext.SubFolder.FirstOrDefault(m => m.SubFolderId == assetDto.DbAssetId);

                    if (assetToDelete != null)
                    {
                        assetToDelete.DeletedFlag = true;
                        assetToDelete.LastModifiedDate = DateTime.Now;

                        dbContext.SaveChanges();
                    }
                    else
                        throw new Exception(string.Format("Missing folder with id: {0}", assetDto.DbAssetId.ToString()));
                }
                else
                {
                    var assetToDelete = dbContext.File.FirstOrDefault(m => m.FileId == assetDto.DbAssetId);

                    if (assetToDelete != null)
                    {
                        assetToDelete.DeletedFlag = true;
                        assetToDelete.LastModifiedDate = DateTime.Now;

                        dbContext.SaveChanges();
                    }
                    else
                        throw new Exception(string.Format("Missing file with id: {0}", assetDto.DbAssetId.ToString()));
                }
            }
        }
    }
}
