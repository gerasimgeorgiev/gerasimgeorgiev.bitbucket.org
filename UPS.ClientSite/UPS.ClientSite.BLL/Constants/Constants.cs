﻿namespace UPS.ClientSite.BLL.Constants
{
    public class Constants
    {
        public static string SuperAdminRole = "SuperAdmins";
        public static string UPSUserRole = "UPSUser";
        public static string ClientUserRole = "ClientUser";
    }
}
