using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using UPS.ClientSite.Core.UserAccount;

namespace UPS.ClientSite.Tests
{
    [TestClass]
    public class AuthenticationTest
    {
        [TestInitialize]
        public void Init()
        {
            TestAuthentication();
        }

        [TestMethod]
        public void TestAuthentication()
        {
            var samlResponse = Login();

            var deserializedResponse = JsonConvert.DeserializeObject(samlResponse);

            var samlToken = JsonConvert.DeserializeObject<dynamic>(samlResponse).data;

            AccessProtectedResourceWithSaml(samlToken.ToString());
        }

        public string Login()
        {
            string targetAPIEndpoint = "http://localhost:2205/api/Account/Login";

            var request = (HttpWebRequest)WebRequest.Create(targetAPIEndpoint);

            var serializedData = JsonConvert.SerializeObject(new LoginDTO()
            {
                Email = "superadmin@philips.com",
                Password = "Secret0107$",
                ReturnUrl = "sadfasdf",
                Requester = "test"
            });

            var data = Encoding.UTF8.GetBytes(serializedData);

            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.ContentLength = data.Length;

            // 3 minutes
            request.Timeout = 180000;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString;
        }

        public void AccessProtectedResourceWithSaml(string saml)
        {
            string targetAPIEndpoint = "http://localhost:2205/api/Home/ProtectedResource";

            var request = (HttpWebRequest)WebRequest.Create(targetAPIEndpoint);

            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Headers.Add("Authorization", "" + saml);
            request.ContentLength = 0;

            // 3 minutes
            request.Timeout = 180000;

            //using (var stream = request.GetRequestStream())
            //{
            //    stream.Write(data, 0, data.Length);
            //}

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
    }
}
