﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class ClientSiteUser
    {
        public ClientSiteUser()
        {
            ClientSiteUserAccountAssociation = new HashSet<ClientSiteUserAccountAssociation>();
        }

        public int ClientSiteUserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool? ActiveFlag { get; set; }

        public ICollection<ClientSiteUserAccountAssociation> ClientSiteUserAccountAssociation { get; set; }
    }
}
