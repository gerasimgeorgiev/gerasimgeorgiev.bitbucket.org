﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class ClientAccountOwner
    {
        public int ClientAccountOwnerId { get; set; }
        public int ClientAccountId { get; set; }
        public int ClientsiteUpsuser { get; set; }
        public int ClientAccountOwnerRoleId { get; set; }
        public bool ActiveFlagId { get; set; }
    }
}
