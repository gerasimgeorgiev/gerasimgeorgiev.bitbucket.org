﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class ClientAccountFolder
    {
        public ClientAccountFolder()
        {
            File = new HashSet<File>();
            SubFolder = new HashSet<SubFolder>();
        }

        public int ClientAccountFolderId { get; set; }
        public string RootFolderName { get; set; }
        public int ClientAccountId { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public ClientAccount ClientAccount { get; set; }
        public ICollection<File> File { get; set; }
        public ICollection<SubFolder> SubFolder { get; set; }
    }
}
