﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class SubFolder
    {
        public SubFolder()
        {
            File = new HashSet<File>();
            InverseParentSubFolder = new HashSet<SubFolder>();
        }

        public int SubFolderId { get; set; }
        public int? ParentSubFolderId { get; set; }
        public int ClientAccountFolderId { get; set; }
        public string SubFolderName { get; set; }
        public bool DeletedFlag { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public ClientAccountFolder ClientAccountFolder { get; set; }
        public SubFolder ParentSubFolder { get; set; }
        public ICollection<File> File { get; set; }
        public ICollection<SubFolder> InverseParentSubFolder { get; set; }
    }
}
