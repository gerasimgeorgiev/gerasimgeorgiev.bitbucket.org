﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class ClientAccount
    {
        public ClientAccount()
        {
            ClientAccountFolder = new HashSet<ClientAccountFolder>();
            ClientSiteUserAccountAssociation = new HashSet<ClientSiteUserAccountAssociation>();
        }

        public int ClientAccountId { get; set; }
        public string ClientAccountNumber { get; set; }
        public string ClientAccountName { get; set; }
        public int? ClientId { get; set; }

        public Client Client { get; set; }
        public ICollection<ClientAccountFolder> ClientAccountFolder { get; set; }
        public ICollection<ClientSiteUserAccountAssociation> ClientSiteUserAccountAssociation { get; set; }
    }
}
