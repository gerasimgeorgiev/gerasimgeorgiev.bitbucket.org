﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class Client
    {
        public Client()
        {
            ClientAccount = new HashSet<ClientAccount>();
        }

        public int ClientId { get; set; }
        public string MasterClientNumber { get; set; }
        public string ClientName { get; set; }

        public ICollection<ClientAccount> ClientAccount { get; set; }
    }
}
