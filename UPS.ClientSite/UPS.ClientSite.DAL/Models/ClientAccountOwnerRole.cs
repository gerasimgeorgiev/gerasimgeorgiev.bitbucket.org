﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class ClientAccountOwnerRole
    {
        public int ClientAccountOwnerRoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public bool? ActiveFlag { get; set; }
    }
}
