﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class ClientSiteUserAccountAssociation
    {
        public int ClientSiteUserAccountAssociationId { get; set; }
        public int? ClientSiteUserId { get; set; }
        public int? ClientAccountId { get; set; }
        public bool? ActiveFlag { get; set; }

        public ClientAccount ClientAccount { get; set; }
        public ClientSiteUser ClientSiteUser { get; set; }
    }
}
