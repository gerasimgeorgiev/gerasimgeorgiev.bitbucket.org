﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class ClientsiteUpsuser
    {
        public int ClientsiteUpsuserId { get; set; }
        public bool? ActiveFlag { get; set; }
    }
}
