﻿using System;
using System.Collections.Generic;

namespace UPS.ClientSite.DAL.Models
{
    public partial class File
    {
        public int FileId { get; set; }
        public string FileName { get; set; }
        public int ClientAccountFolderId { get; set; }
        public int? SubFolderId { get; set; }
        public bool DeletedFlag { get; set; }
        public string FileReferenceNumber { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public ClientAccountFolder ClientAccountFolder { get; set; }
        public SubFolder SubFolder { get; set; }
    }
}
