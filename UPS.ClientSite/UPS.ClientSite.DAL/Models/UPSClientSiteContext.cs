﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace UPS.ClientSite.DAL.Models
{
    public partial class UPSClientSiteContext : DbContext
    {
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientAccount> ClientAccount { get; set; }
        public virtual DbSet<ClientAccountFolder> ClientAccountFolder { get; set; }
        public virtual DbSet<ClientAccountOwner> ClientAccountOwner { get; set; }
        public virtual DbSet<ClientAccountOwnerRole> ClientAccountOwnerRole { get; set; }
        public virtual DbSet<ClientsiteUpsuser> ClientsiteUpsuser { get; set; }
        public virtual DbSet<ClientSiteUser> ClientSiteUser { get; set; }
        public virtual DbSet<ClientSiteUserAccountAssociation> ClientSiteUserAccountAssociation { get; set; }
        public virtual DbSet<File> File { get; set; }
        public virtual DbSet<SubFolder> SubFolder { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(DbConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ClientName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MasterClientNumber)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientAccount>(entity =>
            {
                entity.Property(e => e.ClientAccountId).HasColumnName("ClientAccountID");

                entity.Property(e => e.ClientAccountName)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ClientAccountNumber)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientAccount)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK_ClientAccount_Client");
            });

            modelBuilder.Entity<ClientAccountFolder>(entity =>
            {
                entity.Property(e => e.ClientAccountFolderId).HasColumnName("ClientAccountFolderID");

                entity.Property(e => e.ClientAccountId).HasColumnName("ClientAccountID");

                entity.Property(e => e.RootFolderName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.HasOne(d => d.ClientAccount)
                    .WithMany(p => p.ClientAccountFolder)
                    .HasForeignKey(d => d.ClientAccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientAccountFolder_ClientAccount");
            });

            modelBuilder.Entity<ClientAccountOwner>(entity =>
            {
                entity.Property(e => e.ClientAccountOwnerId)
                    .HasColumnName("ClientAccountOwnerID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActiveFlagId).HasColumnName("ActiveFlagID");

                entity.Property(e => e.ClientAccountId).HasColumnName("ClientAccountID");

                entity.Property(e => e.ClientAccountOwnerRoleId).HasColumnName("ClientAccountOwnerRoleID");

                entity.Property(e => e.ClientsiteUpsuser).HasColumnName("ClientsiteUPSUser");
            });

            modelBuilder.Entity<ClientAccountOwnerRole>(entity =>
            {
                entity.Property(e => e.ClientAccountOwnerRoleId).HasColumnName("ClientAccountOwnerRoleID");

                entity.Property(e => e.ActiveFlag).HasDefaultValueSql("((1))");

                entity.Property(e => e.RoleDescription)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientsiteUpsuser>(entity =>
            {
                entity.ToTable("ClientsiteUPSUser");

                entity.Property(e => e.ClientsiteUpsuserId).HasColumnName("ClientsiteUPSUserID");

                entity.Property(e => e.ActiveFlag).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<ClientSiteUser>(entity =>
            {
                entity.Property(e => e.ClientSiteUserId).HasColumnName("ClientSiteUserID");

                entity.Property(e => e.ActiveFlag).HasDefaultValueSql("((1))");

                entity.Property(e => e.Email).IsRequired();
            });

            modelBuilder.Entity<ClientSiteUserAccountAssociation>(entity =>
            {
                entity.Property(e => e.ClientSiteUserAccountAssociationId).HasColumnName("ClientSiteUserAccountAssociationID");

                entity.Property(e => e.ActiveFlag).HasDefaultValueSql("((1))");

                entity.Property(e => e.ClientAccountId).HasColumnName("ClientAccountID");

                entity.Property(e => e.ClientSiteUserId).HasColumnName("ClientSiteUserID");

                entity.HasOne(d => d.ClientAccount)
                    .WithMany(p => p.ClientSiteUserAccountAssociation)
                    .HasForeignKey(d => d.ClientAccountId)
                    .HasConstraintName("FK_ClientSiteUserAccountAssociation_ClientAccount");

                entity.HasOne(d => d.ClientSiteUser)
                    .WithMany(p => p.ClientSiteUserAccountAssociation)
                    .HasForeignKey(d => d.ClientSiteUserId)
                    .HasConstraintName("FK_ClientSiteUserAccountAssociation_ClientSiteUser");
            });

            modelBuilder.Entity<File>(entity =>
            {
                entity.Property(e => e.FileId).HasColumnName("FileID");

                entity.Property(e => e.ClientAccountFolderId).HasColumnName("ClientAccountFolderID");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FileReferenceNumber).IsRequired();

                entity.Property(e => e.SubFolderId).HasColumnName("SubFolderID");

                entity.HasOne(d => d.ClientAccountFolder)
                    .WithMany(p => p.File)
                    .HasForeignKey(d => d.ClientAccountFolderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_File_ClientAccountFolder");

                entity.HasOne(d => d.SubFolder)
                    .WithMany(p => p.File)
                    .HasForeignKey(d => d.SubFolderId)
                    .HasConstraintName("FK_File_SubFolder");
            });

            modelBuilder.Entity<SubFolder>(entity =>
            {
                entity.Property(e => e.SubFolderId).HasColumnName("SubFolderID");

                entity.Property(e => e.ClientAccountFolderId).HasColumnName("ClientAccountFolderID");

                entity.Property(e => e.ParentSubFolderId).HasColumnName("ParentSubFolderID");

                entity.Property(e => e.SubFolderName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.HasOne(d => d.ClientAccountFolder)
                    .WithMany(p => p.SubFolder)
                    .HasForeignKey(d => d.ClientAccountFolderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SubFolder_ClientAccountFolder");

                entity.HasOne(d => d.ParentSubFolder)
                    .WithMany(p => p.InverseParentSubFolder)
                    .HasForeignKey(d => d.ParentSubFolderId)
                    .HasConstraintName("FK_SubFolder_SubFolder");
            });
        }
    }
}
