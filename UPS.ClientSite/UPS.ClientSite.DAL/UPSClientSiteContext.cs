﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.IO;
using UPS.ClientSite.Core.Common;

namespace UPS.ClientSite.DAL.Models
{
    public partial class UPSClientSiteContext : DbContext
    {
        public string DbConnectionString { get; set; }

        public UPSClientSiteContext() : base()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);

            var root = configurationBuilder.Build();
            DbConnectionString = root.GetSection("Data:UPSClientSite")?["UPSClientSiteDbConnection"];
        }
    }
}
