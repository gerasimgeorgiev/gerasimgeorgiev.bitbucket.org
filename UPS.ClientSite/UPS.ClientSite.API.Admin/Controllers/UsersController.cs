﻿using B2CGraphShell;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UPS.ClientSite.BLL;
using UPS.ClientSite.Core;

namespace UPS.ClientSite.API.Admin.Controllers
{
    [Authorize]
    [Route("api/Users")]
    public class UsersController : BaseController
    {
        public UsersController(IConfiguration Configuration) : base(Configuration)
        {
        }

        [HttpGet]
        [Route("~/api/Users/IsValidUser")]
        public IActionResult IsValidUser()
        {
            if (User.Identity.IsAuthenticated)
                return Json(new { Success = true });
            else
                return Json(new { Success = false });
        }

        [HttpPost]
        [Route("~/api/Users/CreateNewUser")]
        public async Task<IActionResult> CreateNewUser([FromBody] UserDTO newUserData)
        {
            try
            {
                var jsonObject = new JObject
                        {
                            {"accountEnabled", true},
                            {"country", "Test Country"},
                            {"creationType", "LocalAccount"},
                            {"displayName", newUserData.FirstName +" " + newUserData.LastName},
                            {"passwordPolicies", "DisablePasswordExpiration,DisableStrongPassword"},
                            {"passwordProfile", new JObject
                            {
                                {"password", newUserData.Password},
                                {"forceChangePasswordNextLogin", true}
                            } },
                            {"signInNames", new JArray
                                {
                                    new JObject
                                    {
                                        {"value", newUserData.Email},
                                        {"type", "emailAddress"}
                                    }
                                }
                            }
                        };

                string clientId = "09d4254c-e56b-4a46-8480-e5911fdfdbc8";
                string tenantId = "ClientSiteB2cTenant.onmicrosoft.com";
                string clientSecret = "euMPoOUKWbUAefouFPobtkO7EuAbxs4v5fxeuhaZUz8=";

                var client = new B2CGraphClient(clientId, clientSecret, tenantId);
                var response = await client.CreateUser(jsonObject.ToString());

                MembershipActions.UpdateUserInDb(newUserData);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpPost]
        [Route("~/api/Users/UpdateUser")]
        public async Task<IActionResult> UpdateUser([FromBody] UserDTO userData)
        {
            try
            {
                bool success = await LoginWithSuperAdmin();

                string newUserEndpoint = string.Format("{0}{1}", upsClientSiteAPIUrl, "Account/UpdateUser");

                var response = await CreateAuthorizedJsonPostRequest(newUserEndpoint, userData);

                var dynamicObjParsed = JsonConvert.DeserializeObject<dynamic>(response);

                if (dynamicObjParsed["success"] == "true")
                    return Json(new { Success = true });
                else
                    return Json(new { Success = false, Data = dynamicObjParsed["data"] });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpPost]
        [Route("~/api/Users/DisableUser")]
        public async Task<IActionResult> DisableUser([FromBody] UserDTO userData)
        {
            try
            {
                bool success = await LoginWithSuperAdmin();

                string newUserEndpoint = string.Format("{0}{1}", upsClientSiteAPIUrl, "Account/DisableUser");

                var response = await CreateAuthorizedJsonPostRequest(newUserEndpoint, userData);

                var dynamicObjParsed = JsonConvert.DeserializeObject<dynamic>(response);

                if (dynamicObjParsed["success"] == "true")
                    return Json(new { Success = true });
                else
                    return Json(new { Success = false, Data = dynamicObjParsed["data"] });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpGet]
        [Route("~/api/Users/GetRoles")]
        public async Task<IActionResult> GetUserRoles()
        {
            try
            {
                bool success = await LoginWithSuperAdmin();

                string newUserEndpoint = string.Format("{0}{1}", upsClientSiteAPIUrl, "Account/GetRoles");

                var response = await CreateAuthorizedJsonGetRequest(newUserEndpoint);

                var dynamicObjParsed = JsonConvert.DeserializeObject<dynamic>(response);

                if (dynamicObjParsed["success"] == "true")
                {
                    var roles = ((JObject)dynamicObjParsed).GetValue("data").ToObject(typeof(List<string>));

                    return Json(new { Success = true, Data = roles });
                }
                else
                    return Json(new { Success = false, Data = dynamicObjParsed["data"] });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpGet]
        [Route("~/api/Users/GetUsers")]
        public async Task<IActionResult> GetPortalUsers()
        {
            try
            {
                string clientId = "09d4254c-e56b-4a46-8480-e5911fdfdbc8";
                string tenantId = "ClientSiteB2cTenant.onmicrosoft.com";
                string clientSecret = "euMPoOUKWbUAefouFPobtkO7EuAbxs4v5fxeuhaZUz8=";

                var client = new B2CGraphClient(clientId, clientSecret, tenantId);
                var response = client.GetAllUsers(null).Result;

                object formatted = JsonConvert.DeserializeObject(response);

                return Json(new { Success = true, Data = formatted });

                //bool success = await LoginWithSuperAdmin();

                //string newUserEndpoint = string.Format("{0}{1}", upsClientSiteAPIUrl, "Account/GetUsers");

                //var response = await CreateAuthorizedJsonGetRequest(newUserEndpoint);

                //var dynamicObjParsed = JsonConvert.DeserializeObject<dynamic>(response);

                //if (dynamicObjParsed["success"] == "true")
                //{
                //    var users = ((JObject)dynamicObjParsed).GetValue("data").ToObject(typeof(List<UserDTO>));

                //    return Json(new { Success = true, Data = users });
                //}
                //else
                //    return Json(new { Success = false, Data = dynamicObjParsed["data"] });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }
    }
}
