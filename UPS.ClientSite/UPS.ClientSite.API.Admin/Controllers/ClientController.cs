﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UPS.ClientSite.BLL.Client;

namespace UPS.ClientSite.API.Admin.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Client")]
    public class ClientController : Controller
    {
        [HttpGet]
        [Route("~/api/Client/Clients")]
        public async Task<IActionResult> GetClients()
        {
            try
            {
                var clients = ClientActions.GetClients();

                return Json(new { Success = true, Data = clients });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }
    }
}