﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UPS.ClientSite.Core.UserAccount;

namespace UPS.ClientSite.API.Admin.Controllers
{
    [Authorize]
    [Produces("application/json")]
    public class BaseController : Controller
    {
        protected IConfiguration _configuration;
        protected string upsClientSiteAPIUrl;

        public BaseController(IConfiguration Configuration)
        {
            _configuration = Configuration;
            upsClientSiteAPIUrl = _configuration.GetSection("AppSettings")?["UPSClientSiteAPIUrl"];
        }

        protected async Task<bool> LoginWithSuperAdmin()
        {
            var samlToken = Request.Cookies["SamlToken"];

            if (string.IsNullOrEmpty(samlToken))
            {
                string loginEndpoint = string.Format("{0}{1}", upsClientSiteAPIUrl, "Account/Login");

                LoginDTO superAdminLogin = new LoginDTO()
                {
                    Email = "superadmin@philips.com",
                    Password = "Secret0107$",
                    Requester = "test"
                };

                var response = await CreateAnonynousJsonPostRequest(loginEndpoint, superAdminLogin);

                var dynamicObjParsed = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(response);

                samlToken = dynamicObjParsed["data"];
            }

            CookieOptions option = new CookieOptions();
            option.Expires = DateTime.Now.AddMinutes(240);

            Response.Cookies.Append("SamlToken", samlToken, option);

            return true;
        }

        protected async Task<string> CreateAuthorizedJsonPostRequest(string endpoint, object postData)
        {
            var samlToken = Request.Cookies["SamlToken"];

            var httpContent = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(postData), Encoding.UTF8, "application/json");

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + samlToken);

                using (HttpResponseMessage res = await client.PostAsync(endpoint, httpContent))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();

                        return data;
                    }
                }
            }
        }

        protected async Task<string> CreateAuthorizedJsonGetRequest(string endpoint)
        {
            var samlToken = Request.Cookies["SamlToken"];

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + samlToken);

                using (HttpResponseMessage res = await client.GetAsync(endpoint))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();

                        return data;
                    }
                }
            }
        }

        private async Task<string> CreateAnonynousJsonPostRequest(string endpoint, object postData)
        {
            var httpContent = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(postData), Encoding.UTF8, "application/json");

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage res = await client.PostAsync(endpoint, httpContent))
                {
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();

                        return data;
                    }
                }
            }
        }
    }
}