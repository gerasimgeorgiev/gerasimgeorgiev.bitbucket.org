import { Component, OnInit, Inject } from '@angular/core';
import { AssetsService } from '../../../../services/assets.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modalmessage',
  templateUrl: './modalmessage.component.html',
  styleUrls: ['./modalmessage.component.css']
})
export class ModalMessageComponent implements OnInit {
  message: string;

  constructor(private assetService: AssetsService, private dialogRef: MatDialogRef<ModalMessageComponent>,
    @Inject(MAT_DIALOG_DATA) public inputMessage: string) {
      this.message = inputMessage;
    }

  ngOnInit() {
    this.assetService.getReloadAssetsEmitter().subscribe(result => {
      this.dialogRef.close();
    });

    this.assetService.getZipFileAvailableForDownloadEmitter().subscribe(result => {
      this.dialogRef.close();
    });
  }

  closeModalMessagePopup(): void {
    this.dialogRef.close();
  }

}
