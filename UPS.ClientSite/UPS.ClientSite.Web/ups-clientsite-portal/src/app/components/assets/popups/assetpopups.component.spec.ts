import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetpopupsComponent } from './assetpopups.component';

describe('AssetpopupsComponent', () => {
  let component: AssetpopupsComponent;
  let fixture: ComponentFixture<AssetpopupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetpopupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetpopupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
