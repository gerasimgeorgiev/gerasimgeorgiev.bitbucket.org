import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AssetDto } from '../../../classes/assets/asset';
import { AssetsService } from '../../../services/assets.service';

@Component({
  selector: 'app-newfolder',
  templateUrl: './newfolder.component.html',
  styleUrls: ['./newfolder.component.css']
})
export class NewFolderComponent implements OnInit {
  folderData: AssetDto;
  isNewFolder: boolean;
  editFolder: boolean;
  addEditBtnText: string;
  folderName: string;

  constructor(private assetService: AssetsService, private dialogRef: MatDialogRef<NewFolderComponent>,
    @Inject(MAT_DIALOG_DATA) public inputFolderData: AssetDto) {

    if (inputFolderData.DbAssetId > 0) {
      this.editFolder = true;
      this.isNewFolder = false;
      this.addEditBtnText = 'Edit';
      this.folderName = inputFolderData.Name;
    } else {
      this.editFolder = false;
      this.isNewFolder = true;
      this.addEditBtnText = 'Create';
    }
  }

  cancelModal(): void {
    this.dialogRef.close();
  }

  addEditFolder(): void {
    if (this.folderName.trim().length > 0) {
    this.inputFolderData.Name = this.folderName;

    const result = this.assetService.addEditFolder(this.inputFolderData).subscribe(res => {
      this.assetService.emitReloadAssetsEvent();

      this.dialogRef.close();
    });
  }
  }

  ngOnInit() {
    if (this.isNewFolder) {
      this.folderData = new AssetDto();
    }
  }

}
