import { Component, OnInit } from '@angular/core';
import { AssetsService } from '../../../services/assets.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalMessageComponent } from './modalmessage/modalmessage.component';

declare var myExtObject: any;

@Component({
  selector: 'app-assetpopups',
  templateUrl: './assetpopups.component.html',
  styleUrls: ['./assetpopups.component.css']
})

export class AssetpopupsComponent implements OnInit {
  totalFileParts: number;
  currentFilePart: number;
  tempCounter: number;
  uploadProgress: number;
  stringifiedCurrentFolder: string;

  constructor(private assetService: AssetsService, public dialog: MatDialog) { }

  ngOnInit() {
    this.assetService.getAssetUploadStatusEmitter().subscribe(result => {
      this.updateUploadProgress();
    });
  }

  updateUploadProgress(): void {
    this.tempCounter++;
    this.uploadProgress = (this.tempCounter / this.totalFileParts) * 100;

    if (this.uploadProgress === 100) {
      const dialogRef = this.dialog.open(ModalMessageComponent, {
        width: '350px',
        data: 'Your file is being uploaded to the cloud. Once the operation is done, the assets list would be reloaded'
      });

      myExtObject.closePopup(null);
      this.tempCounter = 0;
    }
  }

  closePopup(event): void {
    const target = event.target || event.srcElement || event.currentTarget;
    myExtObject.closePopup(target);
  }

  deleteAsset(): void {
    const stringifiedAsset = myExtObject.getStringifiedAssetToDelete();
    const assetToDelete = JSON.parse(stringifiedAsset);

    myExtObject.closePopup(null);

    this.assetService.deleteAsset(assetToDelete).subscribe(result => {
      this.assetService.emitReloadAssetsEvent();
    });
  }

  upload(event): void {
    const files: FileList = event.target.files;

    if (files.length === 0) {
        return;
    }

    const file = files[0];

    this.tempCounter = 0;

    // create array to store the buffer chunks
    const FileChunk = [];
    // the file object itself that we will work with
    // set up other initial vars
    const MaxFileSizeMB = 1;
    const BufferChunkSize = MaxFileSizeMB * (1024 * 1024);
    const ReadBuffer_Size = 1024;
    let FileStreamPos = 0;
    // set the initial chunk length
    let EndPos = BufferChunkSize;
    const Size = file.size;

    // add to the FileChunk array until we get to the end of the file
    while (FileStreamPos < Size) {
        // "slice" the file from the starting position/offset, to  the required length
        FileChunk.push(file.slice(FileStreamPos, EndPos));
        FileStreamPos = EndPos; // jump by the amount read
        EndPos = FileStreamPos + BufferChunkSize; // set next chunk length
    }
    // get total number of "files" we will be sending
    this.totalFileParts = FileChunk.length;
    const TotalPartsAsString = this.totalFileParts.toLocaleString();
    this.currentFilePart = 0;
    // loop through, pulling the first item from the array each time and sending it
    let chunk: any;
    while (chunk = FileChunk.shift()) {
      this.currentFilePart++;
        // file name convention
        const partCountFormatted = this.padLeft(this.currentFilePart.toLocaleString(), '0', TotalPartsAsString.length);

        const FilePartName = file.name + '.part_' + partCountFormatted + '.' + this.totalFileParts;
        // send the file
        this.UploadFileChunk(chunk, FilePartName);
    }
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }

  UploadFileChunk(Chunk, FileName): void {
    const FD = new FormData();
    FD.append('file', Chunk, FileName);

    this.stringifiedCurrentFolder = myExtObject.getStringifiedCurrentFolder();
    const currentFolder = JSON.parse(this.stringifiedCurrentFolder);

    const rootDirDbId = currentFolder.RootFolderId;
    let parentDirDbId = 0;

    if (!currentFolder.IsRootFolder) {
      parentDirDbId = currentFolder.DbAssetId;
    }

    FD.append('rootDirDbId', rootDirDbId.toString());
    FD.append('parentDirDbId', parentDirDbId.toString());
    FD.append('ConnId', sessionStorage.SignalRConnId);
    this.assetService.uploadfile(FD);
  }

}
