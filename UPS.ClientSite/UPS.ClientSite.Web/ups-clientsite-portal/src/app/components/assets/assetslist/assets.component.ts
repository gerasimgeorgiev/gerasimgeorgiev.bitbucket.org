import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { NewFolderComponent } from '../popups/newfolder.component';
import { AssetsService } from '../../../services/assets.service';
import { AssetDto } from '../../../classes/assets/asset';
import { saveAs } from 'file-saver/FileSaver';
import { environment } from '../../../../environments/environment';
import { ModalMessageComponent } from '../popups/modalmessage/modalmessage.component';

declare var myExtObject: any;

@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.css']
})
export class AssetsComponent implements OnInit {
  public uploadProgress: number;
  file: File;
  isRootLevel = true;
  currentFolder: AssetDto;
  parentFolder: AssetDto;
  assetsList: AssetDto[];
  currentFolderName: string;

  constructor(private assetsService: AssetsService, public dialog: MatDialog) { }

  ngOnInit() {
    this.assetsService.getReloadAssetsEmitter().subscribe(reload => {
      this.getFolderAssets(this.currentFolder);
    });

    this.getAssetsRootLevel();
  }

  createNewFolder(): void {
    this.unmarkOtherAssetsNotEditMode();

    const newFolderData = new AssetDto();

    newFolderData.IsFolder = true;

    if (this.currentFolder.IsRootFolder) {
      newFolderData.ParentFolderId = 0;
      newFolderData.RootFolderId = this.currentFolder.DbAssetId;
    } else {
      newFolderData.ParentFolderId = this.currentFolder.DbAssetId;
      newFolderData.RootFolderId = this.currentFolder.RootFolderId;
    }

    newFolderData.IsEditMode = true;
    this.assetsList.push(newFolderData);
    // this.openAddEditFolderPopup(newFolderData);
  }

  editFolder(folderToEdit: AssetDto): void {
    this.unmarkOtherAssetsNotEditMode();

    folderToEdit.IsEditMode = true;
    // this.openAddEditFolderPopup(folderToEdit);
  }

  unmarkOtherAssetsNotEditMode(): void {
    for (const asset of this.assetsList) {
      asset.IsEditMode = false;
    }
  }

  deleteAssetClicked(event, assetToDelete: AssetDto): void {
    const target = event.target || event.srcElement || event.currentTarget;
    myExtObject.openFileUploadPopup(target, JSON.stringify(assetToDelete));
  }

  openAddEditFolderPopup(folderData: AssetDto): void {
    const dialogRef = this.dialog.open(NewFolderComponent, {
      width: '350px',
      data: folderData
    });
  }

  openModalMessagePopup(message: string): void {
    const dialogRef = this.dialog.open(ModalMessageComponent, {
      width: '350px',
      data: message
    });
  }

  assetClicked(clickedAsset: AssetDto): void {
    if (clickedAsset.IsFolder) {
      this.getFolderAssets(clickedAsset);
    } else {
      window.open(clickedAsset.AssetUri);
    }
  }

  downloadAsset(clickedAsset: AssetDto): void {
    if (!clickedAsset.IsFolder) {
      window.open(clickedAsset.AssetUri);
    } else {
      this.assetsService.sendAssembleZipFileRequest(clickedAsset).subscribe(result => {
         this.openModalMessagePopup('The selected folder is prepared for download. Once ready the download will start.');
      });
    }
  }

  goToParent(parentFolder: AssetDto): void {
    // this.getFolderAssets(parentFolder);
  }

  getAssetsRootLevel(): void {
    this.assetsService.getRootFolders().subscribe(result => this.assetsList = result);
  }

  getFolderAssets(clickedFolder: AssetDto): void {
    // this.parentFolder = Object.assign({}, this.currentFolder);

    this.currentFolder = clickedFolder;

    this.assetsService.getFolderAssets(clickedFolder.DbAssetId, clickedFolder.IsRootFolder)
      .subscribe(result => {
          this.assetsList = result;
          this.isRootLevel = false;
        });
  }

  openFileUploadPopup(event): void {
    const target = event.target || event.srcElement || event.currentTarget;
    myExtObject.openFileUploadPopup(target, JSON.stringify(this.currentFolder));
  }

  goToRootLevel(): void {
    this.assetsService.getRootFolders().subscribe(result => {
      this.assetsList = result;
      this.isRootLevel = true;
    });
  }

  updateAsset(editedAsset: AssetDto): void {
    const result = this.assetsService.addEditFolder(editedAsset).subscribe(res => {
      this.assetsService.emitReloadAssetsEvent();
    });
  }

}
