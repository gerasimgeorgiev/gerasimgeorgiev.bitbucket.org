import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-distribution',
  templateUrl: './distribution.component.html',
  styleUrls: ['./distribution.component.css']
})
export class DistributionComponent implements OnInit {
  isTableView: boolean;
  isGraphView: boolean;
  switchToViewText: string;

  constructor() { }

  ngOnInit() {
    this.isTableView = false;
    this.isGraphView = true;
    this.switchToViewText = 'Table';
  }

  switchView(): void {
    if (this.isTableView === true) {
      this.isTableView = false;
      this.isGraphView = true;
      this.switchToViewText = 'Table';
    } else if (this.isGraphView === true) {
      this.isTableView = true;
      this.isGraphView = false;
      this.switchToViewText = 'Graph';
    }
  }

}
