import { Component, OnInit } from '@angular/core';

declare var myExtObject: any;

@Component({
  selector: 'app-dctable',
  templateUrl: './dctable.component.html',
  styleUrls: ['./dctable.component.css']
})
export class DcTableComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    myExtObject.updateCtrls();
  }

}
