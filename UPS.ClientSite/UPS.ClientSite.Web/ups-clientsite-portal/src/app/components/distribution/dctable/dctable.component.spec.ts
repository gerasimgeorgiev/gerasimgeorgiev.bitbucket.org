import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DctableComponent } from './dctable.component';

describe('DctableComponent', () => {
  let component: DctableComponent;
  let fixture: ComponentFixture<DctableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DctableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DctableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
