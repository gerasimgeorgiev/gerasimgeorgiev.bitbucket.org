import { Component, OnInit } from '@angular/core';

declare var chartingLogic: any;
declare var myExtObject: any;

@Component({
  selector: 'app-dcgraph',
  templateUrl: './dcgraph.component.html',
  styleUrls: ['./dcgraph.component.css']
})
export class DcGraphComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    myExtObject.updateCtrls();
    chartingLogic.updateCharts();
  }

}
