import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DcgraphComponent } from './dcgraph.component';

describe('DcgraphComponent', () => {
  let component: DcgraphComponent;
  let fixture: ComponentFixture<DcgraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcgraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcgraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
