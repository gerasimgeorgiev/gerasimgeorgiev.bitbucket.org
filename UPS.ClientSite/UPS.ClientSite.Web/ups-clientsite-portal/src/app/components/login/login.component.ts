import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { LoginDto } from '../../classes/membership/loginDto';
import { AdminMembershipService } from '../../services/adminmembership.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginVM: LoginDto;

  constructor(private membershipService: AdminMembershipService, private router: Router) { }

  ngOnInit() {
    this.loginVM = new LoginDto();

    this.loginVM.Requester =  'test';
  }

  login(): void {
    // this.membershipService.login(this.loginVM).subscribe(result => {
    //   this.router.navigate(['home']);
    // });
  }
}
