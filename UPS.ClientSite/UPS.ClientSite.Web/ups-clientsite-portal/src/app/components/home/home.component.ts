import { Component, OnInit } from '@angular/core';
import { UserDto } from '../../classes/membership/userDto';
import { MembershipService } from '../../services/membership.service';
import { Router } from '@angular/router';

declare var myExtObject: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isUserAuthenticated = false;
  basicUserData: UserDto;

  constructor(private membershipService: MembershipService, private router: Router) { }

  ngOnInit() {
    this.basicUserData = new UserDto();

    this.isUserAuthenticated = this.membershipService.isUserAuthenticated();

    if (this.isUserAuthenticated === true) {
      this.membershipService.getBasicUserData().subscribe(result => {
        this.basicUserData = result;
      });
    }
  }

  isLinkActive(linkPath: string): string {
    if (window.location.pathname === linkPath) {
      return 'active';
    } else {
      return '';
    }
  }

  openCloseSidebarControl(): void {
    myExtObject.openCloseSidebarControl();
  }

  signOut(): void {
    sessionStorage.token = '';
    window.location.href = '/landing';
  }

}
