import { Component, OnInit } from '@angular/core';

declare var chartingLogic: any;

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomePageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    chartingLogic.updateCharts();
  }

}
