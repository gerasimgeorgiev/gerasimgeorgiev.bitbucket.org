import { Component, OnInit } from '@angular/core';
import { MyProfileDto, ChangePswDto } from '../../classes/membership/myProfileDto';
import { MembershipService } from '../../services/membership.service';

import { MatTable } from '@angular/material';
import { LocationDto } from '../../classes/shared/locationDto';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyProfileComponent implements OnInit {
  myProfileData: MyProfileDto;
  changePsw: ChangePswDto;

  constructor(private membershipService: MembershipService) { }

  ngOnInit() {
    this.myProfileData = new MyProfileDto();
    this.myProfileData.Location = new LocationDto();

    this.changePsw = new ChangePswDto();

    this.getMyProfileData();
  }

  updateProfileData(): void {
    this.membershipService.updateProfileData(this.myProfileData).subscribe(result => {
      if (result === true) {
        alert('Success');
      }
    });
  }

  getMyProfileData(): void {
    this.membershipService.getProfileData().subscribe(result => {
      this.myProfileData = result;
    });
  }

  changePassword(): void {
    this.membershipService.changePassword(this.changePsw).subscribe(result => {
      if (result === true) {
        this.changePsw.OldPassword = '';
        this.changePsw.NewPassword = '';
        alert('Success');
      }
    });
  }

}
