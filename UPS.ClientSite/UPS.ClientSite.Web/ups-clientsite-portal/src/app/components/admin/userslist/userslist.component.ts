import { Component, OnInit } from '@angular/core';

import { UserDto } from '../../../classes/membership/userDto';

import { AdminService } from '../../../services/admin.service';

import { MatDialog } from '@angular/material/dialog';
import { AddEditUserComponent } from '../addedituser/addedituser.component';

import { MatTable } from '@angular/material';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.css']
})
export class UsersListComponent implements OnInit {
  users: UserDto[];
  columnsToDisplay: string[];
  selectedUserId: string;
  selectedUser: UserDto;

  constructor(private adminService: AdminService, public dialog: MatDialog) {
    this.columnsToDisplay = [ 'firstName', 'lastName', 'email', 'userRole'];
  }

  selectRow(row): void {
    this.selectedUserId = row.MembershipId;
    this.selectedUser = row;
  }

  ngOnInit() {
    this.selectedUser = null;
    this.getUsers();

    this.adminService.getReloadUsersEmitter().subscribe(reload => {
      this.getUsers();
    });
  }

  deleteUser(): void {
    if (this.selectedUser !== null) {
      if (confirm('Are you sure?')) {
        const result = this.adminService.disableUser(this.selectedUser).subscribe(res => {
          this.adminService.emitReloadUsersEvent();
        });
      }
    }
  }

  editUser(): void {
    if (this.selectedUser !== null) {
    this.openAddEditUserPopup(this.selectedUser);
    }
  }

  createNewUserPopup(): void {
    this.openAddEditUserPopup(null);
  }

  openAddEditUserPopup(userData: UserDto): void {
    const dialogRef = this.dialog.open(AddEditUserComponent, {
      width: '350px',
      data: userData
    });
  }

  getUsers(): void {
    this.adminService.getUsers().subscribe(data => this.users = data);
  }
}
