import { Component, OnInit } from '@angular/core';

import { UsersListComponent } from './userslist/userslist.component';
import { AddEditUserComponent } from './addedituser/addedituser.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }
}
