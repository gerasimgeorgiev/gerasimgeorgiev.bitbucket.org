import { Component, OnInit, HostListener, Inject } from '@angular/core';

import { UserDto } from '../../../classes/membership/userDto';

import { AdminService } from '../../../services/admin.service';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ClientDto, ClientAccount } from '../../../classes/shared/customerDto';

@Component({
  selector: 'app-addedituser',
  templateUrl: './addedituser.component.html',
  styleUrls: ['./addedituser.component.css']
})

export class AddEditUserComponent implements OnInit {
  userData: UserDto;
  userRoles: string[];
  clients: ClientDto[];
  selectedClient: ClientDto;
  selectedClientAccount: ClientAccount;
  clientAccounts: ClientAccount[];
  isNewUser: boolean;
  editUser: boolean;
  addEditBtnText: string;

  constructor(private adminService: AdminService, private dialogRef: MatDialogRef<AddEditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public editUserData: UserDto) {
      if (editUserData !== null) {
        this.editUser = true;
        this.isNewUser = false;
        this.userData = editUserData;
        this.addEditBtnText = 'Edit';
      } else {
        this.editUser = false;
        this.isNewUser = true;
        this.addEditBtnText = 'Create';
      }
    }

  cancelModal(): void {
    this.dialogRef.close();
  }

  addEditUser(): void {
    this.userData.ClientAccountId = this.selectedClientAccount.Id;

    const result = this.adminService.addEditUser(this.userData).subscribe(res => {
      this.adminService.emitReloadUsersEvent();

      this.dialogRef.close();
    });
  }

  ngOnInit() {
    if (this.isNewUser) {
      this.userData = new UserDto();
    }
    this.adminService.getClients().subscribe(data => this.clients = data);
  }

  onClientSelect() {
    this.clientAccounts = this.selectedClient.Accounts;
  }
}
