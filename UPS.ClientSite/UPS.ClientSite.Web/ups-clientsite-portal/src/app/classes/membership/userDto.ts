export class UserDto {
    FirstName: string;
    LastName: string;
    Email: string;
    Password: string;
    UserRole: string;
    MembershipId: string;
    ClientAccountId: number;
}
