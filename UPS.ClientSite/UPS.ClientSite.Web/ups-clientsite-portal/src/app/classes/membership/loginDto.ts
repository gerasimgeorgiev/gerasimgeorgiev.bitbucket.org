export class LoginDto {
    Email: string;
    Password: string;
    Requester: string;
}

export class AuthenticatedUserDto {
    SamlToken: string;
    UserRoles: string[];
}
