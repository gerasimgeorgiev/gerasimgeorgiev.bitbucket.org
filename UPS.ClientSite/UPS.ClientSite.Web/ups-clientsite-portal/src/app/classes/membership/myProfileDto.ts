import { UserDto } from './userDto';
import { LocationDto } from '../shared/locationDto';

export class MyProfileDto extends UserDto {
    Location: LocationDto;
    PrimaryDistCenterId: number;
    PrimaryFSLId: number;
}

export class ChangePswDto {
    OldPassword: string;
    NewPassword: string;
}
