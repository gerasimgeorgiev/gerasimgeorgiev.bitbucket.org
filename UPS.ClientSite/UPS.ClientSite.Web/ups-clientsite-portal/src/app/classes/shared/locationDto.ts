export class LocationDto {
    Address: string;
    Lat: number;
    Lon: number;
    LocationType: string;
}
