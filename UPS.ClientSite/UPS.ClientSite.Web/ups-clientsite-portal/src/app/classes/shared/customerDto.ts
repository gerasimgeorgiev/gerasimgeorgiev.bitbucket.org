
export class ClientDto {
    Id: number;
    Name: string;
    Accounts: ClientAccount[];
}

export class ClientAccount {
    Id: number;
    Name: string;
}
