export class AssetDto {
    DbAssetId: number;
    Name: string;
    AssetUri: string;
    IsFolder: boolean;
    IsRootFolder: boolean;
    LastModifiedDate: Date;
    RootFolderId: number;
    ParentFolderId: number;
    FileNameWithoutExtension: string;
    FileExtension: string;
    IsEditMode: boolean;
}
