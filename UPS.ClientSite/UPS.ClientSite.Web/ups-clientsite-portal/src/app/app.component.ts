import { Component } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Router } from '@angular/router';
import { AdalService } from 'adal-angular4';
import { HubConnection } from '@aspnet/signalr';

import { MembershipService } from './services/membership.service';
import { UserDto } from './classes/membership/userDto';
import { environment } from '../environments/environment';
import * as signalR from '@aspnet/signalr';
import { AssetsService } from './services/assets.service';

declare var myExtObject: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isUserAuthenticated = false;
  private hubConnection: HubConnection;

  constructor (private membershipService: MembershipService, public router: Router, private adalSvc: AdalService,
  private assetService: AssetsService) {
    this.adalSvc.init(environment.adalConfig);
  }

  ngOnInit() {
    this.adalSvc.handleWindowCallback();

    this.isUserAuthenticated = this.membershipService.isUserAuthenticated();

    if (this.isUserAuthenticated === false) {
      if (this.adalSvc.userInfo.authenticated) {
        this.isUserAuthenticated = true;
      }
    } else {
      const logger = new signalR.ConsoleLogger(signalR.LogLevel.Information);
      const chatHub = new signalR.HttpConnection(environment.upsapiSignalRUrl + 'clientsite?token=' + sessionStorage.token,
      { transport: signalR.TransportType.LongPolling, logger: logger });

      this.hubConnection = new signalR.HubConnection(chatHub);

      this.hubConnection
        .start()
        .then(() => {
          console.log('Connection started!');
          this.hubConnection.invoke('Join');
          }
        )
        .catch(err =>
          console.log('Error while establishing connection. Error: ' + JSON.stringify(err))
        );

        this.hubConnection.on('FilesUploaded', () => {
          this.assetService.emitReloadAssetsEvent();
         });

         this.hubConnection.on('FolderZipped', (targetZipFileName: string) => {
           const downloadZipFileUrl = environment.upsapiUrl + 'Assets/DownloadZipFile';
          myExtObject.downloadZipFile(targetZipFileName, downloadZipFileUrl);
         });

         this.hubConnection.on('FolderDownloaded', (targetZipFileName: string) => {
          this.assetService.emitZipFileAvailableForDownload();
         });

         this.hubConnection.on('ConnId', (connId) => {
          sessionStorage.SignalRConnId = connId;
         });
    }
  }

  initHub(): void {

  }

  logoff(): void {
    this.isUserAuthenticated = false;

    this.membershipService.logoff().subscribe(result => {
      sessionStorage.token = null;

      this.router.navigate(['login']);
    });
  }

  goToMyProfile(): void {
    this.router.navigate(['myProfile']);
  }
}
