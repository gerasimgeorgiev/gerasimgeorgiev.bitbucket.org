import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AdalService, AdalGuard } from 'adal-angular4';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material';

import { AppComponent } from './app.component';

import { MembershipService } from './services/membership.service';
import { AdminMembershipService } from './services/adminmembership.service';
import { AppSettingsService } from './services/appsettings.service';
import { AdauthenticationService } from './services/adauthentication.service';
import { AdminService } from './services/admin.service';
import { AssetsService } from './services/assets.service';

import { LoginComponent } from './components/login/login.component';

import { AppRoutingModule } from './modules/app-routing.module';

import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './components/admin/admin.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { AddEditUserComponent } from './components/admin/addedituser/addedituser.component';
import { UsersListComponent } from './components/admin/userslist/userslist.component';
import { MyProfileComponent } from './components/myprofile/myprofile.component';
import { AssetsComponent } from './components//assets/assetslist/assets.component';
import { NewFolderComponent } from './components/assets/popups/newfolder.component';
import { LandingComponent } from './components/home/landing/landing.component';
import { AssetpopupsComponent } from './components/assets/popups/assetpopups.component';
import { ModalMessageComponent } from './components/assets/popups/modalmessage/modalmessage.component';
import { ReportsComponent } from './components/reports/reports.component';
import { DistributionComponent } from './components/distribution/distribution.component';
import { DcTableComponent } from './components/distribution/dctable/dctable.component';
import { DcGraphComponent } from './components/distribution/dcgraph/dcgraph.component';
import { HomePageComponent } from './components/home/homepage/homepage.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AdminComponent,
    UnauthorizedComponent,
    AddEditUserComponent,
    UsersListComponent,
    MyProfileComponent,
    AssetsComponent,
    NewFolderComponent,
    LandingComponent,
    AssetpopupsComponent,
    ModalMessageComponent,
    ReportsComponent,
    DistributionComponent,
    DcTableComponent,
    DcGraphComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatTableModule
  ],
  entryComponents: [
    AddEditUserComponent,
    NewFolderComponent,
    ModalMessageComponent
  ],
  providers: [AdalService, AdalGuard,
    MembershipService, AdminMembershipService, AppSettingsService, AdauthenticationService, AdminService, AssetsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
