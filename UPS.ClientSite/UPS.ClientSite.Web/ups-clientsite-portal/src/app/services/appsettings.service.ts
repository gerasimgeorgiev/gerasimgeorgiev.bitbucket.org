import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AppSettings } from '../classes/shared/appsetting';

@Injectable()
export class AppSettingsService {
  constructor(private http: HttpClient) {
  }

  getSettings(): Observable<AppSettings> {
    return this.http.get('assets/appsettings.json')
      .map(result => this.extractData(result))
      .catch(error => this.handleErrors(error));
  }

  private extractData(res: any) {
    const appSettings = new AppSettings();

    appSettings.upsapiUrl = res.upsapiUrl;
    appSettings.upsadminapiUrl = res.upsadminapiUrl;

    return appSettings || {};
  }

  private handleErrors(error: any): Observable<any> {
    console.error('An error occurred', error);
    return Observable.throw(error.message || error);
  }
}
