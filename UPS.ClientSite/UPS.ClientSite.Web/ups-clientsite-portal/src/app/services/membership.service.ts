import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AuthenticatedUserDto, LoginDto } from '../classes/membership/loginDto';

import { AppSettingsService } from './appsettings.service';
import { AppSettings } from '../classes/shared/appsetting';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { UserDto } from '../classes/membership/userDto';
import { MyProfileDto, ChangePswDto } from '../classes/membership/myProfileDto';
import { LocationDto } from '../classes/shared/locationDto';
import { environment } from '../../environments/environment';

@Injectable()
export class MembershipService implements CanActivate {
  authenticated = false;
  upsapiUrl: string;
  azureadb2cloginurl: string;

  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private httpClient: HttpClient, private route: ActivatedRoute) {
      this.upsapiUrl = environment.upsapiUrl;
      this.azureadb2cloginurl = environment.AzureAd.B2CLoginUrl;

      if (location.hash.length > 0) {
      const authCode = this.getAccessTokenFromHash();

      if (authCode && authCode.length > 0) {
        sessionStorage.token = authCode;
      }
    }
  }

  getAccessTokenFromHash(): string {
    // tslint:disable-next-line:no-shadowed-variable
    const result = location.hash.substr(1).split('&').reduce(function (result, item) {
      const parts = item.split('=');
      result[parts[0]] = parts[1];
      return result;
  }, {});

  const t = new Date();
  t.setSeconds(+t.getSeconds() + +result['expires_in']);
  sessionStorage.tokenExpires = JSON.stringify(t);

  return result['access_token'];
}

  isUserAuthenticated(): boolean {
    if (sessionStorage.token != null && sessionStorage.token !== 'null' && sessionStorage.token.length > 0) {
      if (sessionStorage.tokenExpires && sessionStorage.tokenExpires.length > 0 &&
        new Date(JSON.parse(sessionStorage.tokenExpires)) < new Date()) {
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  canActivate(): boolean {
    this.authenticated = this.isUserAuthenticated();

    if (this.authenticated === true) {
        return this.authenticated;
    } else {
      window.location.href = this.azureadb2cloginurl;
      // this.router.navigate(['login']);
      return this.authenticated;
    }
  }

  login(credentials: LoginDto): Observable<AuthenticatedUserDto> {
    const loginUserUrl = environment.upsapiUrl + 'Account/Login';

    return this.httpClient.post<any>(loginUserUrl, credentials, this.getCommonHttpOptions())
    .map(result => this.extractData(result))
    .catch(error => this.handleError(error));
  }

  logoff(): Observable<boolean> {
    const logoffUserUrl = environment.upsapiUrl + 'Account/Logout';

    return this.httpClient.get<any>(logoffUserUrl, this.getCommonHttpOptions())
    .map(result => this.extractBooleanResult(result))
    .catch(error => this.handleError(error));
  }

  getBasicUserData(): Observable<UserDto> {
    const basicUserDataUrl = this.upsapiUrl + 'Account/UserData';

    return this.httpClient.get<any>(basicUserDataUrl, this.getCommonHttpOptions())
    .map(result => this.extractBasicUserData(result))
    .catch(error => this.handleError(error));
  }

  getProfileData(): Observable<MyProfileDto> {
    const basicUserDataUrl = this.upsapiUrl + 'Account/ProfileData';

    return this.httpClient.get<any>(basicUserDataUrl, this.getCommonHttpOptions())
    .map(result => this.extractProfileData(result))
    .catch(error => this.handleError(error));
  }

  updateProfileData(profileData: MyProfileDto): Observable<boolean> {
    const updateProfileUrl = environment.upsapiUrl + 'Account/UpdateProfile';

    return this.httpClient.post<any>(updateProfileUrl, profileData, this.getCommonHttpOptions())
    .map(result => this.extractBooleanResult(result))
    .catch(error => this.handleError(error));
  }

  changePassword(changePsw: ChangePswDto): Observable<boolean> {
    const changePasswordUrl = environment.upsapiUrl + 'Account/ChangePassword';

    return this.httpClient.post<any>(changePasswordUrl, changePsw, this.getCommonHttpOptions())
    .map(result => this.extractBooleanResult(result))
    .catch(error => this.handleError(error));
  }

  getCommonHttpOptions(): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + sessionStorage.token
      })
    };

    return httpOptions;
  }

  extractBooleanResult(result: any): Observable<boolean> {
    if (result.success === true) {
      return Observable.of(true);
    } else {
      return Observable.of(false);
    }
  }

  private extractProfileData(res: any) {
    const profileDataDto = new MyProfileDto();
    profileDataDto.Location = new LocationDto();

    if (res.success === true) {
      profileDataDto.FirstName = res.data.firstName;
      profileDataDto.LastName = res.data.lastName;
      profileDataDto.Location.Address = res.data.location.address;

      for (const customer of res.data.customers) {
        // const newCustomer = new CustomerDto();

        // newCustomer.Name = customer.name;
        // newCustomer.Number = customer.number;

        // profileDataDto.Customers.push(newCustomer);
      }
    }

    return profileDataDto || {};
  }

  private extractBasicUserData(res: any) {
    const userDto = new UserDto();

    if (res.success === true) {
      userDto.FirstName = res.data.firstName;
      userDto.LastName = res.data.lastName;
      userDto.Email = res.data.email;
    }

    return userDto || {};
  }

  private extractData(res: any) {
    const userDto = new AuthenticatedUserDto();

    if (res.success === true) {
      userDto.SamlToken = res.data;
      userDto.UserRoles = res.roles;

      sessionStorage.token = userDto.SamlToken;
    }

    return userDto || {};
  }

  handleError(error: any) {
    const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
