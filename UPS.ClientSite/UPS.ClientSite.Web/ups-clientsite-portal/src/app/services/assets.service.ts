import { Injectable, EventEmitter } from '@angular/core';
import { HttpRequest, HttpClient, HttpEventType, HttpResponse, HttpHeaders } from '@angular/common/http';
import { ResponseContentType, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from '../classes/shared/appsetting';
import { AppSettingsService } from './appsettings.service';
import { environment } from '../../environments/environment';

import {AssetDto } from '../classes/assets/asset';
import { saveAs } from 'file-saver/FileSaver';

declare var myExtObject: any;

@Injectable()
export class AssetsService {
  uploadProgress: number;
  reloadAssets: EventEmitter<any> = new EventEmitter();
  assetUploadStatus: EventEmitter<any> = new EventEmitter();
  zipFileAvailableForDownload:  EventEmitter<any> = new EventEmitter();

  constructor(private httpClient: HttpClient) {
  }

  emitReloadAssetsEvent() {
    this.reloadAssets.emit();
  }

  getReloadAssetsEmitter() {
    return this.reloadAssets;
  }

  emitZipFileAvailableForDownload() {
    this.zipFileAvailableForDownload.emit();
  }

  getZipFileAvailableForDownloadEmitter() {
    return this.zipFileAvailableForDownload;
  }

  getAssetUploadStatusEmitter() {
    return this.assetUploadStatus;
  }

  sendAssembleZipFileRequest(folderToDownload: AssetDto): Observable<boolean> {
    const downloadFolderUrl = environment.upsapiUrl + 'Assets/DownloadFolder';

    const postData = {
      DbFolderId: folderToDownload.DbAssetId,
      IsRootFolder: folderToDownload.IsRootFolder,
      SignalRClientId: sessionStorage.SignalRConnId
    };

    return this.httpClient.post(downloadFolderUrl, postData, this.getCommonHttpOptions())
    .map(result => this.extractBooleanResult(result))
    .catch(error => this.handleError(error));
  }

  downloadFile(blob: any, type: string, filename: string): string {
    const url = window.URL.createObjectURL(blob); // <-- work with blob directly

    // create hidden dom element (so it works in all browsers)
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);

    // create file, attach to hidden element and open hidden element
    a.href = url;
    a.download = filename;
    a.click();
    return url;
  }

  addEditFolder(folderData: AssetDto): Observable<boolean> {
    let addEditFolderUrl = environment.upsapiUrl + 'Assets/NewFolder';

    if (folderData.DbAssetId > 0) {
      addEditFolderUrl = environment.upsapiUrl + 'Assets/EditFolder';
    }

    return this.httpClient.post<any>(addEditFolderUrl, folderData, this.getCommonHttpOptions())
    .map(result => this.extractBooleanResult(result))
    .catch(error => this.handleError(error));
  }

  deleteAsset(assetToDelete: AssetDto): Observable<boolean> {
    const deleteAssetUrl = environment.upsapiUrl + 'Assets/Delete';

    return this.httpClient.post<any>(deleteAssetUrl, assetToDelete, this.getCommonHttpOptions())
    .map(result => this.extractBooleanResult(result))
    .catch(error => this.handleError(error));
  }

  getRootFolders(): Observable<AssetDto[]> {
    const getRootFoldersUrl = environment.upsapiUrl + 'Assets/Root';

    return this.httpClient.get<any>(getRootFoldersUrl, this.getCommonHttpOptions())
    .map(result => this.extractAssetsList(result))
    .catch(error => this.handleError(error));
  }

  getFolderAssets(folderId: number, isRootFolder: boolean): Observable<AssetDto[]> {
    const getFolderAssetsUrl = environment.upsapiUrl + 'Assets/Folder';

    const data = {
      DbFolderId: folderId,
      IsRootFolder: isRootFolder
    };

    return this.httpClient.post<any>(getFolderAssetsUrl, data, this.getCommonHttpOptions())
    .map(result => this.extractAssetsList(result))
    .catch(error => this.handleError(error));
  }

  private extractZipFilePath(result: any) {
    let zipFilePath = '';
    if (result.success === true) {
      zipFilePath = result.data;
    }

    return zipFilePath || {};
  }

  private extractAssetsList(result: any) {
    const assetsList = new Array<AssetDto>();

    if (result.success === true) {
      for (const asset of result.data) {
          const assetDto = new AssetDto();

          assetDto.DbAssetId = asset.dbAssetId;
          assetDto.Name = asset.name;
          assetDto.AssetUri = asset.assetUri;
          assetDto.IsFolder = asset.isFolder;
          assetDto.IsRootFolder = asset.isRootFolder;
          assetDto.LastModifiedDate = asset.lastModifiedDate;
          assetDto.RootFolderId = asset.rootFolderId;
          assetDto.ParentFolderId = asset.parentFolderId;
          assetDto.FileNameWithoutExtension = asset.fileNameWithoutExtension;
          assetDto.FileExtension = asset.fileExtension;

          assetsList.push(assetDto);
      }
    }

    return assetsList || {};
  }

  uploadfile(file): void {
    const uploadFileUrl = environment.upsapiUrl + 'Assets/Upload';

    const req = new HttpRequest('POST', uploadFileUrl, file, {
      reportProgress: true,
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + sessionStorage.token
      })
    });

    this.httpClient.request(req).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
          this.uploadProgress = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
          this.assetUploadStatus.emit();
      }
  });
  }

  getCommonHttpOptions(): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + sessionStorage.token
      })
    };

    return httpOptions;
  }

  handleError(error: any) {
    const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  extractBooleanResult(result: any): Observable<boolean> {
    if (result.success === true) {
      return Observable.of(true);
    } else {
      return Observable.of(false);
    }
  }
}
