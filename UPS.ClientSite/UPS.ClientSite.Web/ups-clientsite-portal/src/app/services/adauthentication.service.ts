import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AppSettingsService } from './appsettings.service';
import { AppSettings } from '../classes/shared/appsetting';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()
export class AdauthenticationService implements CanActivate {
  authenticated = false;
  appSettings: AppSettings;
  appSettingsService: AppSettingsService;

  constructor(private router: Router, private httpClient: HttpClient, private settingsService: AppSettingsService) {
    this.appSettingsService = settingsService;
  }

  canActivate(): Observable<boolean>|boolean {
   // return this.appSettingsService.getSettings()
    //  .map(settings => {
    //    this.appSettings = settings;


    //  });

      const authenticateUserUrl = 'http://localhost:33213/api/'+ 'Users/IsValidUser';
      //this.appSettings.upsadminapiUrl + 'User/IsValidUser';

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json; charset=utf-8', 'Accept': 'application/json'
        }),
        withCredentials: true
      };

      return this.httpClient.get(authenticateUserUrl, httpOptions)
        .map(result => this.extractData(result))
        .catch(error => this.handleError(error));
  }

  private extractData(res: any): boolean {
    let isUserAuthenticated = false;

    if (res.success === true) {
      isUserAuthenticated = true;
    } else {
      this.router.navigate(['unauthorized']);
      isUserAuthenticated = false;
    }

    return isUserAuthenticated;
  }

  handleError(error: any) {
    this.router.navigate(['unauthorized']);

    const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
