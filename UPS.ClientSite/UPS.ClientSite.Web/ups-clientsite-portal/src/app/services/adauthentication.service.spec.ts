import { TestBed, inject } from '@angular/core/testing';

import { AdauthenticationService } from './adauthentication.service';

describe('AdauthenticationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdauthenticationService]
    });
  });

  it('should be created', inject([AdauthenticationService], (service: AdauthenticationService) => {
    expect(service).toBeTruthy();
  }));
});
