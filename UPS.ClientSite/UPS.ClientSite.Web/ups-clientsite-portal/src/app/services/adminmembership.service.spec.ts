import { TestBed, inject } from '@angular/core/testing';

import { AdminmembershipService } from './adminmembership.service';

describe('AdminmembershipService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminmembershipService]
    });
  });

  it('should be created', inject([AdminmembershipService], (service: AdminmembershipService) => {
    expect(service).toBeTruthy();
  }));
});
