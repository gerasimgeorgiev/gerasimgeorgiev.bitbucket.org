import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AdalService } from 'adal-angular4';

import { LoginDto, AuthenticatedUserDto } from '../classes/membership/loginDto';
import { UserDto } from '../classes/membership/userDto';
import { environment } from '../../environments/environment';

@Injectable()
export class AdminMembershipService implements CanActivate {
  authenticated = false;
  upsapiUrl: string;

  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private httpClient: HttpClient, private route: ActivatedRoute, private adalSvc: AdalService) {
      if (location.hash.length > 0) {
      const authCode = this.getAccessTokenFromHash();

      if (authCode && authCode.length > 0) {
        sessionStorage.adminToken = authCode;
      }
    }
  }

  getAccessTokenFromHash(): string {
    // tslint:disable-next-line:no-shadowed-variable
    const result = location.hash.substr(1).split('&').reduce(function (result, item) {
      const parts = item.split('=');
      result[parts[0]] = parts[1];
      return result;
  }, {});

  return result['id_token'];
}

  canActivate(): boolean {
    if (this.adalSvc.userInfo.authenticated) {
      return true;
    } else {
      this.adalSvc.login();
      return false;
    }
  }

  logoff(): Observable<boolean> {
    const logoffUserUrl = environment.upsapiUrl + 'Account/Logout';

    return this.httpClient.get<any>(logoffUserUrl, this.getCommonHttpOptions())
    .map(result => this.extractBooleanResult(result))
    .catch(error => this.handleError(error));
  }

  getCommonHttpOptions(): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };

    return httpOptions;
  }

  extractBooleanResult(result: any): Observable<boolean> {
    if (result.success === true) {
      return Observable.of(true);
    } else {
      return Observable.of(false);
    }
  }

  private extractData(res: any) {
      sessionStorage.adminToken = res.access_token;

      this.router.navigate(['admin']);
  }

  handleError(error: any) {
    const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
