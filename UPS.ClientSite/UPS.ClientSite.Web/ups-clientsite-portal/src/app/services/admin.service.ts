import { Injectable, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AdalService } from 'adal-angular4';

import { UserDto } from '../classes/membership/userDto';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { ClientDto, ClientAccount } from '../classes/shared/customerDto';

@Injectable()
export class AdminService {
  adminApiUrl: string;

 reloadUsers: EventEmitter<any> = new EventEmitter();

  constructor(private router: Router, private httpClient: HttpClient) {
    this.adminApiUrl = environment.upsadminapiUrl;
  }

  emitReloadUsersEvent() {
    this.reloadUsers.emit();
  }

  getReloadUsersEmitter() {
    return this.reloadUsers;
  }

  disableUser(userDto: UserDto): Observable<boolean> {
    const disableUserUrl = this.adminApiUrl + 'Users/DisableUser';

    return this.httpClient.post<any>(disableUserUrl, userDto, this.getCommonHttpOptions())
    .map(result => this.extractBooleanResult(result))
    .catch(error => this.handleError(error));
  }

  addEditUser(newUserData: UserDto): Observable<boolean> {
    let addEditUserUrl = this.adminApiUrl + 'Users/CreateNewUser';

    if (newUserData.MembershipId && newUserData.MembershipId.length > 0) {
      // Edit User functionality
      addEditUserUrl = this.adminApiUrl + 'Users/UpdateUser';
    }

    return this.httpClient.post<any>(addEditUserUrl, newUserData, this.getCommonHttpOptions())
    .map(result => this.extractBooleanResult(result))
    .catch(error => this.handleError(error));
  }

  extractBooleanResult(result: any): Observable<boolean> {
    if (result.success === true) {
      return Observable.of(true);
    } else {
      return Observable.of(false);
    }
  }

  getUserRoles(): Observable<string[]> {
    const getUserRolesUrl = this.adminApiUrl + 'Users/GetRoles';

    return this.httpClient.get<any>(getUserRolesUrl, this.getCommonHttpOptions())
    .map(result => this.extractRoles(result))
    .catch(error => this.handleError(error));
  }

  getClients(): Observable<ClientDto[]> {
    const getClientsUrl = this.adminApiUrl + 'Client/Clients';

    return this.httpClient.get<any>(getClientsUrl, this.getCommonHttpOptions())
    .map(result => this.extractClients(result))
    .catch(error => this.handleError(error));
  }

  getUsers(): Observable<UserDto[]> {
    const getUsersUrl = this.adminApiUrl + 'Users/GetUsers';

    return this.httpClient.get<any>(getUsersUrl, this.getCommonHttpOptions())
    .map(result => this.extractUsers(result))
    .catch(error => this.handleError(error));
  }

  private extractRoles(result: any) {
    if (result.success === true) {
      return result.data;
    } else {
      return Observable.of(false);
    }
  }

  private extractUsers(result: any) {
    const users = new Array<UserDto>();

    if (result.success === true) {
      for (const user of result.data.value) {
          const userDto = new UserDto();

          if (user.signInNames && user.signInNames.length > 0) {
          userDto.Email = user.signInNames[0].value;

          const names = user.displayName.split(' ');

          userDto.FirstName = names[0];
          userDto.LastName = names[1];

          users.push(userDto);
          }
      }
    }

    return users || {};
  }

  private extractClients(result: any) {
    const clients = new Array<ClientDto>();

    if (result.success === true) {
      for (const client of result.data) {
          const clientDto = new ClientDto();

          clientDto.Accounts = new Array<ClientAccount>();

          clientDto.Id = client.id;
          clientDto.Name = client.name;

          for (const clientAccount of client.clientAccounts) {
            const clientAccountDto = new ClientAccount();

            clientAccountDto.Id = clientAccount.id;
            clientAccountDto.Name = clientAccount.name;

            clientDto.Accounts.push(clientAccountDto);
          }

          clients.push(clientDto);
      }
    }

    return clients || {};
  }

  getCommonHttpOptions(): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + sessionStorage.adminToken
      })
    };

    return httpOptions;
  }

  handleError(error: any) {
    const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
