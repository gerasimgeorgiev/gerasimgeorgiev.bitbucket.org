import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdalGuard } from 'adal-angular4';

import { MembershipService } from '../services/membership.service';
import { AdminMembershipService } from '../services/adminmembership.service';
import { AdauthenticationService } from '../services/adauthentication.service';

import { LoginComponent } from '../components/login/login.component';
import { HomeComponent } from '../components/home/home.component';
import { MyProfileComponent } from '../components/myprofile/myprofile.component';

import { AdminComponent } from '../components/admin/admin.component';
import { UnauthorizedComponent } from '../components/unauthorized/unauthorized.component';
import { AssetsComponent } from '../components/assets/assetslist/assets.component';
import { LandingComponent } from '../components/home/landing/landing.component';
import { ReportsComponent } from '../components/reports/reports.component';
import { DistributionComponent } from '../components/distribution/distribution.component';
import { HomePageComponent } from '../components/home/homepage/homepage.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: 'home', component: HomePageComponent, canActivate: [MembershipService] },
  { path: 'landing', component: LandingComponent },
  { path: 'portalassets', component: AssetsComponent, canActivate: [MembershipService] },
  { path: 'myProfile', component: MyProfileComponent, canActivate: [MembershipService] },
  { path: 'admin', component: AdminComponent, canActivate: [AdminMembershipService] },
  { path: 'reports', component: ReportsComponent, canActivate: [MembershipService] },
  { path: 'distribution', component: DistributionComponent, canActivate: [MembershipService] },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule ],
  providers: [ MembershipService]
})
export class AppRoutingModule { }
