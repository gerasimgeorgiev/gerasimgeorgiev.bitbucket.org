export const environment = {
  production: true,
  adalConfig: {
    tenant: 'stephenpaulups.onmicrosoft.com',
    clientId: '63607ccc-4b5f-4646-a0c9-c64929487c90',
    postLogoutRedirectUri: 'http://localhost:4200/logout',
    endpoints: {
      'https://adaltestapi.azurewebsites.net': 'https://adaltestapi.azurewebsites.net',
    },
  },
  upsapiSignalRUrl: 'https://clientsiteapi-dev.azurewebsites.net/',
  upsapiUrl: 'https://clientsiteapi-dev.azurewebsites.net/api/',
  upsadminapiUrl: 'https://clientsiteapiadmin-dev.azurewebsites.net/api/',
  AzureAd: {
    Instance: 'https://login.microsoftonline.com/',
    Domain: 'stephenpaulups.onmicrosoft.com',
    TenantId: '430ace76-878d-42b2-9dbe-fe11bb566d4f',

    ClientId: '63607ccc-4b5f-4646-a0c9-c64929487c90',
    CallbackPath: '/admin',
    ClientSecret: 'EWkbsF3KGvy+dbzIWKjN/EEu4YutZBYup6kIBKvfqP8=',

    ClientSiteAdminAPIResourceId: '45f42283-0014-45bf-ad0c-c2d33d88f6b7',
    ClientSiteAdminAPIAddress: 'http://localhost:33213/',
    // tslint:disable-next-line:max-line-length
    B2CLoginUrl: 'https://login.microsoftonline.com/ClientSiteB2cTenant.onmicrosoft.com/oauth2/v2.0/authorize?p=B2C_1_dummyPolicy&client_id=15970efb-3a40-4a6d-a2f8-1d5e6eb6b26b&nonce=defaultNonce&redirect_uri=https%3A%2F%2Fclientsite-dev.azurewebsites.net%2Fhome&scope=https%3A%2F%2FClientSiteB2cTenant.onmicrosoft.com%2Fclientsiteapi%2Fuser_impersonation&response_type=token&prompt=login'
  }
};
