var myExtObject = (function() {

	return {
		downloadZipFile: function(fileName, downloadZipFileUrl) {
			var ajax = new XMLHttpRequest();
			ajax.open("POST", downloadZipFileUrl, true);
			ajax.setRequestHeader('Authorization','Bearer ' + sessionStorage.token);
            ajax.responseType = "blob";
            ajax.onreadystatechange = function () {
                if (this.readyState == 4) {
                    var blob = new Blob([this.response], { type: "application/octet-stream" });
                    saveAs(blob, fileName);
                }
			};
			const FD = new FormData();
			FD.append('FileName', fileName);
			FD.append('SignalRClientId', sessionStorage.SignalRConnId);

            ajax.send(FD);
		},

		sendAssembleZipFileRequest: function(downloadFolderUrl, stringifiedFilter) {
			var ajax = new XMLHttpRequest();
			ajax.open("POST", downloadFolderUrl, true);
			ajax.setRequestHeader('Authorization','Bearer ' + sessionStorage.token);
            ajax.responseType = "blob";
            ajax.onreadystatechange = function () {
                if (this.readyState == 4) {
                    var blob = new Blob([this.response], { type: "application/octet-stream" });
                    var fileName = "test123.zip";
                    saveAs(blob, fileName);
                }
			};
			var filter = JSON.parse(stringifiedFilter);
			const FD = new FormData();
			FD.append('IsRootFolder', filter.IsRootFolder.toString());
			FD.append('DbFolderId', filter.DbFolderId.toString());
			FD.append('ConnId', sessionStorage.SignalRConnId);
            ajax.send(FD);
		},

		openFileUploadPopup: function(target, stringifiedObject) {		
			var self = $(target),
				dataModal = self.attr('data-modal-handle'),
				modalToOpen = $('.modal-wrapper[data-modal-action="' + dataModal + '"]');

				if(!dataModal) {
					this.openFileUploadPopup($(target).parent(), stringifiedObject);
					return;
				}

				if(dataModal=="modal-file-upload")
				{
					$("#divProgress").width(0);
					$("#hdnFieldCurrentFolder").val(stringifiedObject);
				}
				else if(dataModal=="modal-delete-file")
				{
					var assetToDelete = JSON.parse(stringifiedObject);
					$("#pConfirmationText").text("Are you sure you want to delete " + assetToDelete.Name + " from folder?");
					$("#hdnFieldAssetToDelete").val(stringifiedObject);
				}
		
			if (modalToOpen.hasClass('modal-in-content')) {
				var anchorTarget = $('.modal-in-content-anchor[data-modal-handle="' + dataModal + '"]'),
					anchorWidth = anchorTarget.outerWidth(),
					anchorHeight = anchorTarget.outerHeight(),
					anchorOffset = anchorTarget.offset(),
					anchorOffsetTop = anchorOffset.top,
					anchorOffsetLeft = anchorOffset.left,
					windowScrollTop = $(window).scrollTop(),
					windowWidth = $(window).width();
		
				var finalPositionTop = ((anchorHeight + anchorOffsetTop) - windowScrollTop + 2),
					finalPositionRight = (windowWidth - (anchorWidth + anchorOffsetLeft));
		
				var modalToOpenInner = modalToOpen.find('.modal-wrapper-inner');
		
				modalToOpenInner.css({
					'top': finalPositionTop,
					'right': finalPositionRight,
					'left': 'auto',
				});
			};
		
			if (modalToOpen.length) {
				modalToOpen.addClass('opened');
			};
			$('html').addClass('modal-active');
		},

		closePopup: function(target) {
			// var self = $(target);
			
			//self.closest('.modal-wrapper').removeClass('opened');
			$('.modal-wrapper').removeClass('opened');
			$('html').removeClass('modal-active');
		},

		openCloseSidebarControl: function() {
			$('.sidebar').toggleClass('opened');
			$('body').toggleClass('sidebar-opened');
		},

		getStringifiedCurrentFolder: function() {
			var stringifiedFolder = $("#hdnFieldCurrentFolder").val();
			return stringifiedFolder;
		}, 

		getStringifiedAssetToDelete: function() {
			var stringifiedAsset = $("#hdnFieldAssetToDelete").val();
			return stringifiedAsset;
		},

		updateCtrls: function() {
			// pin button
			$('.btn-add-to-favorites').on('click', function(e) {
				e.preventDefault();

				var self = $(this),
					pinTarget = self.closest('.pin-target');

				if (self.hasClass('pinned')) {
					// unpin functionality here
					pinTarget.remove();
				} else {
					// pin functionality here
				};
			});

			// custom tooltip showing general code
			$('.js-custom-tooltip-show').on('mouseenter', function() {
				var self = $(this),
					tooltip = $('.custom-tooltip'),
					selfWidth = self.outerWidth(),
					selfHeight = self.outerHeight(),
					selfOffset = self.offset(),
					selfOffsetTop = selfOffset.top,
					selfOffsetLeft = selfOffset.left,
					windowScrollTop = $(window).scrollTop(),
					windowWidth = $(window).width();

				var finalPositionTop = (selfOffsetTop - 15),
				finalPositionLeft = ((selfWidth + selfOffsetLeft) - (selfWidth/2));

				tooltip.css({
					'top': finalPositionTop,
					'left': finalPositionLeft,
				});

				tooltip.addClass('shown');
			});

			$('.custom-tooltip-close').on('click', function() {
				var self = $(this),
					tooltip = self.closest('.custom-tooltip');
				
					tooltip.removeClass('shown');
			});

			// custom dropdown - selectir plugin used

			$('.js-selectric-dd').selectric({
				arrowButtonMarkup: '<span class="button icon"><img src="/assets/img/icons/icon-arrow-down-brown.png" alt=""></span>',
			});

			// datepicker functionality
			$('[data-toggle="datepicker"]').datepicker({
				date: new Date(2018, 3, 8),
			});
		}
	}
  
  })(myExtObject||{})

$(document).ready(function() {

// file upload from input directly
// $('input[type="file"].direct-upload').on('change', function() {

// 	var self = $(this),
// 		fileInfo = self.val().split('\\').pop(),
// 		fileName = fileInfo.split('.')[0],
// 		fileType = fileInfo.split('.')[1];

// 	if (self.hasClass('upload-from-modal')) {
// 		self.closest('.modal-wrapper').find('.js-modal-close').trigger('click');
// 	};

// 	var tableRowNew = $('.table-standart .new-row').clone();

// 	tableRowNew.prependTo('.table-standart tbody');
// 	tableRowNew.find('.asset-name').text(fileName);
// 	tableRowNew.find('.asset-kind').text(fileType);

// 	var d = new Date();
// 	tableRowNew.find('.asset-upload-time').text(d);
// 	tableRowNew.find('.asset-upload-time').attr('datetime', d);

// 	tableRowNew.removeClass('new-row');
// });

// rename folder
$('.asset-link-action[data-asset-link-action="rename"]').on('click', function(e) {
	e.preventDefault();

	var self = $(this),
		parent = self.closest('tr'),
		assetName = parent.find('.asset-name'),
		assetNameText = assetName.text();

	if (!assetName.hasClass('renaming')) {
		assetName.html('<input type="text" class="input-rename-asset" value="' + assetNameText + '">');
		assetName.addClass('renaming');
	} else {
		return false;
	}
});

// submitting new name on enter press
$('.asset-name').on('keyup', '.input-rename-asset', function(e) {

	var self = $(this),
		selfValue = self.val();

	if (e.keyCode == 13) {
		self.parent('.asset-name').text(selfValue);
	};
});

// grid menu handle
$('.js-grid-menu-handle').on('click', function(e) {
	e.preventDefault();

	var self = $(this),
		gridMenuWrapper = self.closest('.grid-menu-wrapper');

	gridMenuWrapper.toggleClass('grid-opened');
});

	// grid menu handle
	$('.js-top-bar-inner-menu-handle').on('click', function(e) {
		e.preventDefault();

		var self = $(this),
			gridMenuWrapper = self.closest('.top-bar-inner-menu-wrapper');


		if (gridMenuWrapper.hasClass('inner-menu-opened')) {
			gridMenuWrapper.removeClass('inner-menu-opened');
		} else {
			$('.top-bar').find('.inner-menu-opened').removeClass('inner-menu-opened');
			gridMenuWrapper.addClass('inner-menu-opened');
		};
	});

});