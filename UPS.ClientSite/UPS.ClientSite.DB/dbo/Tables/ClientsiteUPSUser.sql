﻿CREATE TABLE [dbo].[ClientsiteUPSUser] (
    [ClientsiteUPSUserID] INT IDENTITY (1001, 1) NOT NULL,
    [ActiveFlag]          BIT CONSTRAINT [DEF_ClientsiteUPSUser_ActiveFlag] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ClientsiteUPSUser] PRIMARY KEY CLUSTERED ([ClientsiteUPSUserID] ASC)
);

