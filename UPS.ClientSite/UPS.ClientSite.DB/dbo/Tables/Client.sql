﻿CREATE TABLE [dbo].[Client] (
    [ClientID]           INT           IDENTITY (1001, 1) NOT NULL,
    [MasterClientNumber] VARCHAR (10)  NOT NULL,
    [ClientName]         VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED ([ClientID] ASC)
);

