﻿CREATE TABLE [dbo].[SubFolder] (
    [SubFolderID]           INT          IDENTITY (1001, 1) NOT NULL,
    [ParentSubFolderID]     INT          NULL,
    [ClientAccountFolderID] INT          NOT NULL,
    [SubFolderName]         VARCHAR (60) NOT NULL,
    [DeletedFlag]           BIT          CONSTRAINT [DEF_SubFolder_DeletedFlag] DEFAULT ((0)) NOT NULL,
    [LastModifiedDate]      DATETIME     NOT NULL,
    CONSTRAINT [PK_SubFolder] PRIMARY KEY CLUSTERED ([SubFolderID] ASC),
    CONSTRAINT [FK_SubFolder_ClientAccountFolder] FOREIGN KEY ([ClientAccountFolderID]) REFERENCES [dbo].[ClientAccountFolder] ([ClientAccountFolderID]),
    CONSTRAINT [FK_SubFolder_SubFolder] FOREIGN KEY ([ParentSubFolderID]) REFERENCES [dbo].[SubFolder] ([SubFolderID])
);



