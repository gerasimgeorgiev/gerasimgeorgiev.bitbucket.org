﻿CREATE TABLE [dbo].[File] (
    [FileID]                INT            IDENTITY (1001, 1) NOT NULL,
    [FileName]              VARCHAR (255)  NOT NULL,
    [ClientAccountFolderID] INT            NOT NULL,
    [SubFolderID]           INT            NULL,
    [DeletedFlag]           BIT            CONSTRAINT [DEF_File_DeletedFlag] DEFAULT ((0)) NOT NULL,
    [FileReferenceNumber]   NVARCHAR (MAX) NOT NULL,
    [LastModifiedDate]      DATETIME       NOT NULL,
    CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED ([FileID] ASC),
    CONSTRAINT [FK_File_ClientAccountFolder] FOREIGN KEY ([ClientAccountFolderID]) REFERENCES [dbo].[ClientAccountFolder] ([ClientAccountFolderID]),
    CONSTRAINT [FK_File_SubFolder] FOREIGN KEY ([SubFolderID]) REFERENCES [dbo].[SubFolder] ([SubFolderID])
);



