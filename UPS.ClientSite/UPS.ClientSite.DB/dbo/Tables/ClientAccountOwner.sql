﻿CREATE TABLE [dbo].[ClientAccountOwner] (
    [ClientAccountOwnerID]     INT NOT NULL,
    [ClientAccountID]          INT NOT NULL,
    [ClientsiteUPSUser]        INT NOT NULL,
    [ClientAccountOwnerRoleID] INT NOT NULL,
    [ActiveFlagID]             BIT NOT NULL,
    CONSTRAINT [PK_ClientAccountOwner] PRIMARY KEY CLUSTERED ([ClientAccountOwnerID] ASC)
);

