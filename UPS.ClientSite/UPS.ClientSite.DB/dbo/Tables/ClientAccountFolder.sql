﻿CREATE TABLE [dbo].[ClientAccountFolder] (
    [ClientAccountFolderID] INT          IDENTITY (1001, 1) NOT NULL,
    [RootFolderName]        VARCHAR (60) NOT NULL,
    [ClientAccountID]       INT          NOT NULL,
    [LastModifiedDate]      DATETIME     NOT NULL,
    CONSTRAINT [PK_ClientAccountFolder] PRIMARY KEY CLUSTERED ([ClientAccountFolderID] ASC),
    CONSTRAINT [FK_ClientAccountFolder_ClientAccount] FOREIGN KEY ([ClientAccountID]) REFERENCES [dbo].[ClientAccount] ([ClientAccountID])
);



