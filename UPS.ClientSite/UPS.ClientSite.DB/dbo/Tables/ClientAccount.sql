﻿CREATE TABLE [dbo].[ClientAccount] (
    [ClientAccountID]     INT          IDENTITY (1001, 1) NOT NULL,
    [ClientAccountNumber] VARCHAR (40) NULL,
    [ClientAccountName]   VARCHAR (40) NULL,
    [ClientID]            INT          NULL,
    CONSTRAINT [PK_ClientAccount] PRIMARY KEY CLUSTERED ([ClientAccountID] ASC),
    CONSTRAINT [FK_ClientAccount_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
);

