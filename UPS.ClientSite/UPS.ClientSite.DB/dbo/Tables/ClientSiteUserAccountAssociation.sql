﻿CREATE TABLE [dbo].[ClientSiteUserAccountAssociation] (
    [ClientSiteUserAccountAssociationID] INT IDENTITY (1001, 1) NOT NULL,
    [ClientSiteUserID]                   INT NULL,
    [ClientAccountID]                    INT NULL,
    [ActiveFlag]                         BIT CONSTRAINT [DEF_ClientSiteUserAccountAssociation_ActiveFlag] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ClientSiteUserAccountAssociation] PRIMARY KEY CLUSTERED ([ClientSiteUserAccountAssociationID] ASC),
    CONSTRAINT [FK_ClientSiteUserAccountAssociation_ClientAccount] FOREIGN KEY ([ClientAccountID]) REFERENCES [dbo].[ClientAccount] ([ClientAccountID]),
    CONSTRAINT [FK_ClientSiteUserAccountAssociation_ClientSiteUser] FOREIGN KEY ([ClientSiteUserID]) REFERENCES [dbo].[ClientSiteUser] ([ClientSiteUserID])
);

