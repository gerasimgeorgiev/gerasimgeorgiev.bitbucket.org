﻿CREATE TABLE [dbo].[ClientAccountOwnerRole] (
    [ClientAccountOwnerRoleID] INT           IDENTITY (1001, 1) NOT NULL,
    [RoleName]                 VARCHAR (60)  NOT NULL,
    [RoleDescription]          VARCHAR (255) NOT NULL,
    [ActiveFlag]               BIT           CONSTRAINT [DEF_ClientAccountOwnerRole_ActiveFlag] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ClientAccountOwnerRole] PRIMARY KEY CLUSTERED ([ClientAccountOwnerRoleID] ASC)
);

