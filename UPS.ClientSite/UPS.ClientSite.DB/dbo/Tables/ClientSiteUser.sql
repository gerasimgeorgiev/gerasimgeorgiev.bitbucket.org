﻿CREATE TABLE [dbo].[ClientSiteUser] (
    [ClientSiteUserID] INT            IDENTITY (1001, 1) NOT NULL,
    [Email]            NVARCHAR (MAX) NOT NULL,
    [FirstName]        NVARCHAR (MAX) NULL,
    [LastName]         NVARCHAR (MAX) NULL,
    [ActiveFlag]       BIT            CONSTRAINT [DEF_ClientSiteUser_ActiveFlag] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ClientSiteUser] PRIMARY KEY CLUSTERED ([ClientSiteUserID] ASC)
);



