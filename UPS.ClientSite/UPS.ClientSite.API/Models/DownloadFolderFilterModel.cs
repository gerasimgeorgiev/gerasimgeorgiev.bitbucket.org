﻿namespace UPS.ClientSite.API.Models
{
    public class DownloadFolderFilterModel
    {
        public int DbFolderId { get; set; }
        public bool IsRootFolder { get; set; }
        public string SignalRClientId { get; set; }
    }
}
