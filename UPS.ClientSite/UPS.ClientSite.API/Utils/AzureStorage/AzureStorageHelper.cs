﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace UPS.ClientSite.API.Utils.AzureStorage
{
    public class AzureStorageHelper
    {
        public static string ConnectionString;
        public static string ContainerName;

        private const int MaxBlockSize = (1024 * 1024) * 4; // 1MB chunk size

        public static async Task<string> UploadFilesIntoAzureBlob(string[] files, string blobName)
        {
            try
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(ConnectionString);

                CloudBlobClient blobclient = cloudStorageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobclient.GetContainerReference(ContainerName);

                CloudBlockBlob blob = container.GetBlockBlobReference(blobName);

                HashSet<string> blocklist = new HashSet<string>();

                int blockId = 0;
                foreach (string filePath in files)
                {
                    byte[] fileContent = File.ReadAllBytes(filePath);

                    foreach (FileBlock block in GetFileBlocks(fileContent))
                    {
                        block.Id = Convert.ToBase64String(BitConverter.GetBytes(blockId));

                        await blob.PutBlockAsync(block.Id, new MemoryStream(block.Content, true), null);

                        blocklist.Add(block.Id);

                        blockId++;
                    }
                }

                await blob.PutBlockListAsync(blocklist);

                return blob.Uri.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static async Task<List<string>> DownloadBlobs(List<string> blobUris, string destinationSubFolderPath)
        {
            List<string> downloadedFilePaths = new List<string>();

            if (!Directory.Exists(destinationSubFolderPath))
                Directory.CreateDirectory(destinationSubFolderPath);

            foreach (string blobUri in blobUris)
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(ConnectionString);

                CloudBlobClient blobclient = cloudStorageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobclient.GetContainerReference(ContainerName);

                CloudBlockBlob blob = container.GetBlockBlobReference(new CloudBlockBlob(new Uri(blobUri)).Name);

                string destinationFile = $"{destinationSubFolderPath}/{Path.GetFileName(blobUri)}";

                await blob.DownloadToFileAsync(destinationFile, FileMode.Create);

                downloadedFilePaths.Add(destinationFile);
            }

            return downloadedFilePaths;
        }

        private static IEnumerable<FileBlock> GetFileBlocks(byte[] fileContent)
        {
            HashSet<FileBlock> hashSet = new HashSet<FileBlock>();
            if (fileContent.Length == 0)
                return new HashSet<FileBlock>();

            
            int ix = 0;

            int currentBlockSize = MaxBlockSize;

            while (currentBlockSize == MaxBlockSize)
            {
                if ((ix + currentBlockSize) > fileContent.Length)
                    currentBlockSize = fileContent.Length - ix;
                byte[] chunk = new byte[currentBlockSize];
                Array.Copy(fileContent, ix, chunk, 0, currentBlockSize);

                hashSet.Add(
                    new FileBlock()
                    {
                        Content = chunk
                    });

                ix += currentBlockSize;
            }

            return hashSet;
        }
    }

    internal class FileBlock
    {
        public string Id
        {
            get;
            set;
        }

        public byte[] Content
        {
            get;
            set;
        }
    }
}