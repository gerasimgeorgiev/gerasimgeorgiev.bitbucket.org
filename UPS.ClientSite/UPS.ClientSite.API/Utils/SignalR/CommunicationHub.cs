﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace UPS.ClientSite.API.Utils.SignalR
{
    [Authorize]
    public class CommunicationHub : Hub
    {
        private ConcurrentDictionary<string, UserInfo> _onlineUser { get; set; } = new ConcurrentDictionary<string, UserInfo>();

        public override Task OnConnectedAsync()
        {
            string currentUsername = Context.User.FindFirst("emails").Value;
            AddUpdate(currentUsername, Context.ConnectionId);

            return base.OnConnectedAsync();
        }

        private async void ConnIdBackToClient(string clientId)
        {
            await Clients.Client(clientId).SendAsync("ConnId", clientId);
        }

        public async Task Join()
        {
            string currentUsername = Context.User.FindFirst("emails").Value;
            var connectedUser = GetUserInfo(currentUsername);
            ConnIdBackToClient(Context.ConnectionId);
        }

        private void AddUpdate(string name, string connectionId)
        {
            var userAlreadyExists = _onlineUser.ContainsKey(name);

            var userInfo = new UserInfo
            {
                UserName = name,
                ConnectionId = connectionId
            };

            _onlineUser.AddOrUpdate(name, userInfo, (key, value) => userInfo);
        }

        private UserInfo GetUserInfo(string username)
        {
            UserInfo user;
            _onlineUser.TryGetValue(username, out user);
            return user;
        }
    }
}
