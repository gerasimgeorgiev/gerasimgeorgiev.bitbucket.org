﻿namespace UPS.ClientSite.API.Utils.SignalR
{
    public class UserInfo
    {
        public string ConnectionId { get; set; }
        public string UserName { get; set; }
    }
}
