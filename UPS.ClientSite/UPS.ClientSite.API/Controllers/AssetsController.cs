﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UPS.ClientSite.API.Models;
using UPS.ClientSite.API.Utils.AzureStorage;
using UPS.ClientSite.API.Utils.FileManager;
using UPS.ClientSite.API.Utils.SignalR;
using UPS.ClientSite.BLL.Assets;
using UPS.ClientSite.Core.Assets;

namespace UPS.ClientSite.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Assets")]
    public class AssetsController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private IHubContext<CommunicationHub> HubContext { get; set; }

        public AssetsController(IHostingEnvironment hostingEnvironment, IHubContext<CommunicationHub> hubcontext)
        {
            _hostingEnvironment = hostingEnvironment;
            HubContext = hubcontext;
        }

        [HttpPost("UploadMultipartUsingReader")]
        [Route("~/api/Assets/Upload")]
        public async Task<IActionResult> UploadNewAsset()
        {
            try
            {
                foreach (var file in Request.Form.Files)
                {
                    var memoryStream = new MemoryStream();
                    file.CopyTo(memoryStream);

                    string path = $"{_hostingEnvironment.ContentRootPath}/uploads/{file.FileName}";
                    var fileStream = System.IO.File.Create(path, (int)file.Length, FileOptions.None);
                    fileStream.Write(memoryStream.ToArray(), 0, (int)file.Length);

                    fileStream.Flush();
                    fileStream.Dispose();

                    memoryStream.Flush();
                    memoryStream.Dispose();

                    bool allPartsRetrieved = FileHelper.AreAllFilePartsDownloaded(path);

                    if (allPartsRetrieved)
                    {
                        int rootDirDbId = int.Parse(Request.Form["rootDirDbId"]);
                        int parentDirDbId = int.Parse(Request.Form["parentDirDbId"]);
                        string signalRClientId = Request.Form["ConnId"];

                        UploadFilePartsIntoAzure(path, file.FileName, rootDirDbId, parentDirDbId, signalRClientId);
                    }
                }

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpGet]
        [Route("~/api/Assets/Root")]
        public async Task<IActionResult> GetRootFolders()
        {
            try
            {
                string currentUsername = User.FindFirst("emails").Value;

                var assetFilter = new AssetsFilterDto()
                {
                    IsRootLevel = true,
                    IsRootFolder = false,
                    CurrentUserEmail = currentUsername,
                    DbFolderId = 0
                };

                var assetsList = AssetActions.GetFolderContent(assetFilter);

                return Json(new { Success = true, Data = assetsList });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpPost]
        [Route("~/api/Assets/Folder")]
        public async Task<IActionResult> GetFolderAssets([FromBody] AssetsFilterDto filter)
        {
            try
            {
                string currentUsername = User.FindFirst("emails").Value;

                filter.CurrentUserEmail = currentUsername;
                filter.IsRootLevel = false;

                var assetsList = AssetActions.GetFolderContent(filter);

                return Json(new { Success = true, Data = assetsList });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpPost]
        [Route("~/api/Assets/DownloadFolder")]
        public async Task<IActionResult> DownloadFolder([FromBody] DownloadFolderFilterModel model)
        {
            try
            {
                string currentUsername = User.FindFirst("emails").Value;

                AssetsFilterDto filter = new AssetsFilterDto();

                filter.DbFolderId = model.DbFolderId;
                filter.IsRootFolder = model.IsRootFolder;

                filter.CurrentUserEmail = currentUsername;
                filter.IsRootLevel = false;

                DownloadFolderActions(filter, model.SignalRClientId);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        private async Task DownloadFolderActions(AssetsFilterDto filter, string signalRClientId)
        {
            List<string> fileNames = new List<string>();

            fileNames = AssetActions.GetAllFileUrisWithinFolder(filter, fileNames);

            fileNames = fileNames.Distinct().ToList();

            string destinationFileName = string.Format("{0}_{1}", filter.CurrentUserEmail, DateTime.Now.Ticks.ToString());

            string downloadsFolderPath = $"{_hostingEnvironment.ContentRootPath}/downloads/";

            string destinationSubFolderPath = $"{downloadsFolderPath}/{destinationFileName}";

            var downloadedFilePaths = await AzureStorageHelper.DownloadBlobs(fileNames, destinationSubFolderPath);

            string destinationZipFilePath = $"{downloadsFolderPath}/{destinationFileName}.zip";

            // Create an archive with all downloaded files
            FileHelper.CreateArchiveFromFiles(destinationSubFolderPath, destinationZipFilePath);

            string fileName = Path.GetFileName(destinationZipFilePath);

            HubContext.Clients.Client(signalRClientId).SendAsync("FolderZipped", fileName);
        }

        [HttpPost]
        [Route("~/api/Assets/DownloadZipFile")]
        public async Task<IActionResult> DownloadZippedFolder()
        {
            try
            {
                string fileName = Request.Form["FileName"];
                string signalRClientId = Request.Form["SignalRClientId"];

                string downloadsFolderPath = $"{_hostingEnvironment.ContentRootPath}/downloads/";
                string destinationZipFilePath = $"{downloadsFolderPath}/{fileName}";

                return await DownloadFile(destinationZipFilePath, signalRClientId);
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        private async Task<FileStreamResult> DownloadFile(string filePath, string signalRClientId)
        {
            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            var mimeType = "application/octet-stream";
            string fileName = Path.GetFileName(filePath);

            var contentDisposition = new ContentDispositionHeaderValue("attachment");
            contentDisposition.SetHttpFileName(fileName);
            Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();

            HubContext.Clients.Client(signalRClientId).SendAsync("FolderDownloaded", fileName);

            return new FileStreamResult(memory, mimeType);
        }

        private async Task UploadFilePartsIntoAzure(string filePath, string fileName, int rootFolderId, int parentFolderId, string signalRClientId)
        {
            string currentUsername = User.FindFirst("emails").Value;

            string[] filesList = FileHelper.GetFilePartsList(filePath);

            string blobName = fileName.Substring(0, fileName.IndexOf(".part_"));

            string newBloblUri = await AzureStorageHelper.UploadFilesIntoAzureBlob(filesList, blobName);

            AssetActions.AddNewAsset(new AssetDto()
            {
                AssetUri = newBloblUri,
                Name = blobName,
                IsFolder = false,
                RootFolderId = rootFolderId,
                ParentFolderId = parentFolderId
            });

            FileHelper.DeleteAllSeparateFileParts(filePath);

            HubContext.Clients.Client(signalRClientId).SendAsync("FilesUploaded");
        }

        [HttpPost]
        [Route("~/api/Assets/NewFolder")]
        public async Task<IActionResult> CreateNewFolder([FromBody] AssetDto folderData)
        {
            try
            {
                AssetActions.AddNewAsset(folderData);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpPost]
        [Route("~/api/Assets/EditFolder")]
        public async Task<IActionResult> EditFolder([FromBody] AssetDto folderData)
        {
            try
            {
                AssetActions.EditFolder(folderData);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpPost]
        [Route("~/api/Assets/Delete")]
        public async Task<IActionResult> DeleteAsset([FromBody] AssetDto assetData)
        {
            try
            {
                AssetActions.DeleteAsset(assetData);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }
    }
}