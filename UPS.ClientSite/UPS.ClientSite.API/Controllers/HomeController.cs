﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace UPS.ClientSite.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Home")]
    [Authorize]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/api/Home/ProtectedResource")]
        public IActionResult ProtectedResource()
        {
            return View();
        }
    }
}