﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UPS.ClientSite.BLL;
using UPS.ClientSite.Core;

namespace UPS.ClientSite.API.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Account")]
    public class UserAccountController : Controller
    {
        [HttpGet]
        [Route("~/api/Account/Logout")]
        public async Task<IActionResult> Logout()
        {
            try
            {
                // await signInManager.SignOutAsync();

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpGet]
        [Route("~/api/Account/UserData")]
        public async Task<IActionResult> GetBasicUserData()
        {
            try
            {
                string currentUsername = User.FindFirst("emails").Value;

                var basicUserData = MembershipActions.GetBasicUserData(currentUsername);

                return Json(new { Success = true, Data = basicUserData });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [HttpPost]
        [Route("~/api/Account/DisableUser")]
        public async Task<IActionResult> DisableUser([FromBody] UserDTO userData)
        {
            try
            {
                return Json(new { Success = true });

                //var user = await userManager.FindByNameAsync(userData.Email);

                //if (user != null)
                //{
                //    user.IsEnabled = false;
                //    user.DisabledDateTime = DateTime.Now;

                //    var result = await userManager.UpdateAsync(user);

                //    if (result.Succeeded)
                //    {
                //        return Json(new { Success = true });
                //    }
                //    else
                //        return Json(new { Success = false, Data = "An error occurred while updating the user" });
                //}
                //else
                //    return Json(new { Success = false, Data = "User not found" });

            }
            catch (Exception e)
            {
                return Json(new { Success = false, Data = e.Message });
            }
        }

        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}
