﻿namespace UPS.ClientSite.Core.Shared
{
    public class LocationDTO
    {
        public string Address { get; set; }
        public decimal? Lat { get; set; }
        public decimal? Lon { get; set; }
        public string LocationType { get; set; }
    }
}
