﻿using System.Collections.Generic;

namespace UPS.ClientSite.Core.Client
{
    public class ClientDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public List<ClientAccountDto> ClientAccounts { get; set; }
    }

    public class ClientAccountDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
    }
}
