﻿using System;

namespace UPS.ClientSite.Core.Assets
{
    public class AssetDto
    {
        public int DbAssetId { get; set; }
        public string Name { get; set; }
        public string AssetUri { get; set; }
        public int? ParentFolderId { get; set; }
        public int RootFolderId { get; set; }
        public bool IsRootFolder { get; set; }
        public bool IsFolder { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string FileNameWithoutExtension { get; set; }
        public string FileExtension { get; set; }
    }
}
