﻿namespace UPS.ClientSite.Core.Assets
{
    public class AssetsFilterDto
    {
        public int DbFolderId { get; set; }
        public bool IsRootLevel { get; set; }
        public bool IsRootFolder { get; set; }
        public string CurrentUserEmail { get; set; }
    }
}
