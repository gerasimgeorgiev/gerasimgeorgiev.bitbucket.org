﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace UPS.ClientSite.Core.UserAccount
{
    public class LoginDTO
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string Requester { get; set; }
        public string ReturnUrl { get; set; }
    }
}
