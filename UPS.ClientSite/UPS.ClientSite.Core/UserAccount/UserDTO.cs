﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UPS.ClientSite.Core
{
    public class UserDTO
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string UserRole { get; set; }

        [Required]
        public int ClientAccountId { get; set; }

        public string MembershipId { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
