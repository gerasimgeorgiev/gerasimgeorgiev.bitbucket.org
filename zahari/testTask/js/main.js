$(document).ready(function() {

	$('html').removeClass('no-js').addClass('js');

	$('.btn.back').click(function() {
		alert('Back button clicked');
	});

	$('.btn-cta').click(function() {
		alert('Call to action button clicked');
	});

	$('#menu-toggle').click(function(e) {
		e.preventDefault();
		$(this).siblings('ul').stop().slideToggle(300);
	});

	$('.btn.toggle').click(function(e) {
		e.preventDefault();
		toggleDropdown(this);
	});

	$('.btn.toggle-inner').click(function(e) {
		e.preventDefault();
		toggleDropdown(this);
	});
});

function toggleDropdown(el) {
	var handle = el;
		icon = $(handle).children('img');
		contentToSlide = $(handle).parent().next();
		target = $(this.event.target);

	if(target.hasClass('edit-handle')) {
		alert('ready to edit');
		return false;
	};

	if(contentToSlide.hasClass('opened')) {
		contentToSlide.removeClass('opened').addClass('closed').stop().slideUp(400);
		if(icon.hasClass('icon-expand-inner')) {
			icon.fadeOut(200, function() {
				icon.attr('src', 'img/icon-plus.svg').removeClass('edit-handle').fadeIn(200);
			});
		} else {
			icon.removeClass('animate rotate180').addClass('animate rotate0');
		};
		
	} else {
		contentToSlide.removeClass('closed').addClass('opened').stop().slideDown(400);
		if(icon.hasClass('icon-expand-inner')) {
			icon.fadeOut(200, function() {
				icon.attr('src', 'img/icon-edit.svg').addClass('edit-handle').fadeIn(200);
			});
		} else {
			icon.removeClass('animate rotate0').addClass('animate rotate180');
		};
	};
};
