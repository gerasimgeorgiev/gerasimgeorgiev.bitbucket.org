(function($){ 
	"use strict";

	// $('.nav').responsiveNav();

	//toggling sidebar on button click
	$('.js-sidebar-control').on('click', function(e) {
		e.preventDefault();
		$('.sidebar').toggleClass('opened');
	});

	//modal control
	// open modal
    $('.js-modal-open').click(function () {
        var self = $(this);
        var btnName = self.attr('name');

        $('.' + btnName).fadeIn(200);
    });

	// close modal
    $('.js-modal-close').click(function () {
        var self = $(this);
        self.parents('.modal-bg-wrapper').fadeOut(200);
    });

    //closing modal on outside click
    $('.modal-bg-wrapper').on('click', function(e){
		var target = e.target;
	    if (!$(target).is('.modal-inner-wrapper *') && !$(target).is('.modal-inner-wrapper')) {
	    	$(this).fadeOut(200);
	    };
	});

    //controlling modal for finalizing data
	$('.js-change-load-state').on('click', function() {
		var parent = $(this).closest('.js-fdata'),
			startPart = parent.find('.start-part'),
			loadPart = parent.find('.load-part'),
			endPart = parent.find('.end-part');

		startPart.fadeOut(300, function() {
			loadPart.fadeIn(300);
		});

		setTimeout(function() {
			loadPart.fadeOut(300, function() {
				endPart.fadeIn(300);
			});
		},2000);
	});

	// hiding broken avatar image for initials can show properly
	$(".js-avatar-img").on('error', function () { 
	    $(this).hide();
	});

	//previewing avatar image in user profile
	$('input#avatar-upload').change(function(){
		inputClicked = $(this);
	    readURL(this);
	});

	$('#avatar-delete').on('click', function(e) {
		e.preventDefault();
		$('#user-avatar-profile').attr('src', '');
	});

	//custom dropdown implementation
	$('.custom-dropdown select').select2({
		minimumResultsForSearch: Infinity,
	});

	//limiting characrets of input with class js-limit and data-limit attribute
	$('.js-limit').keydown( function(e){
		var limit = $(this).attr('data-limit');
	    if ($(this).val().length >= limit) { 
	        $(this).val($(this).val().substr(0, limit));
	    }
	});

	$('.js-limit').keyup( function(e){
		var limit = $(this).attr('data-limit');
	    if ($(this).val().length >= limit) { 
	        $(this).val($(this).val().substr(0, limit));
	    }
	});

	//btn for clearing value of previous input
	$('.js-clear-value').on('click', function(e) {
		e.preventDefault();

		var prev = $(this).prev();

		if (prev.hasClass('awesomplete')) {
			prev.find('input').val('');
		} else {
			prev.val('');
		};
	});

	if ($('.awesomplete-as-dropdown').length) {
		var awesompleteDropdownInput = $('.awesomplete-as-dropdown');

		var awesomplete = new Awesomplete('.awesomplete-as-dropdown',
			{
				autoFirst: true,
				minChars: 0,
				maxItems: 500
		});

		awesompleteDropdownInput.on('click', function() {
			awesompleteDropdownInput.val('');
			awesomplete.evaluate();
			awesomplete.open();
		});

		awesompleteDropdownInput.on('awesomplete-selectcomplete', function(event) {
			alert(awesompleteDropdownInput.val());
			awesompleteDropdownInput.val(event.originalEvent.text.label);
		});
	};

	$('.js-datepicker').datepicker({});
	$('.js-datepicker-init').on('click', function() {
		$(this).prev().datepicker('show');
	});

	//admin btn finalize data control
	$('.js-toggle-fdata').on('change', function() {

		var btn = $('.js-toggle-fdata-btn');

		if (btn.hasClass('inactive')) {
			btn.removeClass('inactive').attr('onclick', 'return true;');
		} else {
			btn.addClass('inactive').attr('onclick', 'return false;');
		};
	});

	// Control Panel: edit notes
    if ($('.control-panel-not').length) {
        // edit button
        $('.js-cp-notes-edit').click(function () {
            // set current value
            var parent = $(this).parent();
            var currentText = $(parent).siblings('p.js-fade').text();
            $(parent).siblings('div.js-fade').children('textarea').html(currentText);

            // show textarea
            $(parent).siblings('p.js-fade').stop().fadeOut(200).css('display', 'block');
            $(parent).siblings('p.js-fade').promise().done(function () {
                $(parent).siblings('div.js-fade').stop().fadeIn(200).css('display', 'block');
            });
        });

        $('.js-cp-notes-done').click(function () {
            var parent = $(this).parent();

            // change on save
            if ($(this).hasClass('save-btn')) {
                var textareaText = $(this).siblings('textarea').val();
                $(parent).siblings('p.js-fade').html(textareaText);
            };

            // hide textarea
            $(parent).stop().fadeOut(200).css('display', 'block');
            $(parent).promise().done(function () {
                $(parent).siblings('p.js-fade').stop().fadeIn(200).css('display', 'block');
            });
        });
    };

    // open-close landing page notifications
    $('.js-landing-notes').click(function () {
        $('.landing-note-wrapper').toggleClass('opened');
    });

	//table sorting
	$('.table-sortable').tablesorter();

	//js mobile detection
	var isMobile = window.matchMedia("only screen and (max-width: 768px)");

	//disabling select2 plugin on mobile to enable default mobile dropdown
	if (isMobile.matches) {
		$('.custom-dropdown select').select2('destroy');
	};

	//custom scrollbar plugin for recruters list in dashboard (control panel)
	$('.my-recruiters-list').mCustomScrollbar({
		theme: 'blue'
	});

	$('.top-recruiters-list').mCustomScrollbar({
		theme: 'blue'
	});

	//custom scrollbar for branches list in admin manage approvers
	$('.manage-approvers .branches-wrap').mCustomScrollbar({
		theme: 'blue'
	});

	//admin manage approvers btn control for branches
	$('.manage-approvers .btn-branches').on('click', function(e) {
		var self = $(this);

		if(self.hasClass('inactive')) {
			e.preventDefault();
		};
	});

	var activeBranchesChecked = 0;
	$('.manage-approvers .active-branches input').on('change', function() {
		var self = $(this),
			btn = $('.btn-branches');

		if (self.is(':checked')) {
			activeBranchesChecked++;
		} else {
			activeBranchesChecked--;
		};

		if (activeBranchesChecked > 0) {
			btn.removeClass('inactive').find('.num').text(activeBranchesChecked);
		} else {
			btn.addClass('inactive').find('.num').text('');
		};
	});

	//admin manage approvers btn control for branches
	$('.manage-approvers .btn-inactive-branches').on('click', function(e) {
		var self = $(this);

		if(self.hasClass('inactive')) {
			e.preventDefault();
		};
	});

	var inactiveBranchesChecked = 0;
	$('.manage-approvers .inactive-branches input').on('change', function() {
		var self = $(this),
			btn = $('.btn-inactive-branches');

		if (self.is(':checked')) {
			inactiveBranchesChecked++;
		} else {
			inactiveBranchesChecked--;
		};

		if (inactiveBranchesChecked > 0) {
			btn.removeClass('inactive').find('.num').text(inactiveBranchesChecked);
		} else {
			btn.addClass('inactive').find('.num').text('');
		};
	});

	//toggle closed branches showing in admin manage approvers
	$('#closed-branches-toggle').on('change', function() {
		$('.closed-branch-wrap').toggleClass('shown');
	});

	//select all months in recruiter control panel
	if ($('.months-wrap').length) {

		var checkBoxes = $('.months-wrap input[type=checkbox]');

		$('#month-toggle-all').on('click', function() {
			checkBoxes.prop('checked', $(this).prop('checked'));
		});
	};

	//fetch data button control
	$('.fetch-data-status-wrap a').on('click', function(e) {
		var self = $(this),
			status = self.siblings('.status'),
			statusProgress = self.siblings('.status.in-progress'),
			statusSuccess = self.siblings('.status.success'),
			statusFailed = self.siblings('.status.failed');

		status.removeClass('active');

		e.preventDefault();
		statusProgress.addClass('active');

		setTimeout(function() {
			statusProgress.removeClass('active')
			statusSuccess.addClass('active');
		}, 1500);

		setTimeout(function() {
			statusSuccess.removeClass('active')
			statusFailed.addClass('active');
		}, 3000);
	});

	//custom tooltip for bar chart
	var customTooltips = function (tooltip) {

		$("#hover-box-inner").css({
			opacity: 0,
		});

		if (!tooltip || !tooltip.opacity) {
			return;
		};

		if (tooltip.dataPoints.length > 0) {
			tooltip.dataPoints.forEach(function (dataPoint) {
				var content = [dataPoint.xLabel, dataPoint.yLabel].join("\n $");
				var $tooltip = $('#hover-box-inner');

				$tooltip.html(content);
				$tooltip.css({
					opacity: 1,
					top: dataPoint.y + "px",
					left: dataPoint.x + "px",
				});
			});
		}
	};

	var customTooltipsBubblesLike = function (tooltip) {

		$("#hover-box-inner").css({
			opacity: 0,
		});

		if (!tooltip || !tooltip.opacity) {
			return;
		};

		if (tooltip.dataPoints.length > 0) {
			tooltip.dataPoints.forEach(function (dataPoint) {
				var content = ['Week: ', dataPoint.xLabel, '\n New Candidates: ', dataPoint.yLabel];
				var $tooltip = $('#hover-box-inner');

				$tooltip.html(content);
				$tooltip.css({
					opacity: 1,
					top: dataPoint.y + "px",
					left: dataPoint.x + "px",
				});
			});
		}
	};

	//bar chart
	if ($('#commission-trend-chart').length) {
		var ctx = $('#commission-trend-chart'),
			pWidth = ctx.closest('.trend-chart').width(),
			pHeight = ctx.closest('.trend-chart').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Quicksand';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["Feb-16", "Mar-16", "Apr-16", "May-16", "Jun-16", "Jul-16", "Aug-16", "Sep-16", "Oct-16", "Nov-16", "Dec-16", "Jan-17*"],
		        datasets: [{
		        	label: '$',
		            data: [1986, 600, 400, 200, 1800, 1200, 980, 600, 632, 770, 1118, 900],
		            backgroundColor: [
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(119, 180, 226, 0.5)',
		                'rgba(76, 217, 100, 0.75)'
		            ],
		            hoverBackgroundColor: [
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(119, 180, 226, 1)',
		            	'rgba(76, 217, 100, 1)'
		            ],
		            borderWidth: 0
		        }]
		    },
		    options: {
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
		    		display: false
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: false,
		    		mode: 'index',
		    		intersect: false,
		    		custom: customTooltips
		    	},
		        scales: {
		        	xAxes: [{
		        		gridLines: {
		            		display: false,
		            		drawTicks: false
		            	},
		            	ticks: {
		                	fontStyle: 'bold',
		                	fontSize: 14
		                },
		                barThickness: 12
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.88)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 16,
		                	fontStyle: 'bold',
		                	min: 0,
		                	max: 2000,
		                	maxTicksLimit: 4,
		                	stepSize: 500,
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};
	

	$(window).resize(function() {
		//resizing bar charts
		if ($('#commission-trend-chart').length) {
			var pWidth = ctx.closest('.trend-chart').width(),
				pHeight = ctx.closest('.trend-chart').height();

			ctx.width(pWidth);
			ctx.height(pHeight);

			myChart.resize();
		};

		if ($('#recruiting-trend-chart').length) {
			var pWidth = ctx.closest('.trend-chart').width(),
				pHeight = ctx.closest('.trend-chart').height();

			ctx.width(pWidth);
			ctx.height(pHeight);

			myChart.resize();
		};

		if ($('#weekly-recruiting-trend-chart').length) {
			var pWidth = ctx.closest('.trend-chart').width(),
				pHeight = ctx.closest('.trend-chart').height();

			ctx.width(pWidth);
			ctx.height(pHeight);

			myChart.resize();
		};

		if ($('#commission-treemap').length) {
			//resizing heatmap/treemap by removing svg element and adding it again
			var svg = document.getElementById('commission-treemap');
			svg.parentNode.removeChild(svg);

			$('#commission-treemap-parent').append('<svg id="commission-treemap">');
			var svg = $('#commission-treemap');
			svgTreemap(svg, 'commission-by-branch');
		};

	});

	if ($('#commission-treemap-parent').length) {

		if ($('#commission-treemap').length) {
			var svg = $('#commission-treemap');
			svgTreemap(svg, 'commission-by-branch');
		};
	};

	//show hoverbox on rect hover

	if ($('#hover-box').length) {
		var hBox = $('#hover-box'),
			comTreemap = $('#commission-treemap-parent');

		comTreemap.on('mouseenter', 'rect', function() {

			var self = $(this),
				selfPos = self.position(),
				selfPosTop = selfPos.top,
				selfPosLeft = selfPos.left,
				selfWidth = self.attr('width'),
				selfHeight = self.attr('height'),
				titleText = self.siblings('title').text(),
				newText = titleText.split(/\n/),
				titleLength = newText.length;

			document.getElementById('hover-box').innerHTML = '';

			for(var i = 0; i < titleLength; i++) {
				var newElem = document.createElement('span');
				var newContent = document.createTextNode(newText[i]);

				newElem.appendChild(newContent);
				// console.log(newElem);
				// newElem.text(newText[i]);
				hBox.append(newElem);
				// console.log(newText[i]);
			};

			hBox.addClass('shown')
			.css(
				{
					'top': (selfPosTop + selfHeight/2),
					'left': (selfPosLeft + selfWidth/2)
				}
			);
			// .text(titleText);
		});

		comTreemap.on('mouseleave', function() {
			hBox.removeClass('shown');
		});
	};

	//recruiting trend chart in recruiter general dashboard
	if ($('#recruiting-trend-chart').length) {
		var ctx = $('#recruiting-trend-chart'),
			pWidth = ctx.closest('.trend-chart').width(),
			pHeight = ctx.closest('.trend-chart').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Quicksand';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["Apr-3", "Apr-10", "Apr-17", "Apr-24", "May-1", "May-8", "May-15", "May-22", "May-29", "Jun-5", "Jun-12", "Jun-19", "Jun-26"],
		        datasets: [{
		        	label: 'Workers',
		            data: [7, 12, 20, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		            backgroundColor: [
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)'
		            ],
		            hoverBackgroundColor: [
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)'
		            ],
		            borderWidth: 0
		        }]
		    },
		    options: {
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
		    		display: false
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: false,
		    		mode: 'index',
		    		intersect: false,
		    		custom: customTooltipsBubblesLike
		    	},
		        scales: {
		        	xAxes: [{
		        		gridLines: {
		            		display: false,
		            		drawTicks: false
		            	},
		            	ticks: {
		                	fontStyle: 'bold',
		                	fontSize: 14
		                },
		                barThickness: 12
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.88)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 16,
		                	fontStyle: 'bold',
		                	min: 0,
		                	max: 30,
		                	maxTicksLimit: 4,
		                	stepSize: 10,
		                    beginAtZero:true
		                }
		            }]
		        },
		        annotation: {
		        	events: ['mouseover', 'mouseout'],
		        	annotations: [{
		        		type: 'line',
		        		mode: 'horizontal',
		        		scaleID: 'y-axis-0',
		        		//the average value has to be inputted below
		        		value: 21,
		        		borderColor: 'rgb(249, 208, 19)',
		        		borderWidth: 2,
		        		label: {
		        			enabled: true,
		        			//the average value has to be inputted below
		        			content: 'Avg: 21',
		        			backgroundColor: 'rgba(255, 255, 255, 0)',
		        			fontFamily: "'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif",
		        			fontSize: 12,
		        			fontStyle: 'normal',
		        			fontColor: '#c2c2c2',
		        			position: 'right',
		        			xAdjust: -4,
		        			yAdjust: -10,
		        		},
		        		onMouseover: function(e) {
		        			var element = this;
		        			element.options.borderColor = 'rgb(230, 192, 18)';
							element.chartInstance.update();
		        		},
		        		onMouseout: function(e) {
		        			var element = this;
		        			element.options.borderColor = 'rgb(249, 208, 19)';
							element.chartInstance.update();
		        		}
		        	}]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

	//weekly recruiting trend chart in approver general dashboard
	if ($('#weekly-recruiting-trend-chart').length) {
		var ctx = $('#weekly-recruiting-trend-chart'),
			pWidth = ctx.closest('.trend-chart').width(),
			pHeight = ctx.closest('.trend-chart').height();

		ctx.width(pWidth);
		ctx.height(pHeight);

		Chart.defaults.global.defaultFontFamily = 'Quicksand';
		Chart.defaults.global.maintainAspectRatio = false;

		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["Apr-3", "Apr-10", "Apr-17", "Apr-24", "May-1", "May-8", "May-15", "May-22", "May-29", "Jun-5", "Jun-12", "Jun-19", "Jun-26"],
		        datasets: [{
		        	label: 'Workers',
		            data: [7, 12, 20, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		            backgroundColor: [
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)',
		                'rgb(249, 208, 19)'
		            ],
		            hoverBackgroundColor: [
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)',
		            	'rgb(230, 192, 18)'
		            ],
		            borderWidth: 0
		        }]
		    },
		    options: {
		    	layout: {
		    		padding: {
		    			bottom: 10,
		    		}
		    	},
		    	responsive: false,
		    	title: {
		    		display: false
		    	},
		    	legend: {
		    		display: false
		    	},
		    	hover: {
		    		mode: 'index',
		    		intersect: false
		    	},
		    	tooltips: {
		    		enabled: false,
		    		mode: 'index',
		    		intersect: false,
		    		custom: customTooltipsBubblesLike
		    	},
		        scales: {
		        	xAxes: [{
		        		gridLines: {
		            		display: false,
		            		drawTicks: false
		            	},
		            	ticks: {
		                	fontStyle: 'bold',
		                	fontSize: 14
		                },
		                barThickness: 12
		        	}],
		            yAxes: [{
		            	gridLines: {
		            		color: 'rgba(224, 224, 224, 0.88)',
		            		lineWidth: 1,
		            		drawBorder: false
		            	},
		                ticks: {
		                	padding: 20,
		                	fontSize: 16,
		                	fontStyle: 'bold',
		                	min: 0,
		                	max: 30,
		                	maxTicksLimit: 4,
		                	stepSize: 10,
		                    beginAtZero:true
		                }
		            }]
		        },
		        annotation: {
		        	events: ['mouseover', 'mouseout'],
		        	annotations: [{
		        		type: 'line',
		        		mode: 'horizontal',
		        		scaleID: 'y-axis-0',
		        		//the average value has to be inputted below
		        		value: 21,
		        		borderColor: 'rgb(249, 208, 19)',
		        		borderWidth: 2,
		        		label: {
		        			enabled: true,
		        			//the average value has to be inputted below
		        			content: 'Avg: 21',
		        			backgroundColor: 'rgba(255, 255, 255, 0)',
		        			fontFamily: "'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif",
		        			fontSize: 12,
		        			fontStyle: 'normal',
		        			fontColor: '#c2c2c2',
		        			position: 'right',
		        			xAdjust: -4,
		        			yAdjust: -10,
		        		},
		        		onMouseover: function(e) {
		        			var element = this;
		        			element.options.borderColor = 'rgb(230, 192, 18)';
							element.chartInstance.update();
		        		},
		        		onMouseout: function(e) {
		        			var element = this;
		        			element.options.borderColor = 'rgb(249, 208, 19)';
							element.chartInstance.update();
		        		}
		        	}]
		        }
		    }
		});

		setTimeout(function() {
			myChart.resize();
		}, 800);
	};

})(jQuery);

function svgTreemap(el, jsonF) {

	var jsonFile = jsonF;

	var self = el,
		parentWidth = self.parent().width(),
		parentHeight = self.parent().height(),
		elId = '#' + self.attr('id');

		self.attr('width', parentWidth);
		self.attr('height', parentHeight);

	var svg = d3.select(elId),
		width = +svg.attr("width"),
		height = +svg.attr("height"),
		rectWidth,
		rectHeight;

	var fader = function(color) { return d3.interpolateRgb(color, "#fff")(0.2); },
		color = d3.scaleOrdinal(d3.schemeCategory20.map(fader)),
		format = d3.format(",d");

	var treemap = d3.treemap()
		.tile(d3.treemapResquarify)
		.size([width, height])
		.round(true)
		.paddingInner(10);

	d3.json('json/' + jsonFile + '.json', function(error, data) {
		if (error) throw error;

		var root = d3.hierarchy(data)
			.eachBefore(function(d) { d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name; })
			.sum(sumBySize)
			.sort(function(a, b) { return b.height - a.height || b.value - a.value; });

		treemap(root);

		var cell = svg.selectAll("g")
			.data(root.leaves())
			.enter().append("g")
			.attr("transform", function(d) { return "translate(" + d.x0 + "," + d.y0 + ")"; });

		cell.append("rect")
			.attr("id", function(d) { return d.data.id; })
			.attr("width", function(d) { return d.x1 - d.x0; })
			.attr("height", function(d) { return d.y1 - d.y0; })
			.attr('rx', '5')
			.attr('ry', '5')
			.attr("style", 'fill: #ffffff; stroke: #d7d7d7; stroke-width: 1;');

		cell.append("clipPath")
			.attr("id", function(d) { return "clip-" + d.data.id; })
			.append("use")
			.attr("xlink:href", function(d) { return "#" + d.data.id; });

		cell.append("text")
			.attr('x', function(d) { return (d.x1 - d.x0)/2; })
			.attr('y', function(d) { return (d.y1 - d.y0)/2; })
			.attr('text-anchor', 'middle')
			.attr('fill', '#399de9')
			.attr("clip-path", function(d) { return "url(#clip-" + d.data.id + ")"; })
			.text(function(d) {
					return d.data.name;
			})
			.text(function(d) {
				var self = $(this),
					siblingWidth = self.siblings('rect').attr('width'),
					siblingHeight = self.siblings('rect').attr('height');

				if (self.width() > siblingWidth) {
					self.attr('fill', 'rgba(92, 92, 92, 0.5)');

					if ((siblingWidth > 45) && (siblingHeight > 30)) {
						return d.data.percent + '%';
					} else {
						return;
					};
				} else {
					return d.data.name;
				};
			});
			// .selectAll("tspan")
			// 	.data(function(d) { return d.data.name.split(/(?=[A-Z][^A-Z])/g) })
			// 		.enter().append("tspan")
						// .attr("x", function(d) { console.log(rectWidth); return rectWidth; })
						// .attr('y', function(d, i) { return rectHeight + i * 15;})
						// .attr('x', '0')
						// .attr('dy', '20')
						// .text(function(d) { return d; });

		cell.append("title")
			.text(function(d) { return d.data.name + '\n' + '$' + format(d.value) + '\n' + d.data.percent + '%'; });

		d3.selectAll("input")
			.data([sumBySize, sumByCount], function(d) { return d ? d.name : this.value; })
			.on("change", changed);

		var timeout = d3.timeout(function() {
			d3.select("input[value=\"sumByCount\"]")
				.property("checked", true)
				.dispatch("change");
		}, 2000);

		function changed(sum) {
			timeout.stop();

			treemap(root.sum(sum));

		cell.transition()
			.duration(750)
			.attr("transform", function(d) { return "translate(" + d.x0 + "," + d.y0 + ")"; })
			.select("rect")
				.attr("width", function(d) { return d.x1 - d.x0; })
				.attr("height", function(d) { return d.y1 - d.y0; });
		}
	});

	function sumByCount(d) {
		return d.children ? 0 : 1;
	};

	function sumBySize(d) {
		return d.size;
	};
};

var inputClicked;

function readURL(input) {
	if (input.files && input.files[0]) {

		var reader = new FileReader();

		if (inputClicked.is('#avatar-upload')) {

			reader.onload = function(e) {
				$('#user-avatar-profile').attr('src', e.target.result);
			};
		};

		reader.readAsDataURL(input.files[0]);
	};
};