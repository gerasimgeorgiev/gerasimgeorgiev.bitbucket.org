(function($){ 
	"use strict";

	// $('.nav').responsiveNav();

	//modal control
	// open modal
    $('.js-modal-open').click(function () {
        var self = $(this);
        var btnName = self.attr('name');

        $('.' + btnName).fadeIn(200);
    });

	// close modal
    $('.js-modal-close').click(function () {
        var self = $(this);
        self.parents('.modal-bg-wrapper').fadeOut(200);
    });

    //closing modal on outside click
    $('.modal-bg-wrapper').on('click', function(e){
		var target = e.target;

	    if (!$(target).is('.modal-inner-wrapper *') && !$(target).is('.modal-inner-wrapper')) {
	    	$(this).fadeOut(200);
	    };
	});

	var feedArticle;

	//opening modal for voting on feed list page
	$('.feed-list a.type').on('click', function(e) {
		var target = e.target;
		var element = $(target);
		var parentEl = $(target).closest('a.type');

		//controlling voting
		if ( ($(target).is('.votes .small-wrap *')) || ($(target).is('.votes .small-wrap'))) {
			e.preventDefault();

			feedArticle = votingForPost(element, parentEl);
		};

		// //controlling liking
		if ( ($(target).is('.likes .small-wrap *')) || ($(target).is('.likes .small-wrap'))) {
			e.preventDefault();

			var element = $(target);

			likingPost(element);
		};
	});

	//opening modal for voting on full story page
	$('.full-story .details').on('click', function(e) {
		var target = e.target;
		var element = $(target);
		var parentEl = $(target).closest('.full-story');

		//controlling voting
		if ( ($(target).is('.votes .small-wrap *')) || ($(target).is('.votes .small-wrap'))) {
			e.preventDefault();

			feedArticle = votingForPost(element, parentEl);
		};

		// //controlling liking
		if ( ($(target).is('.likes .small-wrap *')) || ($(target).is('.likes .small-wrap'))) {
			e.preventDefault();

			var element = $(target);

			likingPost(element);
		};
	});

	//hiding modal voting on mouseleave
	$('.modal-voting').on('mouseleave', function() {
		var self = $(this);
		self.removeClass('shown');
	});

	$('.feed-list a.type').on('mouseleave', function() {
		$('.modal-voting').removeClass('shown');
	});

	$('.modal-voting').on('mouseenter', function() {
		$('.modal-voting').addClass('shown');
	});

	//controlling votes on input click
	$('.modal-voting .link-vote input').on('click', function() {
		var self = $(this);
		var numVotes = self.attr('data-vote-num'),
			numVotes = parseInt(numVotes);

		if (self.is(':checked')) {
			var articleTitle = feedArticle.find('.title').text();
			feedArticle.find('.votes').addClass('voted');

			switch (numVotes) {
				case 1:
					//remove the following alert when deploying!!
					alert('you voted for ' + articleTitle + ' with one vote');
					$('.modal-voting').find('.link-remove-votes').css('display', 'inline-block');
					feedArticle.attr('data-your-votes', '1');
					break;
				case 2:
					//remove the following alert when deploying!!
					alert('you voted for ' + articleTitle + ' with two votes');
					$('.modal-voting').find('.link-remove-votes').css('display', 'inline-block');
					feedArticle.attr('data-your-votes', '2');
					break;
				case 3:
					//remove the following alert when deploying!!
					alert('you voted for ' + articleTitle + ' with three votes');
					$('.modal-voting').find('.link-remove-votes').css('display', 'inline-block');
					feedArticle.attr('data-your-votes', '3');
					break;
				default:
					alert('Something went wrong');
			};
		};
	});

	//removing votes on remove votes link click
	$('.modal-voting .link-remove-votes').on('click', function(e) {
		e.preventDefault();
		feedArticle.attr('data-your-votes', '0');
		feedArticle.find('.votes').removeClass('voted');
		$('.modal-voting input').prop('checked', false);
		$(this).css('display', 'none');
	});

	// hiding broken avatar image for initials can show properly
	$(".js-avatar-img").on('error', function () {
	    $(this).hide();
	});

	//preview image in new post
	$('.post-file-input').on('change', function(){
		inputClicked = $(this);
	    readURL(this);
	});

	//deleting previewed image in new post
	$('.input-wrap').on('click', 'label.with-image', function(e) {
		var self = $(this);

		self.removeClass('with-image').find('img').attr('src', '');
		self.prev('input').val('');
		e.preventDefault();
	});

	//swiper slider in milestones
	var milestonesSwiper = new Swiper ('.swiper-container.milestones-slider', {
		freeMode: true,
		loop: false,
		spaceBetween: 0,
		slidesPerView: 5,
		grabCursor: true,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		onTouchStart: function(swiper, event) {
			$('.modal-milestone-details.shown').removeClass('shown');
		},
		// preventClicks: false,
		// preventClicksPropagation: false
	});

	//sliding to specific slide (the chosen slide ends up as the first from the left (outside of viewport!))
	//chosen slide - 1 would work best, so that the chosen slide can be seen on the leftmost position
	milestonesSwiper.slideTo(2, 600);

	//preventing marking milestone as completed if image is missing
	$('.milestone-small .milestone-checker').on('click', function(e) {
		var parent = $(this).closest('.milestone-small');

		if (parent.hasClass('image-req')) {

			if (!parent.hasClass('image-uploaded')) {
				e.preventDefault();
				$('.modal-bg-wrapper.js-clarification').fadeIn(200);
			};
		};
	});

	var selectedMilestoneInput;

	//opening milestone image upload popup
	$('.milestone-small .milestone-file-label').on('click', function(e) {
		var self = $(this);

		if (self.hasClass('with-image')) {
			e.preventDefault();
		} else {
			e.preventDefault();
			selectedMilestoneInput = self.prev('input');
			$('.modal-bg-wrapper.js-upload-image').fadeIn(200);
		}
	});

	//triggering click on milestone file input on popup browse button click
	$('.js-upload-image .btn-browse').on('click', function(e) {
		selectedMilestoneInput.trigger('click');
	});

	//preview image in milestones
	$('.milestone-file-input').on('change', function() {
		inputClicked = $(this);
		readURL(this);
	});

	//deleting or replacing previewed image in milestones
	$('.milestone-small .file-wrap').on('click', 'label.with-image', function(e) {
		var self = $(this),
			target = e.target;

		//deleting previewed image in milestones
		if (($(target).is('.btn-delete *')) || ($(target).is('.btn-delete'))) {
			self.removeClass('with-image').find('img').attr('src', '');
			self.prev('input').val('');
			e.preventDefault();
		} else {
			//replacing previewed image in milestones
			if (($(target).is('.btn-replace *')) || ($(target).is('.btn-replace'))) {
				e.preventDefault();
				selectedMilestoneInput = self.prev('input');
				$('.modal-bg-wrapper.js-upload-image').fadeIn(200);
			};
		};
	});

	//opening milestone details modal
	$('.milestone-small .title').on('click', function() {
		var self = $(this),
			selfWidth = self.width(),
			selfPos = self.offset(),
        	selfPosTop = selfPos.top,
        	selfPosLeft = selfPos.left,
        	modalDetails = $('.modal-milestone-details'),
        	modalHeight = modalDetails.height(),
        	finalPosTop = selfPosTop - modalHeight - 50,
        	finalPosLeft = selfPosLeft + (selfWidth / 2) - 150;

		$('.modal-milestone-details').css({
			'top': finalPosTop,
			'left': finalPosLeft
		}).addClass('shown');
	});

	// closing milestone details modal
    $('.js-modal-details-close').click(function () {
        var self = $(this);
        self.parents('.modal-milestone-details').removeClass('shown');
    });

	//custom dropdown implementation
	$('.custom-dropdown select').select2({
		minimumResultsForSearch: Infinity,
	});

	//awesomplete as dropdown
	if ($('.awesomplete-as-dropdown').length) {

		var awesompleteDropdownInput = document.querySelectorAll('.awesomplete-as-dropdown');

		awesompleteDropdownInput.forEach(function(el) {

			var awesomplete = new Awesomplete(el,
				{
					autoFirst: true,
					minChars: 0,
					maxItems: 500
				}
			);

			el.addEventListener('click', function() {
				el.value = "";
				awesomplete.evaluate();
				awesomplete.open();
			});

			el.addEventListener('awesomplete-selectcomplete', function(event) {
				alert(el.value);
				// el.value = event.originalEvent.text.label;
			});
		});

		// // old declaration - doesn't work with multiple awesompletes on one page
		// var awesompleteDropdownInput = $('.awesomplete-as-dropdown');

		// var awesomplete = new Awesomplete('.awesomplete-as-dropdown',
		// 	{
		// 		autoFirst: true,
		// 		minChars: 0,
		// 		maxItems: 500
		// });

		// awesompleteDropdownInput.on('click', function() {
		// 	awesompleteDropdownInput.val('');
		// 	awesomplete.evaluate();
		// 	awesomplete.open();
		// });

		// awesompleteDropdownInput.on('awesomplete-selectcomplete', function(event) {
		// 	alert(awesompleteDropdownInput.val());
		// 	awesompleteDropdownInput.val(event.originalEvent.text.label);
		// });
	};

	//showing delete btn for search clear input on initial keypress
	$('.search-wrap input').on('keypress', function() {
		var self = $(this),
			clearBtn = self.next('.btn-search-clear');

		if (clearBtn.hasClass('hidden')) {
			clearBtn.removeClass('hidden');
		};
	});

	//search input field clear on btn click
	$('.btn-search-clear').on('click', function() {
		var self = $(this);

		self.prev('input').val('');
		self.addClass('hidden');
	});

	//js mobile detection
	var isMobile = window.matchMedia("only screen and (max-width: 768px)");

	//disabling select2 plugin on mobile to enable default mobile dropdown
	if (isMobile.matches) {
		$('.custom-dropdown select').select2('destroy');
	};

})(jQuery);

function votingForPost(element, parentEl) {
		
	var self = element,
		parent = parentEl,
		yourCurVotes = parent.attr('data-your-votes'),
		yourCurVotes = parseInt(yourCurVotes);

	feedArticle = parent;

	if (!(self.hasClass('.votes'))) {
		self = self.closest('.votes ');
	};

	var selfPos = self.offset(),
		selfPosTop = selfPos.top,
		selfPosLeft = selfPos.left,
		selfWidth = self.width();
		modalVoting = $('.modal-voting'),
		finalPosTop = selfPosTop - 205,
		finalPosLeft = selfPosLeft + (selfWidth / 2) - 90;

	modalVoting.css({
		'top': finalPosTop,
		'left': finalPosLeft
	});

	switch (yourCurVotes) {
		case 0:
			modalVoting.find('input').prop('checked', false);
			modalVoting.find('.link-remove-votes').css('display', 'none');
			break;
		case 1:
			modalVoting.find('#vote-one').prop('checked', true);
			modalVoting.find('.link-remove-votes').css('display', 'inline-block');
			break;
		case 2:
			modalVoting.find('#vote-two').prop('checked', true);
			modalVoting.find('.link-remove-votes').css('display', 'inline-block');
			break;
		case 3:
			modalVoting.find('#vote-three').prop('checked', true);
			modalVoting.find('.link-remove-votes').css('display', 'inline-block');
			break;
	};

	modalVoting.addClass('shown');

	return feedArticle;
};

function likingPost(element) {

	//controlling liking
	var self = element;

	if (!(self.hasClass('.likes'))) {
		self = self.closest('.likes');
	};

	if (self.hasClass('liked')) {
		self.removeClass('liked');
	} else {
		self.addClass('liked');
	};
};

var inputClicked;

function readURL(input) {
	if (input.files && input.files[0]) {

		var reader = new FileReader();

		if ((inputClicked.is('.post-file-input')) || (inputClicked.is('.milestone-file-input')) ) {

			reader.onload = function(e) {
				inputClicked.next('label').addClass('with-image').find('img').attr('src', e.target.result);
			};
		};

		reader.readAsDataURL(input.files[0]);
	};
};